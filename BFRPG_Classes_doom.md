# **DOOMED CLASSES**
[Back to Wiki](https://gitlab.com/voidcollection/shadows_of_the_abbey/-/wikis/home)

## **The Veteran**
<details>

- **Ancestry Limits:** Any
- **Prime Requisite:** STR (Must be 9+)
- **Hit Dice:** D8
- **Weapons:** Any.
- **Armor:** Any, shields allowed.
- **Starting Saves:** Death 12; Wands 13; Paralysis 14; Breath 15; Spells 17
- **Special:**
  - **Cleave:** If a fighter drops a foe of to Zero health the fighter may make another attack if an available target is within range. Total number of 'Cleave' attacks not to exceed the fighter's level.

### **Progression & Saves**
<details>


#### **PROGRESSION**

| **Lvl** | **XP**  | **HD** | **AB** |
|---------|---------|--------|--------|
| **1**   | 0       | 1d8    | 1      |
| **2**   | 2000    | 2d8    | 2      |
| **3**   | 4000    | 3d8    | 2      |
| **4**   | 8000    | 4d8    | 3      |
| **5**   | 16000   | 5d8    | 4      |
| **6**   | 32000   | 6d8    | 4      |
| **7**   | 64000   | 7d8    | 5      |
| **8**   | 120000  | 8d8    | 6      |
| **9**   | 240000  | 9d8    | 6      |
| **10**  | 360000  | 9d8+2  | 6      |
| **11**  | 480000  | 9d8+4  | 7      |
| **12**  | 600000  | 9d8+6  | 7      |
| **13**  | 720000  | 9d8+8  | 8      |
| **14**  | 840000  | 9d8+10 | 8      |
| **15**  | 960000  | 9d8+12 | 8      |
| **16**  | 1080000 | 9d8+14 | 9      |
| **17**  | 1200000 | 9d8+16 | 9      |
| **18**  | 1320000 | 9d8+18 | 10     |
| **19**  | 1440000 | 9d8+20 | 10     |
| **20**  | 1560000 | 9d8+22 | 10     |

#### **SAVES**

| **Lvl**       | **Death** | **Wands** | **Paralysis** | **Breath** | **Spells** |
|---------------|-----------|-----------|---------------|------------|------------|
| **1**         | 12        | 13        | 14            | 15         | 17         |
| **2**         | 11        | 12        | 14            | 15         | 16         |
| **3**         | 11        | 12        | 14            | 15         | 16         |
| **4**         | 11        | 11        | 13            | 14         | 15         |
| **5**         | 11        | 11        | 13            | 14         | 15         |
| **6**         | 10        | 11        | 12            | 14         | 15         |
| **7**         | 10        | 11        | 12            | 14         | 15         |
| **8**         | 9         | 10        | 12            | 13         | 14         |
| **9**         | 9         | 10        | 12            | 13         | 14         |
| **10**        | 9         | 9         | 11            | 12         | 13         |
| **11**        | 9         | 9         | 11            | 12         | 13         |
| **12**        | 8         | 9         | 10            | 12         | 13         |
| **13**        | 8         | 9         | 10            | 12         | 13         |
| **14**        | 7         | 8         | 10            | 11         | 12         |
| **15**        | 7         | 8         | 10            | 11         | 12         |
| **16**        | 7         | 7         | 9             | 10         | 11         |
| **17**        | 7         | 7         | 9             | 10         | 11         |
| **18**        | 6         | 7         | 8             | 10         | 11         |
| **19**        | 6         | 7         | 8             | 10         | 11         |
| **20**        | 5         | 6         | 8             | 9          | 10         |

</details>

</details>


## **The Pilgrim**
<details>

- **Ancestry Limits:** Any
- **Prime Requisite:** WIS (Must be 9+)
- **Hit Dice:** D6
- **Weapons:** Blunt weapons only.
- **Armor:** Any, shields allowed.
- **Starting Saves:** Death 11; Wands 12; Paralysis 14; Breath 16; Spells 15
- **Special:**
  - **Deity disfavour:** Pilgrims must be faithful to the tenets of their alignment, clergy, and religion. Those who fall from favour with their deity may incur penalties.
  - **Turn Undead:** Pilgrims can invoke the power of their deity to repel undead monsters encountered. Requires brandishing a holy symbol.
  - **Spell casting:** Once a Pilgrim has proven their faith (at 2nd level), the character may pray to receive spells.
  - **Using magic items:** As spell casters, Pilgrim can use magic scrolls of spells on their spell list. They can also use items that may only be used by divine spell casters (e.g. some magic staves).
  - **Magical research:** Can spend time and money on research to create new spells or other magical effects associated with their deity. At 7th level, they are able to make temporary magic items. At 9th level, they can make permanent ones.

### **Progression & Saves**
<details>]


#### **PROGRESSION**

|             |         |        |        | **Spells** |       |       |       |       |       |
|-------------|---------|--------|--------|------------|-------|-------|-------|-------|-------|
| **Lvl**     | **XP**  | **HD** | **AB** | **1**      | **2** | **3** | **4** | **5** | **6** |
| **1**       | 0       | 1d6    | 1      | -          | -     | -     | -     | -     | -     |
| **2**       | 1500    | 2d6    | 1      | 1          | -     | -     | -     | -     | -     |
| **3**       | 3000    | 3d6    | 2      | 2          | -     | -     | -     | -     | -     |
| **4**       | 6000    | 4d6    | 2      | 2          | 1     | -     | -     | -     | -     |
| **5**       | 12000   | 5d6    | 3      | 2          | 2     | -     | -     | -     | -     |
| **6**       | 24000   | 6d6    | 3      | 2          | 2     | 1     | -     | -     | -     |
| **7**       | 48000   | 7d6    | 4      | 3          | 2     | 2     | -     | -     | -     |
| **8**       | 90000   | 8d6    | 4      | 3          | 2     | 2     | 1     | -     | -     |
| **9**       | 180000  | 9d6    | 5      | 3          | 3     | 2     | 2     | -     | -     |
| **10**      | 270000  | 9d6+1  | 5      | 3          | 3     | 2     | 2     | 1     | -     |
| **11**      | 360000  | 9d6+2  | 5      | 4          | 3     | 3     | 2     | 2     | -     |
| **12**      | 450000  | 9d6+3  | 6      | 4          | 4     | 3     | 2     | 2     | 1     |
| **13**      | 540000  | 9d6+4  | 6      | 4          | 4     | 3     | 3     | 2     | 2     |
| **14**      | 630000  | 9d6+5  | 6      | 4          | 4     | 4     | 3     | 2     | 2     |
| **15**      | 720000  | 9d6+6  | 7      | 4          | 4     | 4     | 3     | 3     | 2     |
| **16**      | 810000  | 9d6+7  | 7      | 5          | 4     | 4     | 3     | 3     | 2     |
| **17**      | 900000  | 9d6+8  | 7      | 5          | 5     | 4     | 3     | 3     | 2     |
| **18**      | 990000  | 9d6+9  | 8      | 5          | 5     | 4     | 4     | 3     | 3     |
| **19**      | 1080000 | 9d6+10 | 8      | 6          | 5     | 4     | 4     | 3     | 3     |
| **20**      | 1170000 | 9d6+11 | 8      | 6          | 5     | 5     | 4     | 3     | 3     |


#### **SAVES**

| **Lvl**       | **Death** | **Wands** | **Paralysis** | **Breath** | **Spells** |
|---------------|-----------|-----------|---------------|------------|------------|
| **1**         | 11        | 12        | 14            | 16         | 15         |
| **2**         | 10        | 11        | 13            | 15         | 14         |
| **3**         | 10        | 11        | 13            | 15         | 14         |
| **4**         | 9         | 10        | 13            | 15         | 14         |
| **5**         | 9         | 10        | 13            | 15         | 14         |
| **6**         | 9         | 10        | 12            | 14         | 13         |
| **7**         | 9         | 10        | 12            | 14         | 13         |
| **8**         | 8         | 9         | 12            | 14         | 13         |
| **9**         | 8         | 9         | 12            | 14         | 13         |
| **10**        | 8         | 9         | 11            | 13         | 12         |
| **11**        | 8         | 9         | 11            | 13         | 12         |
| **12**        | 7         | 8         | 11            | 13         | 12         |
| **13**        | 7         | 8         | 11            | 13         | 12         |
| **14**        | 7         | 8         | 10            | 12         | 11         |
| **15**        | 7         | 8         | 10            | 12         | 11         |
| **16**        | 6         | 7         | 10            | 12         | 11         |
| **17**        | 6         | 7         | 10            | 12         | 11         |
| **18**        | 6         | 7         | 9             | 11         | 10         |
| **19**        | 6         | 7         | 9             | 11         | 10         |
| **20**        | 5         | 6         | 9             | 11         | 10         |

</details>

### **Turn Undead**
<details>

| **Caster Level** | **1 Hit Die** | **2 Hit Dice** | **3 Hit Dice** | **4 Hit Dice** | **5 Hit Dice** | **6 Hit Dice** | **7 Hit Dice** | **8 Hit Dice** | **9+ Hit Dice** |
|:----------------:|:-------------:|:--------------:|:--------------:|:--------------:|:--------------:|:--------------:|:--------------:|:--------------:|:---------------:|
| **1**            | 13            | 17             | 19             | No             | No             | No             | No             | No             | No              |
| **2**            | 11            | 15             | 18             | 20             | No             | No             | No             | No             | No              |
| **3**            | 9             | 13             | 17             | 19             | No             | No             | No             | No             | No              |
| **4**            | 7             | 11             | 15             | 18             | 20             | No             | No             | No             | No              |
| **5**            | 5             | 9              | 13             | 17             | 19             | No             | No             | No             | No              |
| **6**            | 3             | 7              | 11             | 15             | 18             | 20             | No             | No             | No              |
| **7**            | 2             | 5              | 9              | 13             | 17             | 19             | No             | No             | No              |
| **8**            | T             | 3              | 7              | 11             | 15             | 18             | 20             | No             | No              |
| **9**            | T             | 2              | 5              | 9              | 13             | 17             | 19             | No             | No              |
| **10**           | T             | T              | 3              | 7              | 11             | 15             | 18             | 20             | No              |
| **11**           | D             | T              | 2              | 5              | 9              | 13             | 17             | 19             | No              |
| **12**           | D             | T              | T              | 3              | 7              | 11             | 15             | 18             | 20              |
| **13**           | D             | D              | T              | 2              | 5              | 9              | 13             | 17             | 19              |
| **14**           | D             | D              | T              | T              | 3              | 7              | 11             | 15             | 18              |
| **15**           | D             | D              | D              | T              | 2              | 5              | 9              | 13             | 17              |
| **16**           | D             | D              | D              | T              | T              | 3              | 7              | 11             | 15              |
| **17**           | D             | D              | D              | D              | T              | 2              | 5              | 9              | 13              |
| **18**           | D             | D              | D              | D              | T              | T              | 3              | 7              | 11              |

</details>

</details>

## **The Magician**
<details>

- **Ancestry Limits:** Human & Elf
- **Prime Requisite:** INT (Must be 9+)
- **Hit Dice:** D4
- **Weapons:** Cudgel, dagger, walking staff.
- **Armor:** None.
- **Starting Saves:** Death 13; Wands 14; Paralysis 13; Breath 16; Spells 15
- **Special:**
  - **Spell casting:** Start with **Read Magic** and 1+INT modifier number of spells, randomly selected. Magic-users carry spell books containing the formulae for arcane spells.
  - **Using magic items:** Able to use magic scrolls of spells on their spell list. They can also use items that may only be used by arcane spell casters (e.g. magic wands).
  - **Magical research:** Can spend time and money on research to add new spells to their spell book and to research other magical effects. At 7th level, they are able to make temporary magic items. At 9th level, they can make permanent ones.

### **Progression & Saves**
<details>


#### **PROGRESSION**

|             |         |        |        | **Spells** |       |       |       |       |       |
|-------------|---------|--------|--------|------------|-------|-------|-------|-------|-------|
| **Lvl**     | **XP**  | **HD** | **AB** | **1**      | **2** | **3** | **4** | **5** | **6** |
| **1**       | 0       | 1d4    | 1      | 1          | -     | -     | -     | -     | -     |
| **2**       | 2500    | 2d4    | 1      | 2          | -     | -     | -     | -     | -     |
| **3**       | 5000    | 3d4    | 1      | 2          | 1     | -     | -     | -     | -     |
| **4**       | 10000   | 4d4    | 2      | 2          | 2     | -     | -     | -     | -     |
| **5**       | 20000   | 5d4    | 2      | 2          | 2     | 1     | -     | -     | -     |
| **6**       | 40000   | 6d4    | 3      | 3          | 2     | 2     | -     | -     | -     |
| **7**       | 80000   | 7d4    | 3      | 3          | 2     | 2     | 1     | -     | -     |
| **8**       | 150000  | 8d4    | 3      | 3          | 3     | 2     | 2     | -     | -     |
| **9**       | 300000  | 9d4    | 4      | 3          | 3     | 2     | 2     | 1     | -     |
| **10**      | 450000  | 9d4+1  | 4      | 4          | 3     | 3     | 2     | 2     | -     |
| **11**      | 600000  | 9d4+2  | 4      | 4          | 4     | 3     | 2     | 2     | 1     |
| **12**      | 750000  | 9d4+3  | 4      | 4          | 4     | 3     | 3     | 2     | 2     |
| **13**      | 900000  | 9d4+4  | 5      | 4          | 4     | 4     | 3     | 2     | 2     |
| **14**      | 1050000 | 9d4+5  | 5      | 4          | 4     | 4     | 3     | 3     | 2     |
| **15**      | 1200000 | 9d4+6  | 5      | 5          | 4     | 4     | 3     | 3     | 2     |
| **16**      | 1350000 | 9d4+7  | 6      | 5          | 5     | 4     | 3     | 3     | 2     |
| **17**      | 1500000 | 9d4+8  | 6      | 5          | 5     | 4     | 4     | 3     | 3     |
| **18**      | 1650000 | 9d4+9  | 6      | 6          | 5     | 4     | 4     | 3     | 3     |
| **19**      | 1800000 | 9d4+10 | 7      | 6          | 5     | 5     | 4     | 3     | 3     |
| **20**      | 1950000 | 9d4+11 | 7      | 6          | 5     | 5     | 4     | 4     | 3     |


#### **SAVES**

| **Lvl**       | **Death** | **Wands** | **Paralysis** | **Breath** | **Spells** |
|---------------|-----------|-----------|---------------|------------|------------|
| **1**         | 13        | 14        | 13            | 16         | 15         |
| **2**         | 13        | 14        | 13            | 15         | 14         |
| **3**         | 13        | 14        | 13            | 15         | 14         |
| **4**         | 12        | 13        | 12            | 15         | 13         |
| **5**         | 12        | 13        | 12            | 15         | 13         |
| **6**         | 12        | 12        | 11            | 14         | 13         |
| **7**         | 12        | 12        | 11            | 14         | 13         |
| **8**         | 11        | 11        | 10            | 14         | 12         |
| **9**         | 11        | 11        | 10            | 14         | 12         |
| **10**        | 11        | 10        | 9             | 13         | 11         |
| **11**        | 11        | 10        | 9             | 13         | 11         |
| **12**        | 10        | 10        | 9             | 13         | 11         |
| **13**        | 10        | 10        | 9             | 13         | 11         |
| **14**        | 10        | 9         | 8             | 12         | 10         |
| **15**        | 10        | 9         | 8             | 12         | 10         |
| **16**        | 9         | 8         | 7             | 12         | 9          |
| **17**        | 9         | 8         | 7             | 12         | 9          |
| **18**        | 9         | 7         | 6             | 11         | 9          |
| **19**        | 9         | 7         | 6             | 11         | 9          |
| **20**        | 8         | 6         | 5             | 11         | 8          |

</details>

</details>

## **The Judge**
<details>

- **Prime Requisite:** STR (Must be 9+); WIS (Must be 9+)
- **Hit Dice:** D6
- **Weapons:** Any
- **Armor:** Any, including Shields
- **Special:**
  - **Cleave:** If Judge drops a foe to Zero health they may make another immediate melee attack if an available target is within range.
  - **Deity disfavour:** Pilgrims must be faithful to the tenets of their alignment, clergy, and religion. Those who fall from favour with their deity may incur penalties.
  - **Turn Undead:** Pilgrims can invoke the power of their deity to repel undead monsters encountered. Requires brandishing a holy symbol.
  - **Spell casting:** Once a Pilgrim has proven their faith (at 2nd level), the character may pray to receive spells.
  - **Using magic items:** As spell casters, Pilgrim can use magic scrolls of spells on their spell list. They can also use items that may only be used by divine spell casters (e.g. some magic staves).
  - **Magical research:** Can spend time and money on research to create new spells or other magical effects associated with their deity. At 7th level, they are able to make temporary magic items. At 9th level, they can make permanent ones.

### **Progression & Saves**
<details>


#### **PROGRESSION

|         |         |        |        | **Spells** |       |       |       |       |       |
| **Lvl** | **XP**  | **HD** | **AB** | **1** | **2** | **3** | **4** | **5** | **6** |
|---------|---------|--------|--------|-------|-------|-------|-------|-------|-------|
| **1**   | 0       | 1d6    | 1      | -     | -     | -     | -     | -     | -     |
| **2**   | 3500    | 2d6    | 2      | 1     | -     | -     | -     | -     | -     |
| **3**   | 7000    | 3d6    | 2      | 2     | -     | -     | -     | -     | -     |
| **4**   | 14000   | 4d6    | 3      | 2     | 1     | -     | -     | -     | -     |
| **5**   | 28000   | 5d6    | 4      | 2     | 2     | -     | -     | -     | -     |
| **6**   | 56000   | 6d6    | 4      | 2     | 2     | 1     | -     | -     | -     |
| **7**   | 112000  | 7d6    | 5      | 3     | 2     | 2     | -     | -     | -     |
| **8**   | 210000  | 8d6    | 6      | 3     | 2     | 2     | 1     | -     | -     |
| **9**   | 420000  | 9d6    | 6      | 3     | 3     | 2     | 2     | -     | -     |
| **10**  | 630000  | 9d6+2  | 6      | 3     | 3     | 2     | 2     | 1     | -     |
| **11**  | 840000  | 9d6+4  | 7      | 4     | 3     | 3     | 2     | 2     | -     |
| **12**  | 1050000 | 9d6+6  | 7      | 4     | 4     | 3     | 2     | 2     | 1     |
| **13**  | 1260000 | 9d6+8  | 8      | 4     | 4     | 3     | 3     | 2     | 2     |
| **14**  | 1470000 | 9d6+10 | 8      | 4     | 4     | 4     | 3     | 2     | 2     |
| **15**  | 1680000 | 9d6+12 | 8      | 4     | 4     | 4     | 3     | 3     | 2     |
| **16**  | 1890000 | 9d6+14 | 9      | 5     | 4     | 4     | 3     | 3     | 2     |
| **17**  | 2100000 | 9d6+16 | 9      | 5     | 5     | 4     | 3     | 3     | 2     |
| **18**  | 2310000 | 9d6+18 | 10     | 5     | 5     | 4     | 4     | 3     | 3     |
| **19**  | 2520000 | 9d6+20 | 10     | 6     | 5     | 4     | 4     | 3     | 3     |
| **20**  | 2730000 | 9d6+22 | 10     | 6     | 5     | 5     | 4     | 3     | 3     |


#### **SAVES**

| **Lvl**       | **Death** | **Wands** | **Paralysis** | **Breath** | **Spells** |
|---------------|-----------|-----------|---------------|------------|------------|
| **1**         | 11        | 12        | 14            | 16         | 15         |
| **2**         | 10        | 11        | 13            | 15         | 14         |
| **3**         | 10        | 11        | 13            | 15         | 14         |
| **4**         | 9         | 10        | 13            | 15         | 14         |
| **5**         | 9         | 10        | 13            | 15         | 14         |
| **6**         | 9         | 10        | 12            | 14         | 13         |
| **7**         | 9         | 10        | 12            | 14         | 13         |
| **8**         | 8         | 9         | 12            | 14         | 13         |
| **9**         | 8         | 9         | 12            | 14         | 13         |
| **10**        | 8         | 9         | 11            | 13         | 12         |
| **11**        | 8         | 9         | 11            | 13         | 12         |
| **12**        | 7         | 8         | 11            | 13         | 12         |
| **13**        | 7         | 8         | 11            | 13         | 12         |
| **14**        | 7         | 8         | 10            | 12         | 11         |
| **15**        | 7         | 8         | 10            | 12         | 11         |
| **16**        | 6         | 7         | 10            | 12         | 11         |
| **17**        | 6         | 7         | 10            | 12         | 11         |
| **18**        | 6         | 7         | 9             | 11         | 10         |
| **19**        | 6         | 7         | 9             | 11         | 10         |
| **20**        | 5         | 6         | 9             | 11         | 10         |

</details>

### **Turn Undead**

<details>

| **Caster Level** | **1 Hit Die** | **2 Hit Dice** | **3 Hit Dice** | **4 Hit Dice** | **5 Hit Dice** | **6 Hit Dice** | **7 Hit Dice** | **8 Hit Dice** | **9+ Hit Dice** |
|:----------------:|:-------------:|:--------------:|:--------------:|:--------------:|:--------------:|:--------------:|:--------------:|:--------------:|:---------------:|
| **1**            | 13            | 17             | 19             | No             | No             | No             | No             | No             | No              |
| **2**            | 11            | 15             | 18             | 20             | No             | No             | No             | No             | No              |
| **3**            | 9             | 13             | 17             | 19             | No             | No             | No             | No             | No              |
| **4**            | 7             | 11             | 15             | 18             | 20             | No             | No             | No             | No              |
| **5**            | 5             | 9              | 13             | 17             | 19             | No             | No             | No             | No              |
| **6**            | 3             | 7              | 11             | 15             | 18             | 20             | No             | No             | No              |
| **7**            | 2             | 5              | 9              | 13             | 17             | 19             | No             | No             | No              |
| **8**            | T             | 3              | 7              | 11             | 15             | 18             | 20             | No             | No              |
| **9**            | T             | 2              | 5              | 9              | 13             | 17             | 19             | No             | No              |
| **10**           | T             | T              | 3              | 7              | 11             | 15             | 18             | 20             | No              |
| **11**           | D             | T              | 2              | 5              | 9              | 13             | 17             | 19             | No              |
| **12**           | D             | T              | T              | 3              | 7              | 11             | 15             | 18             | 20              |
| **13**           | D             | D              | T              | 2              | 5              | 9              | 13             | 17             | 19              |
| **14**           | D             | D              | T              | T              | 3              | 7              | 11             | 15             | 18              |
| **15**           | D             | D              | D              | T              | 2              | 5              | 9              | 13             | 17              |
| **16**           | D             | D              | D              | T              | T              | 3              | 7              | 11             | 15              |
| **17**           | D             | D              | D              | D              | T              | 2              | 5              | 9              | 13              |
| **18**           | D             | D              | D              | D              | T              | T              | 3              | 7              | 11              |

</details>

</details>

## **The Spellsword**
<details>

- **Prime Requisite:** STR (Must be 9+); INT (Must be 9+)
- **Hit Dice:** D6
- **Weapons:** Any.
- **Armor:** Any, including shields.
- **Special:**
  - **Cleave:** If Spellsword drops a foe to Zero health they may make another immediate melee attack if an available target is within range.
  - **Spell casting:** Start with **Read Magic** and 1+INT modifier number of spells, randomly selected. Magic-users carry spell books containing the formulae for arcane spells.
  - **Using magic items:** Able to use magic scrolls of spells on their spell list. They can also use items that may only be used by arcane spell casters (e.g. magic wands).
  - **Magical research:** Can spend time and money on research to add new spells to their spell book and to research other magical effects. At 7th level, they are able to make temporary magic items. At 9th level, they can make permanent ones.

### **Progression & Saves**
<details>


#### **PROGRESSION**

|             |         |        |        | **Spells** |       |       |       |       |       |
|-------------|---------|--------|--------|------------|-------|-------|-------|-------|-------|
| **Lvl**     | **XP**  | **HD** | **AB** | **1**      | **2** | **3** | **4** | **5** | **6** |
| **1**       | 0       | 1d6    | 1      | 1          | -     | -     | -     | -     | -     |
| **2**       | 4500    | 2d6    | 2      | 2          | -     | -     | -     | -     | -     |
| **3**       | 9000    | 3d6    | 2      | 2          | 1     | -     | -     | -     | -     |
| **4**       | 18000   | 4d6    | 3      | 2          | 2     | -     | -     | -     | -     |
| **5**       | 36000   | 5d6    | 4      | 2          | 2     | 1     | -     | -     | -     |
| **6**       | 72000   | 6d6    | 4      | 3          | 2     | 2     | -     | -     | -     |
| **7**       | 144000  | 7d6    | 5      | 3          | 2     | 2     | 1     | -     | -     |
| **8**       | 270000  | 8d6    | 6      | 3          | 3     | 2     | 2     | -     | -     |
| **9**       | 540000  | 9d6    | 6      | 3          | 3     | 2     | 2     | 1     | -     |
| **10**      | 810000  | 9d6+2  | 6      | 4          | 3     | 3     | 2     | 2     | -     |
| **11**      | 1080000 | 9d6+4  | 7      | 4          | 4     | 3     | 2     | 2     | 1     |
| **12**      | 1350000 | 9d6+6  | 7      | 4          | 4     | 3     | 3     | 2     | 2     |
| **13**      | 1620000 | 9d6+8  | 8      | 4          | 4     | 4     | 3     | 2     | 2     |
| **14**      | 1890000 | 9d6+10 | 8      | 4          | 4     | 4     | 3     | 3     | 2     |
| **15**      | 2160000 | 9d6+12 | 8      | 5          | 4     | 4     | 3     | 3     | 2     |
| **16**      | 2430000 | 9d6+14 | 9      | 5          | 5     | 4     | 3     | 3     | 2     |
| **17**      | 2700000 | 9d6+16 | 9      | 5          | 5     | 4     | 4     | 3     | 3     |
| **18**      | 2970000 | 9d6+18 | 10     | 6          | 5     | 4     | 4     | 3     | 3     |
| **19**      | 3240000 | 9d6+20 | 10     | 6          | 5     | 5     | 4     | 3     | 3     |
| **20**      | 3510000 | 9d6+22 | 10     | 6          | 5     | 5     | 4     | 4     | 3     |


#### **SAVES**

| **Lvl**       | **Death** | **Wands** | **Paralysis** | **Breath** | **Spells** |
|---------------|-----------|-----------|---------------|------------|------------|
| **1**         | 12        | 11        | 12            | 15         | 13         |
| **2**         | 11        | 10        | 12            | 15         | 12         |
| **3**         | 11        | 10        | 12            | 15         | 12         |
| **4**         | 11        | 9         | 11            | 14         | 11         |
| **5**         | 11        | 9         | 11            | 14         | 11         |
| **6**         | 10        | 9         | 10            | 14         | 11         |
| **7**         | 10        | 9         | 10            | 14         | 11         |
| **8**         | 9         | 8         | 9             | 13         | 10         |
| **9**         | 9         | 8         | 9             | 13         | 10         |
| **10**        | 9         | 7         | 8             | 12         | 9          |
| **11**        | 9         | 7         | 8             | 12         | 9          |
| **12**        | 8         | 7         | 8             | 12         | 9          |
| **13**        | 8         | 7         | 8             | 12         | 9          |
| **14**        | 7         | 6         | 7             | 11         | 8          |
| **15**        | 7         | 6         | 7             | 11         | 8          |
| **16**        | 7         | 5         | 6             | 10         | 7          |
| **17**        | 7         | 5         | 6             | 10         | 7          |
| **18**        | 6         | 5         | 5             | 10         | 7          |
| **19**        | 6         | 5         | 5             | 10         | 7          |
| **20**        | 5         | 4         | 4             | 9          | 6          |


</details>

</details>

## **The Bard**
<details>

- **Prime Requisite:** CHA (Must be 9+)
- **Hit Dice:** D6
- **Weapons:** Any.
- **Armor:** Any, including shield.
- **Special:**
  - **Bard Songs:** All Bards have the ability to play and sing songs which can influence not just the mood but even the abilities of those who hear them. When performed, these songs typically apply a bonus of some sort to die rolls made by allies of the Bard, or a penalty to his or her enemies.  This is referred to as the "bonus" of the song. Bards begin play knowing One Bard Song plus one additional song for each point of his or her Charisma bonus. At 3rd level and at each odd level afterwards, the Bard learns an additional song. Songs begin with a bonus of +1 (applied as a negative number when used against an enemy) and goes up every 4 levels.
  - **Keen Hearing:** +1 to Hear Noises checks.
  - **Lore:** 2-in-6 chance of knowing a bit of lore about a creature, place, or item when first encountering it. Automatic and rolled by Ref.

### **Progression & Saves**
<details>


#### **PROGRESSION**

| **Lvl**     | **XP**  | **HD** | **AB** | **Songs** | **Bonus** |
|-------------|---------|--------|--------|-----------|-----------|
| **1**       | 0       | 1d6    | 1      | 2         | 1         |
| **2**       | 1500    | 2d6    | 1      | 2         | 1         |
| **3**       | 3000    | 3d6    | 2      | 3         | 1         |
| **4**       | 6000    | 4d6    | 2      | 3         | 2         |
| **5**       | 12000   | 5d6    | 3      | 4         | 2         |
| **6**       | 24000   | 6d6    | 3      | 4         | 2         |
| **7**       | 48000   | 7d6    | 4      | 5         | 3         |
| **8**       | 90000   | 8d6    | 4      | 5         | 3         |
| **9**       | 180000  | 9d6    | 5      | 6         | 3         |
| **10**      | 270000  | 9d6+2  | 5      | 6         | 4         |
| **11**      | 360000  | 9d6+4  | 5      | 7         | 4         |
| **12**      | 450000  | 9d6+6  | 6      | 7         | 4         |
| **13**      | 540000  | 9d6+8  | 6      | 8         | 5         |
| **14**      | 630000  | 9d6+10 | 6      | 8         | 5         |
| **15**      | 720000  | 9d6+12 | 7      | 9         | 5         |
| **16**      | 810000  | 9d6+14 | 7      | 9         | 6         |
| **17**      | 900000  | 9d6+16 | 7      | 10        | 6         |
| **18**      | 990000  | 9d6+18 | 8      | 10        | 6         |
| **19**      | 1080000 | 9d6+20 | 8      | 11        | 7         |
| **20**      | 1170000 | 9d6+22 | 8      | 11        | 7         |


#### **SAVES**

| **Lvl**   | **Death** | **Wands** | **Paralysis** | **Breath** | **Spells** |
|---------------|-----------|-----------|---------------|------------|------------|
| **1**         | 12        | 13        | 14            | 15         | 17         |
| **2**         | 11        | 12        | 14            | 15         | 16         |
| **3**         | 11        | 12        | 14            | 15         | 16         |
| **4**         | 11        | 11        | 13            | 14         | 15         |
| **5**         | 11        | 11        | 13            | 14         | 15         |
| **6**         | 10        | 11        | 12            | 14         | 15         |
| **7**         | 10        | 11        | 12            | 14         | 15         |
| **8**         | 9         | 10        | 12            | 13         | 14         |
| **9**         | 9         | 10        | 12            | 13         | 14         |
| **10**        | 9         | 9         | 11            | 12         | 13         |
| **11**        | 9         | 9         | 11            | 12         | 13         |
| **12**        | 8         | 9         | 10            | 12         | 13         |
| **13**        | 8         | 9         | 10            | 12         | 13         |
| **14**        | 7         | 8         | 10            | 11         | 12         |
| **15**        | 7         | 8         | 10            | 11         | 12         |
| **16**        | 7         | 7         | 9             | 10         | 11         |
| **17**        | 7         | 7         | 9             | 10         | 11         |
| **18**        | 6         | 7         | 8             | 10         | 11         |
| **19**        | 6         | 7         | 8             | 10         | 11         |
| **20**        | 5         | 6         | 8             | 9          | 10         |


</details>

### **Bard Songs**
<details>

All Bards have the ability to play and sing songs which can influence not just the mood but even the abilities of those who hear them. When performed, these songs typically apply a bonus of some sort to die rolls made by allies of the Bard, or a penalty to his or her enemies.  This is referred to as the "bonus" of the song. Bards begin play knowing two Bard Songs plus one additional song for each point of his or her Charisma bonus. At 3rd level and at each odd level afterwards, the Bard learns an additional song. Songs begin with a bonus of +1 (applied as a negative number when used against an enemy) and goes up every 4 levels.

Listed below are the basic Bard Songs available to Bard characters with an explanation of each song's benefit. The player should be encouraged to come up with his or her own unique song names and perhaps even come up with some sample lyrics or melody for each. Of course, this is not necessary for actual play but may enhance the role-playing aspect of the campaign.

Songs that are played during the course of combat must be played continuously. Regardless of the Bard's particular playing style, he or she may not utilize a weapon; if Combat Options is being used, the Bard may not engage in Defending.  The Bard may move about, but may not attack during a round in which he or she is performing.  If the Bard is struck in combat, the effect is immediately canceled until the Bard's turn when he or she can resume performing.  If a Bard chooses to end a song (with a flourish), the effect lingers one additional round; this does not apply if the Bard is interrupted.  Note that this allows the Bard to take advantage of the song effect himself on his next turn.

Not all songs require an instrument; if a song says "singing" then no instrument is needed, while if one says "playing" then an instrument is required.  Performing without an instrument applies a penalty of -1 to the song (completely nullifying the effect if the song has a bonus of +1).

### **The Songs**

**Alertness Song (rondo) –** While singing this quiet tune, all allies within 10 feet are less likely to be surprised, reducing the die roll range by 1 (from 1-2 on d6 to a roll of 1 on d6). A second rank of proficiency reduces the chance further to a roll of 1 on d8, and a third rank modifies the roll to 1 on d10. Proficiency in this particular Bard Song may not be increased beyond 3 such ranks. 

**Battle Song, offense (march) –** While playing all allies within 60 feet receive the benefit of +1 to their AB.

**Battle Song, defense (strophic) –** While playing all allies within 60 feet receive the benefit of +1 to their AC. 

**Charms Song (lullaby) –** By playing lullaby, a Bard lulls those listening into a drowsy day-dreamy state. Allies are unaffected, but others have a -1 penalty on saves versus sleep, charms, illusions, suggestions and similar effects.

**Funeral Song (requiem) –** While playing this song, undead creatures (or vile beings from netherworld regions) are more easily turned by Clerics or those with similar powers, granting a +1 to the Turning attempt.

**Healing Rest Song (nocturne) –** By playing periodically during the periods of rest, each allied character including the Bard receives the benefit of an additional point of healing (see p. 51 of the Core Rules).  A major disruption of the rest period will spoil the effect. 

**Laying of Hands Song (hymn) –** While this song of divine inspiration is being played, any allied Clerics within 60 feet receive an additional +1 point per die when casting any healing spell.

**Lock Picking Song (measures) –** By this carefully timed piece, a Bard can assist in the concentration of roguish characters during attempts to Open Locks and in the Removal of Traps (but not location of such traps).  The Thief gets a +1 bonus on such attempts for each rank of proficiency of the Bard.  Any character with such abilities can benefit from the song's effect.

**Magic Weakening Song (elegy) –** When playing this tune, magical effects are easier to shrug off, granting a +1 to any saving throw versus a magical effect (at the very least, all saves vs. Wands and Spells, and any other effect the GM rules is magical).  However, this song affects all within 60 feet of the Bard, whether ally or enemy.  If opposing Bards play the song at the same time, only the highest bonus is applied.

**Morale Improving Song (polonaise) –** While the Bard plays, allies within 60 feet have the benefit of +1 on morale or saves against fear effects. 

**Morale Defeating Song (caprice) –** While the Bard plays, any enemies within 60 feet have a penalty of -1 on morale or saves against fear effects.

**Pied Piper Song (allegro) –** While playing this song, natural animals (including giant varieties) are more easily befriended or calmed by druids or those with similar powers. The druid receives a +1 to his or her Animal Affinity (Turning) attempt. While the song is playing, any animal specific charm or control type spell (as determined by Game Master) has a -1 penalty to the animal's saves.

**Recall Song (lament) –** This calming tune aids concentration and contemplation, allowing a spell caster (including the playing Bard if applicable) to recall a previously cast 1st level spell after a period of 1 hour of meditation while the Bard plays. Neither the Bard nor the spell caster may be interrupted during this time. No one spell caster may benefit from this effect more than once per day, but multiple spell casters may benefit from the effect. Increased proficiency allows spells of higher level to be recalled (up to level 5 spells), but the spell caster may choose lower level spells if desired. Regardless, only one spell can be recalled. 

**Song of Destruction (crescendo) –** When playing this tune, magical effects are much more destructive, granting a +1 to the effective caster level of damaging spells originating within 60 feet of the Bard, modifying spell effects such as range, damage, and/or duration. This song affects both allies and enemies equally. Only the highest proficiency Bard effect applies when multiple bards might play this song at same time. The empowered caster does not receive additional memorized spells or other level derived benefits, only increased effectiveness with their current allotment of damaging spells.

**Travel Song (barcarolle) –** By playing a rhythmic tune during the majority of a day's traveling time, the allied group receives the benefit of +10% additional distance after all other factors are figured. Each additional level of proficiency increases this bonus by an additional +10% (up to +50% maximum).

</details>
</details>
