# ADVENTURE EQUIPMENT
[Back to Wiki](https://gitlab.com/voidcollection/shadows_of_the_abbey/-/wikis/home)

# Equipment Packs
<details>
<summary> Less / More </summary>

- **Bag Option 1 (7gp; Wt: 2.2):**   
Backpack, Weapon Belt, Large Pouch. *Carries:* 45lb
- **Bag Option 2 (11gp, 8sp; Wt: 3.4):**   
Oilskin Satchel, Baldric, Weapon Belt, Large Pouch, Belt Pouch(2). *Carries:* 24lb
- **Adventure  Pack 1 - Adventuring Basics (20gp, Wt. 29)**   
Chalk, small bag of pieces; Grappling hook; Hemp Rope (2×50 ft); Lantern, hooded; Oil (3 flasks); Tent, Small (one man)
- **Adventurer Pack 2 - Ready for Anything (10gp; Wt. 12)**  
Glass bottle; Iron Spikes, 12; Pole, 10 ft wooden; Map or scroll case; Mirror, small metal
- **Cleric Pack (60gp; Wt. 18.3):**  
Bandages (10), Holy symbol*, holy water (1), parchment (2), ink and quill, 1 week rations, tinderbox, waterskin
- **Fighter Pack (21gp; Wt. 22.8)**  
Bandages (5), Oil, cooking pot, 1 week rations, 3 large sacks, tinderbox, torches (6), waterskin, whetstone
- **Magic-User Pack (37gp; Wt. 18.7):**  
writing ink and quill, parchment (5), scroll case(2), spellbook*, hand mirror, 1 week rations, Torches (6), Tinderbox, glass vial
- **Thief Pack (58gp; Wt. 31.2):**  
Candles (12), crowbar, grappling hook, mallet, iron spikes (12), 1 wk rations, large sack, small lens, Thieves' tools, tinderbox, waterskin, whetstone
- **Dungeon Mapper (17gp; Wt. 1.2):**  
10 sheets paper, ink, quill & quill knife, writing board, bag of chalk pieces
- **Camp Cook (24gp; Wt. 23):**  
Iron pan, iron pot, Fire grate, mess kit, common spices, dried meat (2lb), standard rations (5 days), teapot


</details>

# Weapons
<details>
<summary> Less / More </summary>

## **Melee Weapons**
<details>
<summary> Less / More </summary>

*Unless otherwise noted- Small and Medium sized weapons can be held one-handed. Large weapons must be held by two hands to use.*

| **Weapon**                   | **Price** | **Size** |  **Weight** |      **Dmg.**|
|-------------------------------|:---------:|:--------:|:-----------:|--------------|
 | **Axes**                     |           |          |             |              |
 | Hand Axe                     |     4 gp  |      S   |         5   |     1d6      |
 | Battle Axe                   |    7 gp   |     M    |       7     |       1d8    |
 | Great Axe                    |     14 gp |      L   |         15  |  1d10        |
 | **Daggers**                  |           |          |             |              |
 | Dagger                       |   2 gp    |    S     |     1       |     1d4      |
 | Silver Dagger                |   25 gp   |    S     |      1      |       1d4    |
 | **Swords**                   |           |          |             |              |
 | Shortsword / Cutlass         |    6 gp   |     S    |      3      |      1d6     |
 | Longsword / Scimitar         |   10 gp   |    M     |     4       |     1d8      |
 | Two-Handed Sword             |   18 gp   |    L     |     10      |     1d10     |
 | **Hammers and Maces**        |           |          |             |              |
 | Warhammer                    |    4 gp   |     S    |       6     |      1d6     |
 | Mace                         |   6 gp    |    M     |     10      |     1d8      |
 | Maul / Great Mace            |    10 gp  |     L    |      16     |      1d10    |
 | **Spears and Polearms**      |           |          |             |              |
 | Spear                        |    5 gp   |     M    |      5      |     1d6 / 1d8 2H      |
 | Lance                        |    10 gp  |     L    |      10     |      1d8     |
 | Quarterstaff                 |    2 gp   |     L    |      4      |      1d6     |
 | Pole Arm                     |    9 gp   |     L    |      15     |      1d10    |
|  **Chain and Flail**          |           |          |             |              |
|  Chain††                      |    9 gp   |     M    |      3      |      1d4     |
|  Flail                        |    8 gp   |     M    |      6      |      1d8     |
|  Great Flail                  |   12 gp   |     L    |     15      |     1d10     |
|  Whip                         |    3 gp   |     M    |      2      |      1d3     |
|  **Other Weapons**            |           |          |             |              |
|  Club/Cudgel/Walking Staff    |    2 sp   |     M    |      1-3    |      1d4     |

\* These items weigh little individually. Ten of these items weigh one
pound.

\*\* This weapon only does subduing damage -- see **Basic Fantasy RPG
Core Rules**, p. 48.

\(E\) Entangling: This weapon may be used to snare or hold opponents

† Silver tip or blade, for use against lycanthropes.

†† Minimum weight and price

---
### **Weapon Descriptions**
<details>
<summary> Less / More </summary>

**Axes:** A group of tools and weapons designed to deliver heavy, chopping blows.

- *Hand Axes* are small, light axes, generally with good balance for throwing.  Examples: hatchet, tomahawk, francisca.
- *Battle Axes and Great Axes* differ in scale, but share many of the same features.  The style of the axe head (crescent, bearded, double-bit, etc.) will be a matter of setting and preference.

**Daggers:** A variety of short blades (typically under 12” in length).  While most daggers will have sharpened edges, these weapons are most often used as thrusting weapons.  Daggers are assumed to be balanced for throwing, unless otherwise specified by the GM.  Daggers include the dirk, seax, poniard, stiletto (a spike-like dagger), tanto, punch daggers, and smaller types of katar.

**Swords:** A variety of hilt blade weapons, designed for cutting or thrusting strikes. The different types of swords are based on size, shape, and use.  Note that at each size, there are two versions; one straight-bladed, one curved.

- *Short Swords* are straight thrusting weapons, much like a dagger, but with a longer blade (typically around 2 feet). Some examples may in fact be oversized daggers, including cinquedea, gladius, xiphos, and katar.  The Cutlass group represents short blades – curved or straight single-edged – designed for hacking or slashing rather than thrusting.  This includes the aforementioned cutlass, hangars, and machetes, as well as short reverse-curve blades such as the kukri and kopis.
- *Long Swords* represent a wide range of straight bladed swords, primarily designed for one-handed use.  These weapons will typically be between 3 and 4 feet in length and designed for both slashing and thrusting.  Examples include the broadsword, pata, spatha, and jian.  The Scimitar group includes single-edged, curved swords, designed primarily for slashing – though specific types may be straight or balanced enough to be an effective thrusting weapon.  This group includes saber, falchion, dao, and katana.
- *Two-Handed Sword* is simply any sword requiring two hands to use – due to weight and balance.  These will typically have an elongated handle or hilt, and blades 3½’ or longer. Most European-style fantasy two-handers tend to be large, straight double-edged blades, including claymores, zweihander, flamberge, and other “greatswords.”  This also includes curve-bladed single-edged swords such as kriegsmesser, nodachi, great scimitar, and dadao.

**Hammers & Maces:** Weapons that use weight and force to deliver damage.  In game terms, the differences are primarily one of style, although hammers may be balanced for throwing, much like a hand axe. Maces may come with smooth, studded, or flanged heads.
- *Maul and Great Mace* are larger, two-handed versions of the hammer and mace, respectively.

**Spears and Polearms:** Covers a variety of hafted or pole-handled weapons, most based on the simple Spear.
- *Quarterstaff* is included here as it functions as a headless spear (or if you prefer, a spear is a quarterstaff with a pointy bit on one end).
- *Lance* is a larger, sturdier spear, and not suited for throwing.  A properly balanced lance may be used one-handed while mounted.
- *Pole arms* place a weapon head on a longer pole, usually 6’ - 8’, sometimes combining different attack forms: blade/axe, spear, pick/spike, bludgeon, or hook.  Examples & combinations: Glaive, Voulge, Bardiche, and Bisento (blade), Halberd (axe & spear), Guisarme (blade and hook), Partisan (Spear), Ranseur (Spear – akin to boar spear), Bill (Blade and Hook), Bec de Corbin (bludgeon and spike), and Fork (Spike and “hook”).

**Chain Weapons:** Incorporate a length of chain or other flexible material to allow the weapon to swing, using momentum to deliver more damage.
- *Chain* is the simplest form – a length of chain, swung as a weapon.
- *Flails* extend this by adding a weighted striking head of some kind, from long heads mounted on a short cord or hinge, to a variety of ball and chain weapons this includes weapons like nunchaku.  Most flails are usable by Clerics, though spiked flails (similar to a morningstar) may be prohibited.
- *Whips* operate on similar principle, with an even greater emphasis on speed – to the point that it acts more as a slashing weapon.  Whips do poorly against hard armors or thick hides, and will not do damage to targets with an armor or natural AC of 14 or higher.

**Other Weapons:** Weapons that don't fit the traditional categories above.
- *Clubs, Cudgels, and Walking Sticks* represent a variety of simple bludgeoning weapons and objects. This includes the basic club, gentleman's cane, hiking stick, shillelagh, knobkerry, or wizard's staff. Because of their design and lesser weight, these weapons are less effective than a mace or hammer of similar proportion.  A walking stick, staff, or cane may be purchased with a silver head or handle.
-
</details>
</details>

---
---

## **Ranged Weapons**

<details>
<summary> Less / More </summary>


| **Weapon**                   | **Price** | **Size** |  **Weight** |      **Dmg.**|
|-------------------------------|:---------:|:--------:|:-----------:|--------------|
| **Bows**                      |        |       |          |         |
| Shortbow                      | 25 gp  | M     | 2        |         |
| - Shortbow Arrow             | 1 sp   |       | \*       | 1d6     |
| - Silver Shortbow Arrow      | 2 gp   |       | \*       | 1d6     |
| Longbow                       | 60 gp  | L     | 3        |         |
| - Longbow Arrow              | 2 sp   |       | \*       | 1d8     |
| - Silver Longbow Arrow       | 4 gp   |       | \*       | 1d8     |
| Light Crossbow                | 30 gp  | M     | 7        |         |
| - Light Quarrel              | 2 sp   |       | \*       | 1d6     |
| - Silver Light Quarrel       | 5 gp   |       | \*       | 1d6     |
| Heavy Crossbow                | 50 gp  | L     | 14       |         |
| - Heavy Quarrel              | 4 sp   |       | \*       | 1d8     |
| - Silver Heavy Quarrel       | 10 gp  |       | \*       | 1d8     |
| Hand Crossbow                 | 150 gp | S     | 3        |         |
| - Hand Quarrel               | 2 sp   |       | \*       | 1d3     |
| - Silver Hand Quarrel        | 5 gp   |       | \*       | 1d3     |
| **Slings & Hurled Weapons**   |       |       |          |         |
| Sling                         | 1 gp   | S     | \*       |         |
| - Stone                       | n/a    |       | \*       | 1d3     |
| - Bullet                     | 1 sp   |       | \*       | 1d4     |
| - Silver Bullet              | 1 gp   |       | \*       | 1d4     |
| Bola (E)                      | 2 gp   | S     | 2        | 1d3     |
| Dart / Throwing Blade         | 1 gp   | S     | \* to ½  | 1d3     |
| Javelin                       | 1 gp   | M     | 2        | 1d4     |
| Blowgun                       | 2 gp   | M     | 2        |         |
| Dart                          | 1 sp   |       | \*       | 1d3     |
| Net (E)                       | 20 gp  | M     | 5        | \-      |
| Random Object (thrown)        | \-     | S-M   | 1 to 5   | 1d3     |

\* These items weigh little individually. Ten of these items weigh one
pound.

\*\* This weapon only does subduing damage -- see **Basic Fantasy RPG
Core Rules**, p. 48.

\(E\) Entangling: This weapon may be used to snare or hold opponents

† Silver tip or blade, for use against lycanthropes.

### Missile Weapon Ranges
<details>
<summary> Less / More </summary>

| **Weapon**    |   **Short (+1)** | **Medium (0)** |   **Long (-2)** |
|---------------|:----------------:|:--------------:|:----------------|
|  Longbow    |               70     |        140     |          210 |
|  Shortbow   |               50      |       100    |           150 |
|  Heavy Crossbow  |          80     |        160   |            240 |
|  Light Crossbow  |          60     |        120    |           180 |
|  Hand Crossbow      |       30     |        60     |           90  |
|  Bola              |        20    |         40     |           60  |
|  Blowgun          |         10    |         20     |           30  |
|  Dagger          |          10    |         20     |           30  |
| Dart             |         10     |        20      |          30   |
| Throwing Blade   |         10     |        20     |           30    |
| Warhammer          |          10     |        20     |           30   |
| Hand Axe        |          10      |       20     |           30   |
| Oil or Holy Water   |      10      |       30     |           50   |
| Sling               |      30      |      60      |          90    |
| Spears         |  10      |       20    |            30   |
| Javelin        |           20      |       40    |            60   |
| Net            |           10      |       15     |           20     |
| Random Object    |         \-       |      10        |        20     |
  ------------------------- -------------- ----------------- ------------

Missile weapon ranges are given in feet. In the wilderness, substitute
yards for feet. If the target is at the Short range figure or closer,
the attacker gets a +1 attack bonus. If the target is further away than the Medium range figure, but not beyond the Long range figure, the attacker receives a -2 attack penalty.

</details>

### **Weapon Descriptions**

<details>
<summary> Less / More </summary>

**Bows:** Slender lengths (staves) of wood or other materials, bent and fastened at the ends with a shorter length of animal tendon or other strong fiber. The tension of the bent staff is what provides the power to launch arrows great distances. Careful selection of materials and changing the length of the stave can increase the power and range of these weapons.
- *Short bows* have staves around 5 foot unstrung, though shorter staves of more flexible materials may be favored by smaller characters.
- *Long bows* are typically around 6 feet unstrung, though longer staves are not unheard of.

**Crossbows:** Essentially short, heavy bows (laths) of wood or metal set across a stock or tiller.  Much of the popularity of the crossbow comes from its ease in use – and much shorter training time for men-at-arms – compared to bows.  The mechanical lock & pulling mechanisms means higher tensions can be drawn and held without tiring the archer.  This allows for ready fire, and with greater ranges than a regular bow, at the expense of a slower rate of fire.
- *Light Crossbows* are hand- or lever-drawn, and have a rate of fire of 1 per 2 rounds (one round to draw and set, ready to fire the next).  Man-sized creatures (including Dwarves) may attempt to fire a light crossbow with one hand, but at -3 attack roll.
- *Heavy Crossbows* are a more powerful weapon, usually requiring composite or steel laths. Because of the high draw strength, heavy crossbows can fire 1 per 3 rounds (2 rounds for drawing and setting the bolt).  Heavy crossbows are often mechanically drawn, though some may use a braced draw (holding the bow with a stirrup, and using the arms and legs to draw the string back).
- *Hand Crossbows* are something of a novelty – rare, fairly delicate, and difficult to craft.  They are easy to draw and set (firing once per round), but have short range and less penetration (damage) compared to the heavier models. Their small size and ease of use makes these popular for assassination using poisoned bolts. As the name suggests, these can be wielded with one hand.  The short bolts require a special-sized bolt case.

**Slings:** Represent a variety of simple tools that arc or launch a small projectile at high speed.  The cup on a length of cord is most common, but also includes short levers and elastic-powered devices.  Depending on the exact design, most slings can be “fired” one-handed, but take two hands to load. Stones are often chosen for shape and weight (and fairly easily found while traveling, particularly around rivers).  A sling may be used to hurl small, roundish objects (gems, eggs, tiny jars), but with diminished range and accuracy.

**Blowguns:** Long, hollow tubes of metal or wood, which use the wielders' own lung power to fire small darts (or potentially other projectiles).  These weapons are typically around 4 feet in length, but lengths of up to 7 feet are not unheard of. Traditionally a hunting weapon, a blowgun will occasionally find use in combat, and are particularly popular among assassins.

**Hurled Weapons:** Weapons meant to be thrown or hurled. While several melee weapons noted above may be hurled, the following weapons are made primarily to be thrown  Some hurled weapons may be usable in melee, but with a -2 to strike.
- *Throwing blades* are light, edged or pointed throwing weapons. This includes throwing knives, chakram, and large shuriken.
- *Darts* are short, weighted missiles, similar in size and design to a blowgun dart or hand crossbow bolt.
- *Javelins* are lightweight cousins of the spear, with better range but inflicting less damage. Includes Roman pilum.
- *Bolas* are made from 2-3 weights attached by lengths of cord or leather. It is a hunting weapon designed primarily to snare targets. If an entangle attack is made, the target must save vs. Paralysis to avoid being tripped, immediately losing movement and DEX AC bonus until he or she can be freed.  Note that for flying creatures, a 'trip' means that one or more wings have been pinned. The flying target will start to fall until it can free itself or crashes; see **Falling Damage** on page 52 of the **Basic Fantasy Core Rules** for more details. The target can be freed (or free itself if possible) with a full round of action, or in place of moving or attacking if it can find a way to cut or break the cord. Quadrupeds receive a +2 on their save.
- *Nets* are only used to snare opponents. A net for combat use is typically about 10' in diameter, with a series of weights around the edges to help spread the net in flight, and hold down the edges to better trap targets.  On a successful hit, the target must save v. Death Ray or be caught.  It will take 1d4+1 rounds to get free, or 1 round if he or she can be cut free.  Strong creatures may attempt to tear the net apart.  This is similar to a Strength check against a locked door to tear free.  A net can normally only catch a single man-sized target when thrown.  Larger nets tend to be difficult to manage in combat, but make excellent traps.  Nets can be used in melee, but only to attempt to tangle a weapon.  It must be thrown to capture a target.

</details>
</details>
</details>

# Armor

<details>
<summary> Less / More </summary>

| **Armor**       | **AC** |**Cost**|**Weight**|
|-----------------|:------:|:------:|:----------------:|
| Leather         | 13     | 20     |15                |
| Chainmail       | 15     | 60     |40                |
| Plate mail      | 17     | 300    |50                |
| Shield          | +1     | 7     |5               |

</details>


# General Equipment
<details>
<summary> Less / More </summary>

|**Item**|**Price**|**Wt**|
| :- | -: | -: |
|**Cooking**|||
|Cooking Supplies (per week)|3 gp|7|
|Fire grate|3 gp|5|
|Fishhook|1 sp|\*\*|
|Iron Pan (Frying)|8 sp|2|
|Iron Pot|5 sp|3|
|Rations, Dry, one day|2 gp|2|
|Tea pot|3 sp|2|
|Tripod, cooking|3 gp|10|
|**Class Items**|||
|Holy Symbol|25 gp|\*|
|Holy Symbol, Ornate|50 gp|1|
|Holy Water, per vial|10 gp|\*|
|Quiver or Bolt case (20)|1 gp|1|
|Thieves' picks and tools|25 gp|1|
|Whetstone|1 gp|1|
|**Dungeon Exploration**| ||
|Bed Roll|6 gp|5|
|Candles, 12|1 gp|\*|
|Chalk, colored, small bag of pieces|4 gp|\*|
|Chalk, small bag of pieces|2 gp|\*|
|Charcoal sticks|1 gp|\*|
|Coal Keeper|2 gp|1|
|Cord/ Strap, per 3'|1 sp|1|
|Flask, Silver|20 gp|1|
|Flask, Steel|2 gp|1|
|Grappling Hook|2 gp|4|
|Hammock|5 gp|5|
|Iron Spikes, 12|1 gp|1|
|Jar or Bottle, Ceramic|4 sp|1|
|Jar or Bottle, Glass|12 sp|1|
|Ladder, Rope, 25ft|3 gp|10|
|Lantern, Bullseye|14 gp|3|
|Lantern, Hooded|8 gp|2|
|Lens, small|8 gp|\*|
|Lock, Excellent|200 gp|2|
|Lock, Good|100 gp|1|
|Lock, Poor|20 gp|1/2|
|Magnet, small|1 sp|\*|
|Magnifying glass|100 gp|\*|
|Mirror, small metal|7 gp|\*|
|Mirror, small silver|25 gp|\*|
|Nails, Iron (20)|2 sp|1|
|Nails, silver (20)|2 gp|1|
|Needle, magnetized|1 gp|\*\*|
|Oil (per flask)|1 gp|1|
|Fine Paper or Vellum, per sheet|4 gp|\*\*|
|Paper or Parchment, per sheet|1 gp|\*\*|
|Rope, Hemp (per 50 ft.)|1 gp|5|
|Rope, Silk (per 50 ft.)|10 gp|2|
|Signal whistle|1 gp|\*\*|
|String / Twine, 100 ft|2 sp|1|
|String / Twine, Silk, 100 ft|6 sp|1/2|
|Vial, glass|1 gp|\*|
|Wax, beeswax|3 sp|1|
|Wooden Stake|2 cp|2|
|**Health**|||
|Bandages|1 sp|\*\*|
|Crutches|1 gp|4|
|Oil (per flask), scented / rubbing|5 gp|2|
|Perfume (per vial)|5 gp|1/2|
|Razor|1 gp|\*|
|Soap (per lb.)|5 sp|1|
|Soap, perfumed (per lb.)|5 gp|1|
|**Outdoor**|||
|Air Bladder|15 gp|3|
|Caltrops|1 gp|2|
|Crampons|4 gp|\*|
|Hunter's Horn|5 gp|2|
|Piton, climbing|2 sp|\*\*|
|Skates|10 gp|2|
|Snow skis|15 gp|7|
|Tent, Large (ten men)|25 gp|20|
|Tent, Pavilion|100 gp|40|
|Tent, Small (one man)|5 gp|10|
|Trap, small animal|4 gp|1|
|Trap, medium animal|7 gp|2|
|Trap, large animal|14 gp|5|
|Travois|5 gp|15|
|**Personal Equipment**|||
|Journal (blank)|20 gp|1|
|Map or scroll case|1 gp|1/2|
|Mess Kit|8 gp|2|
|Sealing Wax|3 sp|\*|
|Signet ring or personal seal|5 gp|\*\*|
|Smoking Pipe|1 gp|\*|
|Smoking pouch|1 gp|\*|
|Tinder Box, Flint & Steel|3 gp|1|
|Torches, 6|1 gp|1|
|Wineskin/Waterskin|1 gp|2|
|Wineskin/Waterskin, gallon|3 gp|7|
|Winter blanket|1 gp|3|
|Writing ink (per vial)|8 gp|1/2|
|**Tools**|||
|Bell, small|1 gp|\*|
|Bellows|10 gp|3|
|Block and tackle|5 gp|2|
|Bucket (up to 5 gal)|5 sp|2 (15)|
|Canvas (per sq. yard)|4 sp|5|
|Chain (per ft.), Heavy|4 gp|10|
|Chain (per ft.), Light|3 gp|5|
|Chisel|2 gp|2|
|Crowbar (3')|2 gp|10|
|Fishing net, 10 ft. sq.|4 gp|1|
|Grease Pot|5 gp|5|
|Hammer or Mallet|3 gp|2|
|Hand Drill|10 gp|3|
|Hourglass (Hour)|25 gp|3|
|Ladder, 10 ft.|1 gp|20|
|Marbles, Bag|8 sp|1|
|Needle, sewing|5 sp|\*\*|
|Paint, per gallon|1-2 gp|4|
|Paint, small pot|2 sp|1|
|Pick Axe, Mining|4 gp|7|
|Pliers|1 gp|1|
|Pole, 10' Collapsing|50 gp|15|
|Pole, 10' wooden|1 gp|10|
|Scissors|5 sp|1|

\* These items weigh little individually.  Ten of these items weigh one pound.

\*\* These items have no appreciable weight and should not be considered for encumbrance unless hundreds are carried.

# Explanation of Equipment

<details>

## Cooking

**Cooking Supplies** are odds and ends needed for cooking on the road, including some basic sundries, common seasonings, smoking wood, and so on.

## Rations

**Dry Rations** may consist of dry bread, hard cheese, dried fruit, nuts, beans, jerky, or any other food which will not “go bad” in less than about a month (if not longer).  Dry rations are generally sold in quantities sufficient for one character for a week, and are packaged in waxed or oiled cloth to protect them.

## Class

**Holy Water** is explained in the **Encounter** section of the **Basic Fantasy RPG Core Rules**.

A **Quiver** is an open container used to hold arrows.  A **Bolt Case** is a similar sort of container for crossbow bolts.  In either case, the standard capacity is 20 missiles.  The length of a quiver or bolt case must match the length of the ammunition for it to be useful; therefore, there are longbow and shortbow quivers and light and heavy crossbow bolt cases.  The price is the same regardless of the type.

A **Whetstone** is used to sharpen and maintain edged weapons such as swords, daggers, and axes.

## Dungeon Exploration

A **Candle** will shed light over a 5' radius, with dim light extending 5' further.  A normal candle will burn about 3 turns per inch of height.

**Chalk** is useful for “blazing a trail” through a dungeon or ruin to ensure that the adventurers can find their way back out again.

**Charcoal** consists of** pieces of carbonized wood, which can be used like chalk (making black markings), or may be added to tinder.

A **Coal Keeper** is a small, lined ceramic pot designed to hold and keep a small coal or ember lit for several hours.  While this will resist moisture and wind, immersion will extinguish the coal immediately

A **Cord/Strap** is a short length of thin rope, leather, or a short belt for tying something to an arm, leg, or other accessories.  This can secure a scabbard to an arm or leg, or attach a pouch to a baldric, or the strap of a backpack.

**Flasks** are metal containers with stoppers that hold between 8 and 12 oz. of liquid.

**Iron Spikes** are useful for spiking doors closed (or spiking them open) and may be used as crude pitons in appropriate situations.

**Jars** include lids or stoppers and have a volume of 8-16 oz. (double or halve price for larger/smaller volumes).  They are good for wet or dry materials, but prone to breakage.

A **Rope Ladder** is a regularly knotted rope, or two lengths of rope, strung with rungs, with a single line at top for a hook.  It typically has an 850 lb capacity.

A **Lantern** will provide light covering a 30' radius; dim light will extend about 20' further.  A lantern will consume a flask of oil in 18+1d6 turns.  

A **Hooded Lantern** has a shutter mechanism to close off the light and prevent it from being seen.

A **Lantern (Bulls-eye)** is** similar to a hooded lantern, only it is closed on all but one lensed side.  This lamp projects light up to 30', and 30' at its widest, and includes a shutter.

A **Lens** enlarges the image of an object; a **Magnifying Glass** consists of a large lens held in a frame with a handle.  The bare lens is smaller and is limited to 2-3x magnification.  A magnifying glass is of higher quality with a minimum of 5x magnification.  Both can be used to attempt to start fires using strong sunlight.

A **Mirror** is useful in a dungeon environment for many reasons.  For instance, it is the only way to look at a medusa without being turned to stone.  Mirrors are also useful for looking around corners, and can be used outdoors to send signals using reflected sunlight.

**Paper** of decent quality, refined papyrus, cotton, cloth, or wood pulp, or parchment – depending on the setting & availability.  **High quality paper** and **vellum** is not necessarily more durable, but will take ink better, and is required for magic writing (both books and scrolls).

**Hemp Rope** is ½ inch in diameter and has a breaking strength of 1,600 pounds.  A safe working load for a rope is normally one-quarter of the breaking strength.  One or more knots in a rope cut the breaking strength in half.  This does not affect the safe working load because knots are figured into the listed one-quarter ratio.

**Silk Rope** is about 3/8 inch in diameter and has a breaking strength of 1,600 pounds, although it weighs considerably less than hemp rope.  The notes regarding rope strength given for hemp rope, above, apply here also.

**String / Twine** holds up to 30 lb.

**Wax, beeswax** is a softer wax, useful for making impressions, hasty patches, stuffing in ears, etc.

## Health

**Bandages** are** clean, rolled linens, which when used are enough to prevent blood loss for one character's wounds from one combat encounter.

## Outdoors

An **Air Bladder** is** a leather pouch with a tube fitting into the user’s mouth. It will hold an extra breath of air, allowing a swimmer to extend their range.  The user must overcome the buoyancy the air bladder provides to be able to stay under water.

Along with helping breathe underwater, the air bladder may also be used to help a character float.  This allows a moderately-encumbered character to float; a heavily-encumbered character will have to drop enough weight to be moderately-encumbered in order to float.  An unencumbered character will need to make a swim check every other round instead of every round.

**Caltrops** are metal spikes that look like large jacks, shaped so that there is always one sharp point facing up. One bag can be scattered over a 5' area. If a creature moves through or spends a round fighting in an area scattered with caltrops, there is a 2 in 6 chance it will step on one. Heavy boots reduce the chances of stepping on a caltrop to 1 in 6. Stepping on a caltrop deals 1 point of damage and reduces the creature's movement rate by half for 24 hours, or until it receives some form of healing. If the creature is attempting to move or charge through the caltrops and it steps on one, the pain forces it to stop, unless the creature is mindless or cannot feel pain. 

**Crampons** are spikes for boots which improve the wearer's chances at climbing and maintaining balance on ice.

A **Hunter's Horn** is a signal horn, often made from brass or the actual horn of an animal.

A **Piton** is a spike for climbing; designed to wedge into rock or ice cracks.

A **Trap** generally refers to a leg-hold trap for fur-bearing animals of various sizes.  The jaws of the large-sized traps have teeth to help hold the large animals they are designed for.    When used, a trap is staked to the ground with a rope or chain.  These traps are typically hidden and will surprise on a 1-2 on 1d6.  They are set by pressing down on the spring and then setting the trigger.  There is a 1 in 20 chance the trap will trip while the trapper is setting it, for a non-Thief.  A natural roll of 1 means the trap misfired and the trapper has pinched their finger, hand or arm depending on the size of the trap. 

|**Traps**|**Price**|**Size**|**Wt**|**Damage**|
| :- | -: | :-: | :-: | :-: |
|Small|4 gp|S|5|1d4|
|Medium|7 gp|M|7|1d8|
|Large|14 gp|L|15|1d12|

A **Travois** is a sort of litter designed to be pulled by a person or animal.  Anything loaded on the travois (including people) only counts 1/3 of its weight against carrying capacity.  Anyone hauling a travois moves as if *heavily encumbered*, regardless of how much weight is actually being pulled.

## Personal Equipment

A **Journal** is a leather bound paper book 6” by 9” in size and 50 pages long.  It is good for making notes, sketches, or small maps.  The paper is **not** spellbook quality.

A **Map or Scroll Case** is a tubular oiled leather case used to carry maps, scrolls, or other paper items.  The case will have a water-resistant (but not waterproof) cap which slides over the end and a loop to allow the case to be hung from a belt or bandolier.  A standard scroll case can hold up to 10 sheets of paper, or a single scroll of up to seven spells.

A **Mess Kit** includes the essential utensils for eating while camping.  Most often these consist of a trencher (something between a bowl and a plate), spoon, knife, fork if appropriate, and a small wood, tin, or brass cup.

A **Smoking Pipe & Pouch** are considered Halfling essentials, and are even carried by most Halfling adventurers.  The pipe is hand sized or a little longer, and consists of a short tube with a bowl at the end which is filled with tobacco.  When lit, the smell is much more aromatic than other smoking implements which may be part of its appeal.  A pipe and several days of tobacco can be stored in the pouch.

**Sealing wax** is a kind of shellac or paraffin wax that is softened when heated, but dries solid to seal containers (bottles), or written messages, letters, and even scrolls.

A **Tinderbox** is generally purchased with a **flint and steel**.  The flint, which is a hardened piece of hard rock, is struck vigorously against a C-shaped piece of high-carbon steel.  When done correctly, hot sparks will fly from the flint and steel into the tinder, hopefully starting a fire.  The best tinder is a dried piece of prepared tinder fungus, carried in the tinderbox to keep it dry. Char cloth, hemp rope, or even very dry grass can substitute for the tinder fungus if prepared tinder fungus is not available.  The time required to start a fire should be determined by the GM according to the prevailing conditions, but under ideal conditions, starting a fire with a flint, steel, and tinder takes about a turn.

A **Torch** is normally fashioned out of a short wooden stick with tightly wrapped rags at one end that have been soaked in animal fat or paraffin to allow it to burn for extended periods of time. When lit, the burning wick sheds light over a 30' radius, with dim light extending about 20' further, and burns for 1d4+4 turns.  Of course, a torch is also useful for setting flammable materials (such as cobwebs or oil) alight.

A **Wineskin/Waterskin** is a container for drinking water or wine; though generally water is taken into a dungeon or wilderness environment.  The standard waterskin holds one quart of liquid, which is the minimum amount required by a normal character in a single day.  If adventuring in the desert or other hot, dry areas, a character may need as much as ten times this amount.  Note that the given 2 pound weight is for a full skin, and an empty skin has negligible weight.

**Writing ink** is made of charcoal, various pigments, resins, and waxes, and is stored in a small ceramic jar or tin sufficient for 50 pages of normal writing.

## Tools

A **Bellows** is a fire tending tool that is about a foot long with a leather air bag between two wooden handles.  When the handles are squeezed together, air will blow out the nozzle of the air bag, increasing the heat of a fire as well as clearing dust or ash.

A **Block and tackle** is a system of two or more pulleys with a rope threaded between them to reduce the weight of loads that are being lifted.  The effective weight is reduced by ¼ with one pulley (lifting at load up 50 feet with 100 feet of rope), ½ with two pulleys (with 200 feet of rope), and by ⅔ with three pulleys (with 400 feet of rope).  

A **Crowbar** a bar of iron that curves at one end in a kind of hook shape.  The end itself is flattened with a narrow split on the tip to help pull nails out of a board.  The other end is flattened without a split to help pry one piece of wood from another or force open a jammed or locked door, or heavy objects such as a stone coffin lid.  In game terms, add one to the die roll range when forcing a door with this tool.  For example, a character with 16 Strength would open doors on 1-3 on 1d6; with a crowbar, the range becomes 1-4 on 1d6.

**Grease** is a thick, slippery lubricant made from made from animal fat, or oil from the ground.  It is sold in a tin or jar, and is useful for cooking or lubrication. Not all forms are flammable or edible.

A **Pick Axe** is a “T” shaped tool with a wooden handle, and a combination metal hammer and pick at the end.  It is designed for breaking up stones and soil or prying things open.   It is also used for prospecting or ice climbing.  It deals 1d3 points of damage if used as a weapon.

**Collapsing Pole** is a handy invention consisting of ten 1-foot wood sections, and two metal end caps.  Each piece is threaded so that the pieces can be combined to make a pole of any needed length.  Collapsing poles from the same maker may be combined for longer reach.

</details>
</details>

---
---

# Beasts & Vehicles
<details>
<summary> Less / More </summary>

## **Beasts**

<details>
<summary> Less / More </summary>

| **Animal**     |  **Cost**  |  **Miles/Day**  |   **Movement**  |  **Hvy Load**   |
|----------------|:----------:|:---------------:|:---------------:|:---------------:|
| Mule	         |    30      |	    24/6        |      40'(10')   |     301-600     |
| Horse (draft)  |    120     |     36/6        |      60'(10')   |	351-700     |
| Horse (riding) |    75      |	    48/6        |      80'(10')   |	251-500     |
| Horse (war)    |    200     |	    36/6        |	60'(10')  |	351-700     |
| Pony           |    40      |	    24/6        |	40'(10')  |	276-550     |
| Riding Dog     |    80      |     30/10       |       50'(10')  |     151-300     |
| Dog (hunting)  |    17      |	    30/10       |	50'(10')  |	 —          |
---

### **Tack & Harness**
<details>
<summary> Less / More </summary>

| **Item**                | **Cost** | **Wt.** |
|-------------------------|:--------:|---------|
| Light Barding (+2)      | 120      | 50      |
| Mail Barding (+4)       | 400      | 80      |
| Plate Barding (+6)      | 1,200    | 100     |
| Saddle and Bridle       | 10       | 35      |
| Saddle Bags [20 lbs.]   | 4        | 7       |

**Small Mounts:** 1/2 cost and weight. For those required by Dwarves and Halflings. This includes ponies, and riding dogs. This is also suitable for other dogs (hunting and war dogs for example).
**Large Mounts:** Twice as expensive and heavy. For creatures significantly larger than a draft horse, such as an elephant. For the truly titanic, apply this modifier multiple times.

</details>
</details>

---
---

## **Land Vehicles**

<details>
<summary> Less / More </summary>

| **Vehicle** |  **Cost**  |  **Length x width**  |  **Hardness/HP** | Miles/Day | **Movement**  |  **Cargo** |
|-------------|:-----------:|:-------------------:|:---------------:|:---------------:|:---------------:|------------|
| Cart        |    50      |	15' x 4           | 8/10           |24/12|	   40(20')    | 500        |
| Chariot     |    400      |	15' x 6           | 10/10           |36/6|	   60(10')    | 300        |
| Coach	      |    1,500    |	30' x 8           | 6/12            |24/6|	   40(15')    | 2,000 |
| Wagon	      |    500      |	35' x 8           | 6/16            |12/6|	   20(15')    | 4,000 |

**Cart** is a small two-wheeled vehicle pulled by a horse; there is enough room for two people and their traveling gear.
**Chariot** A two-wheeled vehicle pulled a single war horse. The body is designed for one person. The common chariot is usually used to bring a champion warrior out to the battle field so they aren’t tired by running.
**Coach:** The coach is a long distance vehicle pulled by four to six horses hitched in pairs. A coach will be able to sit six people inside and has a bench on the front for a driver and a guard if traveling through wilderness. There is a place for storage of one or two chests per passenger, either on the top of the coach or on the boot located at the back of the vehicle.
**Wagon:** A large vehicle used to transport goods from farms into towns or transport all a family’s possessions across the frontier. The wagon is pulled by a team (pair) of horses or oxen. The sides of the wagon are 4’ to 6’ high.


</details>
</details>
