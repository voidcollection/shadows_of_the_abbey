﻿# **NEW SPELLS**
[Back to Wiki](https://gitlab.com/voidcollection/shadows_of_the_abbey/-/wikis/home)

## **Advanced Illusion**
Range:	180’.    Illusionist 4.   Duration:	1 minute / level

This spell functions like the spell **spectral force** except that the illusion follows a script determined by the caster. The illusion follows the script without requiring the caster to concentrate on it. If the illusion includes intelligible speech, however, such speech is likewise scripted, so the illusion will not respond if spoken to.

## **Alter Self**
Range:	self.    Illusionist 2.  Duration:	10 minutes / level

This spell allows the caster to assume the form of a creature of the same body type (i.e. humanoid). The new form must be within 50% of his or her normal size. The assumed form can’t have more hit dice than the caster's level, nor more than 5 HD at most. The caster can change into a member of his or her own kind or even into him- or herself.

The caster retains his or her own ability scores, class, level, hit points, attack bonus and saving throws. If the new form is capable of speech, he or she can communicate normally and cast spells. The caster acquires the physical qualities of the new form, including armor class, movement capabilities (such as climbing, swimming and flight, but not including magical movement abilities such as **levitation**), natural weapons (such as claws, bite, and so on), racial abilities, and any gross physical qualities (wings, additional extremities, etc).

Using alter self does not permit additional attacks, and the caster does not gain any special, supernatural, or spell-like abilities of the new form.

## **Animal Friendship**
Range: 30 ft.   Druid 1.	Duration: permanent

Upon encountering a normal or giant-sized (but not magical) animal, the caster may begin casting this spell, which requires an hour to complete. During this period the animal will remain nearby, and will not attack the caster or his or her allies for the duration of the casting (so long as they do not attack or otherwise disturb it). At the end of the casting, if the animal rolls a successful save vs. Spells, the spell has failed. At this point the animal acts naturally, without bearing the caster any special ill-will.

If the animal fails its save, it becomes an *animal friend* and joins the caster for the rest of its natural life, assisting in any way it can. Both the caster and any other *animal friends* are treated as treasured partners. There is no arcane mental connection, nor any particular control, but rather a strong fraternal bond which should go both ways. If the animal is treated poorly, or it's love not returned over a period of time, it can attempt another save vs. Spells at the GM's discretion.

A caster may only have, at most, twice his or her level in hit dice of *animal friends*. If this spell is cast on an animal that would put the total above that number, it has no effect.

## **Animate Natural Objects**
Range: 100'+10'/level.   Druid 6.		Duration: 1 round/level

This spell functions identically to the cleric spell *animate objects*, but it may only be cast on living trees, plants, or fungi, wooden objects, unworked stone or earth, or naturally-occurring bodies of water or ice. At the GM's option other natural phenomena such as nonmagical fires or weather effects might also be subject to this spell.

## **Assume Animal Form**
Range: Self.    Druid 3.	 Duration: 1 hour/level

This spell allows the caster to change into the form of any natural animal. The assumed animal form cannot have more hit dice than the caster's level. If slain, the caster reverts back to his or her original form.

The caster gains the physical capabilities and statistics of the new form but retains his or her own mental abilities. The caster may remain transformed up to one hour per level, or end the spell earlier if he or she so desires.

## **Audible Glamour**
Range:	60’ + 10'/level.    Illusionist 1.	Duration:	2 rounds / level

**Audible glamour** allows the caster to creates a volume of sound that rises, recedes, approaches, or remains at a fixed location. The caster chooses what type of sound he or she creates when casting the spell, and cannot thereafter change the sound’s basic character.

The volume of sound created is equivalent to the noise of two normal Humans per caster level. Thus, talking, singing, shouting, walking, marching, or running sounds can be created. The noise an **audible glamour** spell produces can be virtually any type of sound within the volume limit. A horde of rats running and squeaking is about the same volume as six Humans running and shouting. A roaring lion is equal to the noise from twelve humans, while a roaring dragon is equal to the noise from twenty Humans.

Note that this spell is particularly effective when cast in conjunction with **phantasmal force**.

If a character states that he or she does not believe the sound, a save vs. Spells is made; if the save succeeds, the character hears the sound as distant and obviously fake.

## **Blur**
Range:	touch.    Illusionist 2.	Duration:	1 minute / level

When a blur spell is cast, the caster causes the outline of the creature touched to appear blurred, shifting and wavering. This distortion causes all missile and melee combat attacks to be made at -4 on the first attempt and -2 on all successive attacks. It also grants a +1 on the saving throw die roll against any direct magical attack.

A detect invisible spell does not counteract the blur effect, but a true seeing spell does. Opponents that cannot see the subject ignore the spell’s effect (though fighting an unseen opponent carries penalties of its own).

## **Call Lightning**
Range: 100 ft + 10'/level.   Druid 3.	Duration: 1 round/level

Immediately upon completion of the spell, and at most once per round thereafter, the caster may call down a 5-foot-wide, 30-foot-long, vertical bolt of lightning that deals 3d6 points of electricity damage. The bolt of lightning flashes down in a vertical stroke at whatever target point the caster chooses within the spell’s range (measured from the caster's position at the time of casting). Any creature in the target area or in the path of the bolt is affected. A successful save vs. Spells reduces damage taken by half.

The caster needs not call a bolt of lightning immediately; other actions, even spellcasting, can be performed. However, each round after the first the caster may choose to call a bolt instead of taking some other normal action. The caster may call a total number of bolts equal to one-third of his or her caster level, rounded down.

If the caster is are outdoors and in a stormy area — a rain shower, clouds and wind, hot and cloudy conditions, or even a tornado (including a whirlwind formed by a djinni or an air elemental) — each bolt deals 3d8 points of damage instead of 3d6.

This spell functions only where the open sky is accessible, generally meaning outdoors; bolts may be summoned through windows or skylights at the GM's discretion.

## **Call Woodland Beings**
Range : 120 ft.  Druid 4.		Duration: 1 round/level

This spell summons woodland creatures. They appear where the caster designates and act immediately, on his or her turn (they cannot be summoned into an environment that cannot support them). They attack the caster opponents to the best of their ability. If the caster can communicate with the creatures, he or she can direct them not to attack, to attack particular enemies or to perform other actions.

The spell conjures one type of creatures of the caster's choice from this list:
`	`4 Centaurs, 16 Pixies, 2 Treants, or 4 Unicorns

This spell only works outdoors in a natural location (not in town or any structure). 

## **Change Self**
Range:self.  Illusionist 1. Duration:	1 turn / level

This spell allows the caster to alter his or her appearance. This includes not only body shape and facial features, but also clothing, armor, weapons, and equipment. The caster can seem up to one foot shorter or taller, and may appear thin, fat, or any size in between. This spell cannot changes the overall body shape of the caster (to look like a dog, for example). Otherwise, the extent of the apparent change is up to the caster.

The spell does not provide the abilities or mannerisms of the chosen form, nor does it alter the perceived tactile (touch) or audible (sound) properties of the caster or his equipment.

A creature that interacts with the caster may, at the discretion of the GM, be allowed a save vs. Spells to recognize the illusion.

## **Color Cloud**
Range:	40’ (see text).   Illusionist 3.	Duration:	1 round/level

This spell is similar to **color spray**.  It causes a vivid cloud of clashing colors which cause living creatures in the area of effect (or who enter the area of effect) to become blinded or possibly even unconscious.  The cloud covers an area up to 20 feet square or 20 feet in diameter, as chosen by the caster.  Each creature within the cloud is affected according to its hit dice:

- 4 HD or less: The creature becomes unconscious for 2d4 rounds, then is blinded for 1d4 rounds.
- 5 to 8 HD: The creature is blinded for 1d4 rounds.
- 9 or more HD: The creature is blinded for 1 round.

All creatures having more levels or hit dice than the spell caster, or having 10 or more levels or hit dice in any case, are entitled to a save vs. Spells to negate the effect.  Sightless creatures are not affected by this spell.

## **Color Spray**
Range:	20 feet (see text).   Illusionist 1.	Duration:	instantaneous

This spell causes vivid cone of clashing colors to be projected from the caster hands, causing living creatures in the area of effect to become blinded or possibly unconscious.  The cone has a 5' diameter at the base, 20' diameter at the end, and is 20' long.  Each creature within the cone is affected according to its Hit Dice:

- 2 HD or less: The creature is unconscious for 2d4 rounds, then blinded for 1d4 rounds.
- 3 or 4 HD: The creature is blinded for 1d4 rounds.
- 5 or more HD: The creature is blinded for 1 round.

All creatures having more levels or hit dice than the spell caster, or having 6 or more levels or hit dice in any case, are entitled to a save vs. Spells to negate the effect.  Sightless creatures are not affected by color spray.

## **Commune With Nature**
Range : see text.    Druid 5.		Duration : instantaneous

The caster becomes one with nature, attaining knowledge of the surrounding territory. After 10 minutes of concentration, the caster instantly gains knowledge of one fact per caster level among the following subjects : the ground or terrain, plants, minerals, bodies of water, people, general animal population, presence of woodland creatures, presence of powerful unnatural creatures, presence of settlement or structure or even the general state of the natural setting.

In outdoor settings, the spell operates in a radius of 1 mile per caster level. In natural underground settings – caves, caverns, and the like – the radius is limited to 100 feet per caster level. The spell does not function where nature has been replaced by construction or settlement, such as in dungeons and towns.

## **Control Temperature, 10' Radius**
Range : 0.   Druid 4**. Duration : 1 hour/level

The caster can change automatically the surrounding temperature (10’ radius sphere) by up to 10 degrees Fahrenheit per caster level. The change can be upward or downward (caster’s choice).

## **Control Winds**
Range: 0 (see text).    Druid 5.	Duration: 10 minutes/level

The caster alters wind force around him or her (40’ per caster level radius cylinder 40’ high). The caster can make the wind blow in a certain direction or manner, increase its strength, or decrease its strength. The new wind direction and strength persist until the spell ends or until the caster chooses to alter it, which requires concentration. The caster may create an “eye” of calm air up to 40’ radius around him or her and may choose to limit the area of effect to any cylindrical area less than his or her full limit.

The caster may choose wind patterns over the spell’s area. He or she can choose a downdraft blows from the center outward, an updraft blows from the outer edges in toward the center, a rotation that causes the winds to circle the center or a blast that simply causes the winds to blow in one direction across the entire area from one side to the other.

For every three caster levels, the caster can increase or decrease wind condition by one level (as described in the Waterborne Adventures section of the Basic Fantasy RPG Core Rules).

## **Dancing Lights**
Range:	 40 feet +10'/level.  Illusionist 1.		Duration: 2 rounds / level

Depending on the version of the spell selected, the caster creates up to four lights that resemble lanterns or torches (and cast that amount of light), or up to four glowing spheres of light (which look like will-o’-wisps), or one faintly glowing, vaguely humanoid shape.

The dancing lights must stay within a 10-foot-radius area in relation to each other but otherwise move as the caster desires, up to 100 feet per round. Note that concentration is not required; if ignored, the lights continue to move as they were moving when the caster last directed them. If the lights pass beyond the maximum range, the spell ends immediately.

## **Detect Illusion**
Range:	60 feet.  Illusionist 2.	Duration:	2 turns

This spell allows the caster to recognize illusions by sight. To the caster, all illusions within the given range appear slightly translucent and obviously fake. Detect illusion allows detection of visual illusions only; it does not detect auditory illusions such as audible glamer.

## **Detect Snares and Pits**
Range: 60 ft**. Druid 1.	Duration: 10 turns

By means of this spell, the caster can detect simple pits, deadfalls, and snares as well as mechanical traps constructed of natural materials. The spell does not detect complex traps, including trapdoor traps.

This spell does detect certain natural hazards, for instance quicksand (detected as a snare), a sinkhole (a pit), or unsafe walls of natural rock (a deadfall).  However, it does not reveal other potentially dangerous conditions.  The spell does not detect magic traps (except those that operate by pit, deadfall, or snaring), nor mechanically complex ones, nor those that have been rendered safe or inactive.

The amount of information revealed depends on how long the druid studies a particular area.

- 1st Round: Presence or absence of hazards.
- 2nd Round: Number of hazards and the location of each. If a hazard is outside the druid's line of sight, then the caster discern its direction but not its exact location.
- Each Additional Round: The general type and trigger for one particular hazard closely examined by the caster.

Each round, the druid can turn to detect snares and pits in a new area. The spell can penetrate barriers, but 1 foot of stone, 1 inch of common metal, a thin sheet of lead, or 3 feet of wood or dirt blocks it.

## **Dispel Illusion**
Range:	120 feet.    Illusionist 2.  Duration:	instantaneous

The caster can use this spell to end all ongoing “illusion spells” within a cubic area 20' on a side. Dispel illusion affects spells such as audible glamer, phantasmal force, spectral force, advanced illusion, etc. The GM has the responsibility to identify which spells are illusions.

Any illusion spell cast by a character of a level equal to or less than the dispel illusion caster's level is ended automatically. Those created by higher level casters might not be canceled; there is a 5% chance of failure for each level the illusion's caster level exceeds the dispel illusion caster level. For example, a 10th level caster dispelling an illusion created by a 14th level caster has a 20% chance of failure.

When an Illusionist attempts to dispel an illusion cast by a non-Illusionist, the Illusionist is treated as if he or she were two levels higher; if it is not obvious whether the illusion was created by an Illusionist or not, the GM should assume it was.

Non-illusion spells cannot be ended by this spell.

## **Displacement**
Range:	touch.   Illusionist 3.  Duration:	2 rounds / level

When a **displacement** spell is cast, the caster causes the creature touched to appear 2 feet away from its actual position. This distortion causes all missile and melee combat attacks to be made at -4 on all attacks. It also grants a +2 on the saving throw die roll against any direct magical attack.

A **detect invisible** spell does not counteract the blur effect, but a **true seeing** spell does. Opponents that cannot see the subject ignore the spell’s effect (though fighting an unseen opponent carries penalties of its own)..

## **Dream**
Range:	see text.    Illusionist 5.  Duration:	special

This spell allows the caster, or a messenger touched by the caster, to send a recipient a message through the recipient’s dreams. When the spell is cast, the caster must identify the recipient in an unambiguous way. The messenger (whether the caster or someone else) then appears in the recipient’s dream and then delivers the message. The message is purely one-way, and there is no opportunity for questions or interaction. When the recipient wakes up, they will remember the message perfectly.

After casting the spell, the messenger is helpless and completely unaware of their surroundings until the spell ends. If the recipient is awake when the spell is cast, the messenger can either end the spell immediately or wait until the recipient goes to sleep. Once the recipient goes to sleep, the message can be delivered. Whether the messenger has to wait or not, the messenger immediately becomes fully aware again once the message is delivered. Creatures who do not sleep cannot be a recipient of this spell.

## **Entangle**
Range: 200 ft. Druid 1. Duration: 3 rounds per level

This spell causes grasses, trees, bushes, shrubs, or other plants to entwine around creatures in a 10'x10' area.  Most creatures within the area move at ¼ normal speed; very large and/or very strong creatures (at least as big or strong as a giant or dragon) move at ½ normal speed. Entangled creatures may not attack nor take most actions other than movement due to the interference of the enchanted plants.

## **Faerie Fire**
Range: 200 feet. +10'/level.    Druid 1. 	Duration: 1 minute / level

A pale glow surrounds and outlines all objects including individuals within a 20 foot radius from a point chosen by the caster. Outlined subjects shed light as candles. Outlined creatures do not benefit from the concealment normally provided by darkness, and the spell effectively negates the effects of blur, displacement, invisibility, or similar effects. The light is too dim to have any special effect on undead or dark-dwelling creatures vulnerable to light. The faerie fire can be blue, green, or violet, according to your choice at the time of casting. The faerie fire does not cause any harm to the objects or creatures thus outlined.

## **False Vision**
Range:	touch.   Illusionist 6.  Duration:	1 hour / level

Any attempt to observe anything within the area of this spell from most kinds of scrutiny will fail. Those looking into the area see a false image (as the spell **advanced illusion**), as defined by the caster at the time of the casting. As long as the duration lasts, the caster can concentrate to change the image as desired. While not concentrating, the image remains static. Those inside the affected area can see and hear outside the affected area normally. Anyone inside the affected area are immune to **ESP** spells.

Divination spells like **magic mirror**, **clairvoyance**, and **clariaudience** cannot perceive anything within the area. **False vision** does not stop creatures or objects from moving into and out of the area, but they will likely need to disbelieve the illusion before they try.

## **Flame Strike**
Range : 60 ft.  Druid 5.	Duration : instantaneous

A Flame strike produces a vertical column of divine fire roaring downward (cylinder of 10' radius, 30' high). The spell deals 6d8 points of damage to any creatures within the area of effect. A save vs. Spells for half damage is allowed.

## **Gaze Reflection**
Range:	0.   Illusionist 1.  Duration:	1 round / level

The spell creates a mirror like image in the air in front of the caster’s face. Any gaze attack, such as that from a basilisk or a medusa, will be reflected back upon the attacker when it looks at the caster.

## **Heat Metal**
Range: 25 ft.   Druid 2.	Duration: 7 rounds

This spell causes a single item made of ferrous (iron-based) metal to become hot for a brief period of time.   The affected item is warm to the touch immediately, and then becomes progressively hotter each round as indicated on the table below. The damage roll indicated is applied to any creature holding or wearing the affected item; a brief touch does no damage.

|Round|Temperature|Damage|
| :- | :- | :- |
|1st|Warm|None|
|2nd-3rd|Hot|1d4 points|
|4th-5th|Searing|2d4 points|
|6th|Hot|1d4 points|
|7th|Warm|None|

## **Hold Animal**
Range: 180 ft.  Druid 3.	Duration: 2d8 turns

This spell functions like **hold person,** except that it affects only animals.  Specifically, this means non-magical living creatures of animal intelligence, including giant sized animals.  A save vs. Spells is allowed to resist this spell.

## **Hypnotic Pattern**
Range:	120’.    Illusionist 2.  Duration:	concentration

A shifting pattern of light moves through the air mesmerizing those who look at it. Roll 2d6 plus the caster level to determine how many HD are affected by the spell. Creatures with the smallest hit dice are affected first, and among those with the same HD, those closest to the effect are affected first. HD of effect that are not sufficient to affect the next creature are wasted. Affected creatures will stop and watch the effect until the duration expires or they are threatened. Sightless creatures cannot be affected by this spell.

The effect continues for 2 rounds after the caster stops concentrating on the spell.

## **Illusory Script**
Range:	touch.   Illusionist 3.  Duration:	permanent

This spell allows the caster to write whatever they want on whatever they want. The caster specifies who they want to read it. (This can be an individual, class of individuals, a group, etc.) For anyone else this appears to be an unintelligible magical writing of some kind, though an Illusionist will recognize it as an Illusory Script.

Any unauthorized creature attempting to read the script must save vs. Spells or fall under the effects of a Suggestion. The Suggestion only applies for 30 minutes and must be very short and simple (for example, “Put this down and leave”).

## **Illusionary Wall**
Range:	60 feet. Illusionist 3.  Duration:	permanent

This spell creates the illusion of a wall, floor, ceiling, or similar surface, covering up to a 10' square area, up to 1' thick. The section of wall created appears absolutely real when viewed, but physical objects can pass through it without difficulty. When the spell is used to hide pits, traps, or normal doors, any detection abilities that do not require sight work normally. Touch or a probing search may reveal the true nature of the surface; the GM may either allow a save vs. Spells to detect the illusion, or if the probing attempt is well devised the GM may allow it to automatically succeed. In either case, such measures do not cause the illusion to disappear.

## **Improved Invisibility**
Range:	touch.   Illusionist 4.  Duration:	1 round / level

This spell works exactly like invisibility, except that it does not end if the subject attacks or casts a spell.

## **Invisibility, Mass**
Range:	240'.    Illusionist 6.  Duration:	Special

This spell bestows the effect of an **invisibility** spell on all creatures within a 30' by 30' area, exactly as if each such creature had received its own spell; thus, each subject will remain invisible until he or she attacks or casts a spell, and will remain invisible after leaving the area of effect.  As with the normal invisibility spell, this spell lasts at most 24 hours.

## **Mass Invisibility**
Range:	240’.    Illusionist 6.  Duration:	special

Magic-User 7

This spell bestows the effect of an **invisibility** spell on all creatures within a 30-foot by 30-foot area. Each subject will remain invisible until he or she attacks or casts a spell. A subject that leaves the original area of effect remains invisible.

## **Mass Suggestion**
Range:	30’. Illusionist 6.  Duration:	up to 1 hour / level

This spell bestows the effect of a suggestion spell on all creatures within a 30-foot by 30-foot area. The single suggestion applies to all of the targets. Each target gets its own saving throw.

# **Maze**
Range:	60’. Illusionist 6.  Duration:	1 turn (see text)

By means of this spell, the caster banishes the target creature into an extra-dimensional labyrinth. Once per round, the victim of this spell may attempt a save vs. Spells to escape the labyrinth. If the victim does not escape, the maze disappears after 10 minutes, freeing the victim at that time.

On escaping or leaving the maze, the target creature reappears where it had been when the maze spell was cast. If this location is filled with a solid object, the subject appears in the nearest open space. Spells and abilities that move a creature within a plane, such as **teleport** or **dimension door**, do not help a creature escape a **maze** spell.

Minotaurs are not affected by this spell.

## **Mirage Arcana**
Range:	120’.    Illusionist 5.	Duration:	instantaneous

This spell works as **hallucinatory terrain**, except that any kind of terrain may be re-imaged, including terrain, structures, and equipment. This allows buildings to be hidden or added, and other equipment to be hidden or shown. Creatures, however, cannot be disguised or concealed, though they may always hide themselves within the illusion just as they could hide were the illusory terrain real. This spell requires a full turn to cast.

**Mislead**	Range:	100’

Illusionist 5	Duration:	special

The caster becomes invisible (as if by means of **improved invisibility**) and at the same time an illusory double of him or her appears (as if by means of **spectral force**). The caster is then free to go elsewhere while his or her double moves away. The double appears within the given range, but thereafter moves as the caster directs it (which requires concentration). The double may be controlled at any distance from the caster, so long as the caster remains able to see it.

The caster can make the double appear superimposed perfectly over his or her body so that observers don’t notice an image appearing when the caster turns invisible. The caster and the double can then move in different directions. The double moves at the caster's speed and can talk and gesture as if it were real, but it cannot attack or cast spells, though it can pretend to do so.

The illusory double lasts as long as the caster concentrates upon it, plus 3 additional rounds. After the caster ceases concentration, the illusory double continues to carry out the same activity until the duration expires. The improved invisibility lasts for 1 round per caster level, regardless of concentration.

## **Nightmare**
Range:	see text.    Illusionist 5.  Duration:	instantaneous

This spell allows the caster to send a horrific phantasmal vision through the recipient’s dreams. When the spell is the caster must identify the recipient in an unambiguous way. The nightmare prevents restful sleep and causes 1d10 points of damage to the recipient. They are then unable regain spells again for 24 hours. The recipient is allowed a saving through vs. Spells, which can be modified by how well the caster knows the recipient and if the caster has some token connected to the recipient. 

If **dispel evil** is cast on the recipient while this spell is being cast, the nightmare is dismissed with no effect and the caster is paralyzed for 1 turn per level of the caster performing the **dispel evil**.

If the recipient is awake when the spell is cast, the caster can either end the spell immediately or wait in a trance until the recipient goes to sleep. Once the recipient goes to sleep, the message can be delivered. Whether the caster has to wait or not, the caster immediately becomes fully aware again once the message is delivered. The caster is completely helpless physically and mentally while in the trance. Creatures who do not sleep cannot be a recipient of this spell.

## **Obscurement**
Range:	100' +10'/level. Illusionist 2.  Duration:	1 turn/level

A bank of fog, up to a 20' cube in volume, billows out from the point the caster designates. The cloud moves at a rate of 10' per round under the control of the caster (so long as he or she concentrates on it).  The fog obscures all sight, including darkvision, beyond 5'.  Thus, beyond 5', all creatures will be effectively blind.

The cloud persists for the entire duration even if the caster ceases to concentrate upon it, but a moderate wind (11+ mph) disperses the fog in 4 rounds and a strong wind (21+ mph) disperses it in 1 round. This spell does not function underwater.

## **Obscuring Mist**
Range: 20' radius.  Druid 2.	Duration: 1 turn/level

This spell causes a bank of misty vapor to arise around the caster, which remains stationary once created.  The vapor obscures all sight, including darkvision  The fog obscures all sight, including darkvision, beyond 5'.  Thus, beyond 5', all creatures will be effectively blind.  A strong wind will reduce the remaining duration of this spell to just one-quarter.

## **Part Water**
Range: 60'. Druid 6.	Duration: 1 turn/level

This spell, when cast on a body of water, causes it to part.  This exposes a path that can be traversed as if it where dry land.  Note that this spell does not change the topography of the bottom of the body of water, so the terrain my still be difficult to cross.  The caster is able to affect a body of water up to 5 feet/caster level.  The caster may dismiss this spell at any time.

## **Pass Tree**
Range: Touch.   Druid 6	Duration: Instantaneous

This spell allows the caster and up to two others to teleport between any two living trees on the same plane.  The caster choses a general location or a specific tree.  The caster must have personal knowledge of the specific tree or general area that is the target destination.

## **Pass Without Trace**
Range: Touch.   Druid 1.	Duration: 1 hour/level

This spell permits up to one creature per caster level to  move through any type of terrain, leaving neither footprints nor scent.  Tracking the subjects by nonmagical means is thus rendered impossible.

## **Permanent Illusion**
Range:	180 feet.    Illusionist 6.  Duration:	permanent

This spell functions much like advanced illusion, except that the spell is permanent. The “script” for this spell simply repeats endlessly.

## **Phantasmal Image**
Range:	180’.    Illusionist 1.  Duration:	concentration

This spell is often the first spell an Illusionist will learn. The spell creates the visual illusion of an object, creature, or other effect, as visualized by the caster up to a maximum size of a 20-foot cube. The illusion does not create sound, smell, texture, temperature, or movement. The image persists so long as the caster concentrates upon it.

A save vs. Spells may be granted by the GM any time he or she feels the illusion is likely to be seen through, especially if the player describes an illusion which seems improbable or otherwise poorly conceived.

## **Phantasmal Killer**
Range:	100’ + 10’ / level.  Illusionist 4.  Duration:	instantaneous

When this spell is cast, it creates a quasi-real creature that is the target’s worst possible fear. It is literally the worst possible creature to fight that the target can imagine. Only the target can see the phantasmal killer; the caster sees a rough shape.

The target must make a save vs. Spells to disbelieve the phantasmal killer. If the saving throw is failed, the killer then touches the target, who must then save vs. Petrify or die from fear. Even if the target makes the second saving throw, they still take 3d6 of damage.


## **Phantom Messenger**
Range:	special. Illusionist 3.  Duration: special

When this spell is cast, it creates a quasi-real, birdlike creature. It may appear as a small hawk/falcon or as a large dove, and may be any shade of grey from nearly white to nearly black. It does not fight, but all normal animals shun it and only monstrous ones will attack. The messenger has an Armor Class of 18 and 2 hit points, plus 1 per level of the caster. If it loses all of its hit points, the Messenger disappears.

The Messenger flies at a movement rate of 120 feet per round. It can bear up to one ounce per five full levels of the caster (one ounce up to level 9, two ounces up to level 14, three ounces from levels 15 through 19, four ounces at level 20).

When created, the Messenger must be given a specific destination, which can be any location on the same plane of existence to which the caster has been at least once (even if he or she was lost at the time). After the caster attaches any message or small item(s) to the legs of the bird, he or she releases it, and it flies without error to the specified location.

The caster may additionally visualize a specific person, whom the Messenger will seek out near the target location. Note that this does not allow location of a person; the Messenger will fly around the target area looking for the target creature.

The Messenger will travel at its maximum movement from the caster to the target location. Distance is no object; the Messenger will continue indefinitely until the target area is reached. If a target creature was specified, the Messenger will then fly around up to one day per level of the caster looking, until the target creature is found; otherwise the Messenger will immediately land in the target area. After it lands the Messenger will wait patiently for the message or item(s) to be removed, and then disappear in a faint puff of smoke. If the item(s) or message are not removed immediately the Messenger will disappear anyway after waiting one round per level of the caster, dropping the items on the spot; this will also happen if a target creature is specified and cannot be found (the Messenger will land first before this happens so as not to drop any carried objects a great distance).**


## **Phantom Steed**
Range:	touch.   Illusionist 3.  Duration: 1 hour / level

The caster conjures a quasi-real horselike creature. The steed can be ridden only by the caster or by the one person for whom he specifically created the mount. A phantom steed has a black head and body, gray mane and tail, and smoke-colored, insubstantial hooves that make no sound. It has what seems to be a saddle, bit, and bridle. It does not fight, but animals shun it and refuse to attack it.

The mount has an AC of 18 and 12 hit points. If it loses all its hit points, the phantom steed disappears. A phantom steed has a speed of 40 feet per caster level. It can bear its rider’s weight and what he or she carries (the steed cannot carry saddlebags or the like).

These mounts gain additional powers according to the caster's level:

**8th level:** Ability to ride over sandy, muddy, or even swampy ground without difficulty or decrease in speed.

**10th level:** Ability to pass over water as if it were firm, dry ground.

**12th level:** Ability to travel in the air as if it were firm land instead, so chasms and the like can be crossed without benefit of a bridge. Note, however, that the mount can not casually take off and fly.

**14th level:** Ability to fly as if it were a pegasus.

A mount’s abilities include those of mounts of lower caster levels; for example, a phantom steed created by a 13<sup>th</sup> level caster can pass over water, sand, or mud as well as cross chasms.

## **Phantom Trap**
Range:	touch.   Illusionist 2.  Duration:	permanent

The spell makes a lock or other small mechanism appear to be trapped. Anyone that can detect traps, or any spell that can detect traps will show the item to be trapped, and the person checking will be convinced that the trap is present. Nothing happens if the trap is sprung, as there is no trap. The purpose is to dissuade thieves or make the thieves waste time.

## **Phase Door**
Range:	touch.   Illusionist 6.  Duration:	1 usage / 2 levels

This spell creates a magical passage through a wall, the floor, the ceiling, or even through a section of ground. The **phase door** is invisible and inaccessible to all creatures except the caster, who is the only one that can use the passage. The passage is 10 feet deep plus another 5 feet for every three caster levels. The caster disappears when entering the **phase door** and appears when he or she exits. If desired, the caster can take one other creature through the door. This counts as two uses of the door. The door does not allow light, sound, or spell effects through it, nor it is possible to see through it without using it. 

A **phase door** is subject to **dispel magic**, but only from someone who is of higher level than the caster. If anyone is within the passage when it is dispelled, he or she is harmlessly ejected (determine randomly in which direction). 

It is possible to allow other creatures to use the **phase door** by setting a triggering condition for the door. This condition can be as simple or elaborate as desired. It can be based on a creature’s name or identity, but otherwise must be based on observable actions or qualities. Intangibles such as level, class, Hit Dice, and hit points don’t qualify.

## **Plant Growth**
Range: special. Druid 3.	Duration: permanent

This spell causes normal vegetation (grasses, briars, bushes, creepers, thistles, trees, vines) within 400 feet + 40 feet per caster level to become thick and overgrown. The plants entwine to form a thicket or jungle that creatures must hack or force a way through. Speed drops to 5 feet, or 10 feet for Large or larger creatures. The area must have brush and trees in it for this spell to take effect.  An area up to 100 ' radius may be thus altered.  Also, the caster may specify an area (a path, a clearing, etc.) within the given area of effect which is not so affected.

This spell has no effect on plant creatures, that is, any self-willed and/or animated plant.

## **Produce Flame/Cold**
Range: 0 ft.    Druid 2.    Duration: 1 min./level

Flames as bright as a torch appear in your open hand. The flames harm neither you nor your equipment. Alternatively, a bluish, cold aura may be produced (also produces light bright as torch-light).

In addition to providing illumination, the flames or cold aura can be hurled or used to touch enemies. You can strike an opponent with a melee touch attack, dealing fire or cold damage respectively equal to 1d6 +1 point per caster level (maximum +5). Alternatively, you can hurl the flames or cold aura up to 120 feet as a thrown weapon. When doing so, you attack with a ranged touch attack (with no range penalty) and deal the same damage as with the melee attack. No sooner do you hurl the flames or cold aura than a new set appears in your hand. Each attack you make reduces the remaining duration by 1 minute. If an attack reduces the remaining duration to 0 minutes or less, the spell ends after the attack resolves.

Neither variation of the spell functions underwater.

## **Programmed Illusion**
Range:	180 feet.    Illusionist 5.  Duration:	special

This spell functions like advanced illusion, except that this spell activates when a specific condition occurs. The caster sets the triggering condition when casting the spell. The event that triggers the illusion can be as general or as specific and detailed as desired, but must be based on an audible, tactile, olfactory, or visual trigger. The trigger cannot be based on some quality not normally obvious to the senses, such as religious belief or magical ability. For example, the spell could be set to trigger when a character wearing robes and a pointed hat enters an area, but not when a Magic-User enters the area.

The spell remains ready indefinitely. When triggered, the spell will last at most 1 round per caster level.

## **Protection From Fire**
Range: Touch.   Druid 3.	Duration: special

If the caster touches himself or herself, this spell grants temporary immunity to fire (normal and magic). When the spell absorbs 12 points per caster level of magical fire damage, it is discharged. Otherwise the spell lasts for 10 minutes per caster level.

If the spell is cast upon another creature than the caster, *protection from fire* grants temporary immunity against normal fire. The spell also offers a +4 bonus to saving throws against magical fire and if the save is successful, the creature suffers no damage (and only 25% if the save fails). This version of the spell lasts for 10 minutes per caster level.

## **Protection From Lightning**
Range : Touch.  Druid 4.    Duration: special

This spell functions exactly like *protection from fire*, except that that it protects against any sort of electrical or lightning damage.

## **Rainbow Pattern**
Range:	180’.    Illusionist 4.  Duration:	concentration

This spell functions as **hypnotic pattern**, except that it affects up to 24 HD of creatures and the caster may move the pattern by up to 30 feet per round. If moved, all creatures under its effects will follow the flowing lights, trying to keep as close to it as possible. If any affected creatures move into a dangerous area, they get a new saving throw. If the view is moved out of sight of an affected creature, they are not longer affected.

This spell will continue for 2 rounds after the caster stops concentrating on it.

## **Rock to Mud**
Range: 150 ft.  Druid 5.	Duration: permanent

This spell turns natural, uncut or unworked rock of any sort into an equal volume of mud; up to two 10 foot cubes per caster level may be so transformed.  Magical stone is not affected by this spell.  The depth of the mud created cannot exceed 10 feet.  A creature unable to levitate, fly, or otherwise free itself from the mud sinks until hip- or chest-deep, reducing its speed to 5 feet and causing it to suffer a -2 penalty to attack rolls, saving throws, and AC.  Creatures large enough to walk on the bottom can wade through the area at a speed of 5 feet, suffering none of the  other penalties given.

If transmute rock to mud is cast upon the ceiling of a cavern or tunnel, the mud falls to the floor and spreads out in a pool to a depth of 5 feet.  The falling mud and the ensuing cave-in deal 8d6 points of damage to anyone caught directly beneath the area, or half damage to those who succeed at a save vs. Death Ray.

Castles and large stone buildings are generally immune to the effect of the spell, since transmute rock to mud cannot affect worked stone and doesn’t reach deep enough to undermine such a buildings’ foundation.  However, small buildings or structures often rest upon foundations shallow enough to be damaged or even partially toppled by this spell.

The mud remains until a successful dispel magic or transmute mud to rock spell restores its substance (though not necessarily its form).  Evaporation turns the mud to normal dirt over a period of days.  The exact time depends on exposure to the sun, wind, and normal drainage.

## **Rope Trick**
Range: touch.   Illusionist 2.	Duration: 1 hour / level

When this spell is cast upon a piece of normal, non-magical rope from 5 to 30 feet long, one end of the rope rises into the air until the whole rope hangs perpendicular to the ground, as if affixed at the upper end. The upper end is, in fact, fastened to an extradimensional space, similar to a bag of holding. Creatures in the space are hidden, beyond the reach of spells (including divinations), unless those spells work across planes.

The space holds as many as eight creatures of man-size or smaller (larger creatures cannot fit through the invisible opening at the top of the rope). Creatures in the space can pull the rope up into the space, making the rope “disappear.” Otherwise, the rope simply hangs in air.

Spells cannot be cast through the extradimensional opening, nor can area effects cross it. Those in the extradimensional space can see out of it as if a 3' square window were centered on the rope. The window is present on the Material Plane, but is invisible, and even creatures that can see the window can’t see through it.

The rope can be climbed by only one person at a time. The rope trick spell enables climbers to reach a normal place if they do not climb all the way to the extradimensional space.

When the spell ends, creatures or objects within the extradimensional space are ejected through the window. The rope, if still attached, drops free at the same moment.

## **Seeming**
Range:	30’. Illusionist 5.  Duration:	1 turn / level

This spell works as **change self**, but instead can affect up to one person per two levels of the caster. All creatures to be affected must be willing and within the given range. The caster may choose which creatures are affected and may include themselves.

## **Shadow Door**
Range:	10 feet. Illusionist 3.  Duration:	1 round / level

With this spell, the caster creates the illusion of a door, which he or she will appear to pass through and close. In reality, the caster becomes invisible (as the spell) when the spell is cast. Any creatures opening the “door” will see an empty 10 foot square room of similar style to the surrounding area. The caster remains invisible for the duration of the spell, unless, as with an invisibility spell, he or she attacks any creature or casts a spell.

## **Shadow Walk**
Range:	  touch (see text).  Illusionist 6.		Duration: 1 hour / level

Shadow walk can only be cast in an area of heavy shadows. The caster and up to one willing creature per level are transported to the edge of the Material Plane where it borders the Plane of Shadow. In the region of shadow, the caster (and all the creatures that accompany him or her) moves at an effective rate of 50 miles per hour.

Because of the blurring of reality between the Plane of Shadow and the Material Plane, the caster can’t make out details of the terrain or areas he or she passes over during transit, nor can he or she predict perfectly where the travel will end. When the spell effect ends, the caster and any creatures accompanying him or her arrives 1d10 times 100 feet in a random horizontal direction from the desired endpoint, as nearly as possible. The caster and his or her companions always arrive at ground level, except if the landing area is in a body of water (in which case they arrive at the water level) or underground. If arriving underground, the altitude of arrival should be as close as possible to the same altitude as the intended endpoint location.

## **Slow Poison**
Range : Touch.  Druid 2.	Duration : 1 hour/level

The creature touched by the caster becomes temporarily immune to poison. Any poison in its system or any poison to which it is exposed during the spell’s duration does not affect the subject until the spell’s duration has expired. Slow poison does not cure any damage that poison may have already done, with exception of the following circumstances.

If cast on a character who received lethal damage from poison the previous round, the spell will remove that round's poison damage only, and delay it for the duration of the spell. Prior poison damage will remain unaffected, but, as noted above, no further damage is accrued until the spell lapses, and will never accrue if the poison is neutralized while this spell is in effect. This will give the poisoned victim an opportunity to seek a cure to neutralize the poison before it is fatal. 

## **Solid Fog**
Range:	100’ + 10’ / level.  Illusionist 4.  Duration:	1 minute / level

This spell functions like **obscurement**, but in addition it is so thick that any creature trying to move through it is slowed to a speed of 5 feet, irrespective of its normal movement. It also takes a -2 penalty to all attack and damage rolls made in the fog. The solid fog also stops any physical projectiles, making ranged weapon attacks ineffective. The reduction of movement also applies to falling, so any distance that falls through the fog doesn’t count toward falling damage.

This fog is more difficult to disperse than obscurement. It takes a severe wind (31+ mph) to disperse it, doing so in 1 round.

## **Spectral Force**
Range:	180 feet.    Illusionist 3.  Duration:	special

This spell functions like phantasmal force, except for the following:  Sound, smell, and thermal effects are included, creatures created do not necessarily disappear when touched, assuming the caster causes the illusion to react appropriately. For instance the caster displays illusory wounds when the image is attacked. The spell will last for 3 rounds after concentration ceases.

## **Stinking Cloud**
Range:	100’ +10' / level.   Illusionist 3.  Duration:	10 rounds / level

The spell functions like **obscurement**, but any living creature in the cloud becomes nauseated such that they may not attack, concentrate, cast spells, or do anything other than move. A creature remains nauseated as long as the creature is in the cloud and for 1d4 + 1 rounds after leaving the cloud. A creature may save vs. Spells to avoid the effect of the cloud, but must save every round they remain in the cloud.

## **Suggestion**
Range:	30 feet. Illusionist 4.  Duration:	special

By means of this spell, the caster influences a target creature by suggesting a course of activity limited to a sentence or two. The suggestion must be worded in such a manner as to make the activity sound reasonable. Asking the creature to do some obviously harmful act automatically negates the effect of the spell.

The suggested course of activity can continue for up to 1 hour per caster level. If the suggested activity can be completed in a shorter time, the spell ends when the subject finishes the activity. The caster can specify conditions that will trigger an activity during the duration. If the condition is not met before the spell duration expires, the activity is not performed.

If the recipient creature makes its saving throw, the spell has no effect. A very reasonable suggestion causes the save to be made with a penalty (-1 or -2 is recommended).

The creature to be influenced must, of course, be able to understand the suggestion, i.e., it must be spoken in a language which the spell recipient understands. Undead are not subject to suggestion.

## **Summon Animals I**
Range: 40'/level.   Druid 4.	Duration: One hour

This spell summons 1d8 ordinary animals (including giant ones) of no more than 4 HD each if they are within range. The druid must name the type of animal summoned, and can have up to three choices. If none of the three species are found within range, the spell fails. The animals will be friendly when summoned and aid the druid's current task, including combat, in whatever way they can (to the best of their understanding). The druid may dismiss any animals called by this spell at any time.

## **Summon Animals II**
Range: 60'/level.   Druid 5.	Duration: One day

This spell is a more powerful version of **summon animals i**.  Up to 3d4 animals of 4HD or less can be summoned, or 1d6 animals of 8 HD or less.

## **Tree Sanctuary**
Range: Touch.   Druid 4.    Duration: 1 turn/level

This spell enables a druid to create an invisible door in the trunk of a tree that is visible only to dryads and other druids. The tree must be at least 10' tall, planted into the earth for this spell to work. Once the spell is complete, the druid may enter the tree, effectively disappearing from sight. Only one tree may be effected by Tree Door at a time. Furthermore, this spell may only be cast on a tree occupied by a dryad if the dryad allows it.

The druid is completely aware of his surroundings while in the tree, gaining the bonus of 360-degree vision and hearing, as well as being able to detect changes in the immediate temperature surrounding the tree. The druid may meditate to memorize spells in the tree. Another benefit of this spell is that each turn the druid sleeps inside the tree counts as a full hour of sleep. Also, if the tree is an oak, the druid heals 1d4 hp per turn spent inside the tree.

The druid however loses the senses of taste, smell, and touch and may not speak or cast spells while inside the tree. The druid takes 1/2 of any damage the tree suffers, and if the tree is destroyed or cut down, the druid is cast out into the nearest available space.

## **Veil**
Range:	400’ radius. Illusionist 6.  Duration:	1 hour / level

This spell works as **change self**, but instead can affect any number of willing creatures within the given range. The caster may choose which creatures are affected and may include themselves.

The victim of an illusion does not automatically get to  save vs. Spells to resist the effects of the illusion.  Rather, the victim must have a good reason to believe that a creature, object, or situation is an illusion.  The Game Master must base his or her decision on the quality and the credibility of the illusion.

A high-quality illusion is one created by a caster who has a clear “mental image” of the creature, an object, or situation being simulated.  For example, the quality of an illusion of a dragon cannot be high if the caster has never seen a real dragon.

An illusion can be credible only if the creature, the object, or the situation is realistic.  For example, the illusion of a door on a wall is credible, but not a door floating in the air.  Illusions created by higher-level spells are generally more credible since several senses are affected.

If an illusion is of low quality and/or is not credible, the GM may decide to allow an automatic save vs. Spells.

In addition, a player can announce to the Game Master that his or her character does not "believe" in the existence of a creature, an object, or a situation.  The GM must then make a secret save vs. Spells for that character.  Note that the GM should always make a roll, even if the creature, object, or situation is not an illusion, as omitting the roll would give this fact away to the player.

If a saving throw vs. an illusion is successful, the Game Master must announce this fact to the player; if the character then tells his or her comrades, they in turn receive a save vs. Spells with a +4 bonus.

## **Warp Wood**
Range : 50 ft.  Druid 2.	Duration : instantaneous

The caster causes non-magic wood in a 20’ radius to bend and warp, permanently destroying its straightness, form, and strength. A warped door springs open or becomes stuck. A boat or ship springs a leak. Warped weapons (like bows and arrows) are useless. The caster may warp wood object(s) up to 1’ in all side or its equivalent per caster level.

Alternatively, the caster can “un-warp” non-magic wood (effectively warping it back to normal), straightening wood that has been warped by this spell or by other means. The caster can combine multiple consecutive warp wood spells to warp (or unwarp) an object that is too large to warp with a single spell.

## **Weather Summoning**
Range: 10 miles.    Druid 6.		Duration: 5 turns/level

The caster is able to summon nearby weather conditions. The caster must be aware of the weather condition to summon it. This spell does not grant control of the weather to the caster.

