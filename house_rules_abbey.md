# **HOUSE RULES**

[Back to Wiki](https://gitlab.com/voidcollection/shadows_of_the_abbey/-/wikis/home)

## **CHARACTER CREATION**
- **HP:** Max HP at 1st level.
- **Attribute Generation:** 3d6 down the line. Swap 2 stats.
- **Starting Equipment:** 3d6x10gp. All holy classes start with basic holy symbol, all arcane classes start with spell book.
- **Ancestry Changes-**
  - **Human:** Gains 20% (instead of 10%) more XP than other ancestries as they must learn unusually fast.
- **Class Changes-**
  - **Fighter** uses *Cleave*. If a fighter drops a foe to Zero health in melee, the fighter may make another attack if an available target is within range. Total number of 'Cleave' attacks not to exceed the fighter's level.
  - **Arcane Starting Spells:** Start with **Read Magic** and 1+INT modifier number of spells in their book, randomly selected.
- **Allowed Classes-**
  - **Fighter** - *All Ancestries*
  - **Cleric** - *All Ancestries*
  - **Thief** - *All Ancestries*
  - **Magic-User** - *Elves, and Humans only*
  - **Spellsword** (Fighter+MU) - *Elves only*
  - **Druid** (Cleric Sub-Class) - *All Ancestries*
  - **Paladin** (Fighter Sub-Class) - *All Ancestries*
  - **Ranger** (Fighter Sub-Class) - *Elves, Halflings, and Humans only*  

## **GAMEPLAY**

- **Weapons-** 
  - **Weapon Attributes:** 
    - *Small Weapons* are more easily hidden. 
    - *Medium Weapons* act as normal. 
    - *Large Weapons* have -1 To Hit modifier when in confined spaces.
- **Rations:** Rations are now per day. They cost 2gp weigh 2 lbs. each.
- **Weight of Treasure:** Regardless of value, 20 coins equal 1 pound. Rings and gems will be case by case basis.
- **Retainers:** Can only hire retainers ½ current employer's level. Retainers of equal level to players will leave.Standard payment is ½ share and cost of upkeep.
- **Hazard Die:** 1d6, check every other Turn: 1- Encounter; 2- Light reduction (Candles, 1 tick; Torches, 2 ticks; Lanterns, 4 ticks); 3- Light reduction; 4- Nothing; 5- Nothing; 6- Nothing.
- **Magic Changes-** 
  - **Spells at Level Advancement:** Having access to a mentor allows an Arcane caster to learn a new spell at level up. It takes one day per level to inscribe the new spell into their book. These spells are free but randomized.
  - **Learning from other Schools:** Magic-Users may learn spells from other Arcane spell lists through discovery or research subject to Ref approval. Cost to research or copy takes 25% longer and 25% more expensive than the default spell list.
- **Critical Success on Attacks:** In case of critical success, you deal the maximum damage of your weapon and then add the usual damage roll (Example: If using a sword, you deal 1d8+8.)
- **Dungeon Respite:** Party may decide to rest while in the dungeon. Takes **2 Turns** and requires **1 day of Rations** and **1/2 a Waterskin of water per person** taking part. After resting, all members taking part in food and drink gain **1d3 HP** back. A Cleric/Druid may choose to spend this time tending to one in need and provide +1hp to the one being aided.
- [**BFRPG Optional Death vs Save Rule**](https://www.basicfantasy.org/srd/gm01.html#optional-rules)**:** At 0 HP or less, the character must make a **Save vs. Death**. If the save is made, the character will die in 2d10 Rounds; unless the character’s wounds are bound (or he or she receives healing magic). The PC remains unconscious for the full 2d10 rounds rolled, either dying if left untreated or waking with their wounds bound. Failure is Death.  
- **Honor after Death:** Body must be recovered and any treasure held in safety by the fallen PC or provided by the surviving group that is spent on their funeral is halved and given to the next character as starting XP.

## **DOWNTIME**  
- **Healing:** 1HP per day of normal rest. 1 HD of HP per day of complete bed rest, minimum 2 HP can be gained this way.
- **Training:** Spend a minimum of 100gp in the persuit of "training". 1gp : 1xp. Takes 1 week. 
- **Rumor-Mongering:** A PC can spend the week buying drinks and greasing palms to get rumors about local happenings. Requires spending 1d4x10gp and making a reaction roll, modified by Charisma. This takes 1 full week per attempt. This is rolled by the Ref. **2:** Failure. **3-11:** Hear 1 Rumor. **12+:** Hear 2 Rumors.
- **Recruitment:** Spending the day going through taverns spreading the word and buying rounds costs 50gp. There is a 3-in-6 chance of successfully locating applicants. Retainers available for employment need to be developed. May only be used once a week. As those adventurers gain XP and level up, as long as they are not mistreated, remain in the pool for employment.
- **Magic Activities:**
  - **Spell Research:** Researching a "standard" spell costs 1,000gp/week per spell level. These are chosen by the researcher. The chance of success is 25%, plus 5% per level of the character, minus 10% per level of the spell; the maximum chance of success is 95%.
  - **Magic Item Creation:** Spell Scrolls are 50gp per level and can be researched at level 1.
- **Ability Improvements:** Ability scores can be raised through intense and dedicated training. The first time a score is raised, it costs 2000gp and takes 1 month. Each additional time the same stat is increased, the cost doubles. A score cannot be raised beyond 16.

## **ADVANCEMENT**

- **XP for Exploration (Individual):** XP gained for each room properly explored during an expedition. Room must be engaged with. Worth 10xp per room, cumulative, and resets when the party returns to town. A room can only be “explored” once.
- **XP for Challenges Overcome (Individual):** XP gained from overcoming challenges presented by enemies.
- **XP for Training (Individual):** See above entry on Training
- **XP for Session Attendance (Individual):** 100xp gained from coming to the session.