# **CLASSES**
[Back to Wiki](https://gitlab.com/voidcollection/shadows_of_the_abbey/-/wikis/home)

## **The Fighter**
<details>

- **Ancestry Limits:** Any
- **Prime Requisite:** STR (Must be 9+)
- **Hit Dice:** D8
- **Weapons:** Any.
- **Armor:** Any, shields allowed.
- **Starting Saves:** Death 12; Wands 13; Paralysis 14; Breath 15; Spells 17
- **Special:**
  - **Cleave:** If a fighter drops a foe of to Zero health the fighter may make another attack if an available target is within range. Total number of 'Cleave' attacks not to exceed the fighter's level.

### **Progression & Saves**
<details>


#### **PROGRESSION**

| **Lvl** | **XP**  | **HD** | **AB** |
|---------|---------|--------|--------|
| **1**   | 0       | 1d8    | 1      |
| **2**   | 2000    | 2d8    | 2      |
| **3**   | 4000    | 3d8    | 2      |
| **4**   | 8000    | 4d8    | 3      |
| **5**   | 16000   | 5d8    | 4      |
| **6**   | 32000   | 6d8    | 4      |
| **7**   | 64000   | 7d8    | 5      |
| **8**   | 120000  | 8d8    | 6      |
| **9**   | 240000  | 9d8    | 6      |
| **10**  | 360000  | 9d8+2  | 6      |
| **11**  | 480000  | 9d8+4  | 7      |
| **12**  | 600000  | 9d8+6  | 7      |
| **13**  | 720000  | 9d8+8  | 8      |
| **14**  | 840000  | 9d8+10 | 8      |
| **15**  | 960000  | 9d8+12 | 8      |
| **16**  | 1080000 | 9d8+14 | 9      |
| **17**  | 1200000 | 9d8+16 | 9      |
| **18**  | 1320000 | 9d8+18 | 10     |
| **19**  | 1440000 | 9d8+20 | 10     |
| **20**  | 1560000 | 9d8+22 | 10     |

#### **SAVES**

| **Lvl**       | **Death** | **Wands** | **Paralysis** | **Breath** | **Spells** |
|---------------|-----------|-----------|---------------|------------|------------|
| **1**         | 12        | 13        | 14            | 15         | 17         |
| **2**         | 11        | 12        | 14            | 15         | 16         |
| **3**         | 11        | 12        | 14            | 15         | 16         |
| **4**         | 11        | 11        | 13            | 14         | 15         |
| **5**         | 11        | 11        | 13            | 14         | 15         |
| **6**         | 10        | 11        | 12            | 14         | 15         |
| **7**         | 10        | 11        | 12            | 14         | 15         |
| **8**         | 9         | 10        | 12            | 13         | 14         |
| **9**         | 9         | 10        | 12            | 13         | 14         |
| **10**        | 9         | 9         | 11            | 12         | 13         |
| **11**        | 9         | 9         | 11            | 12         | 13         |
| **12**        | 8         | 9         | 10            | 12         | 13         |
| **13**        | 8         | 9         | 10            | 12         | 13         |
| **14**        | 7         | 8         | 10            | 11         | 12         |
| **15**        | 7         | 8         | 10            | 11         | 12         |
| **16**        | 7         | 7         | 9             | 10         | 11         |
| **17**        | 7         | 7         | 9             | 10         | 11         |
| **18**        | 6         | 7         | 8             | 10         | 11         |
| **19**        | 6         | 7         | 8             | 10         | 11         |
| **20**        | 5         | 6         | 8             | 9          | 10         |

</details>

</details>


## **The Cleric**
<details>

- **Ancestry Limits:** Any
- **Prime Requisite:** WIS (Must be 9+)
- **Hit Dice:** D6
- **Weapons:** Blunt weapons only.
- **Armor:** Any, shields allowed.
- **Starting Saves:** Death 11; Wands 12; Paralysis 14; Breath 16; Spells 15
- **Special:**
  - **Deity disfavour:** Clerics must be faithful to the tenets of their alignment, clergy, and religion. Those who fall from favour with their deity may incur penalties.
  - **Turn Undead:** Clerics can invoke the power of their deity to repel undead monsters encountered. Requires brandishing a holy symbol.
  - **Spell casting:** Once a Cleric has proven their faith (at 2nd level), the character may pray to receive spells.
  - **Using magic items:** As spell casters, Cleric can use magic scrolls of spells on their spell list. They can also use items that may only be used by divine spell casters (e.g. some magic staves).
  - **Magical research:** Can spend time and money on research to create new spells or other magical effects associated with their deity. At 7th level, they are able to make temporary magic items. At 9th level, they can make permanent ones.

### **Progression & Saves**
<details>


#### **PROGRESSION**

|             |         |        |        | **Spells** |       |       |       |       |       |
|-------------|---------|--------|--------|------------|-------|-------|-------|-------|-------|
| **Lvl**     | **XP**  | **HD** | **AB** | **1**      | **2** | **3** | **4** | **5** | **6** |
| **1**       | 0       | 1d6    | 1      | -          | -     | -     | -     | -     | -     |
| **2**       | 1500    | 2d6    | 1      | 1          | -     | -     | -     | -     | -     |
| **3**       | 3000    | 3d6    | 2      | 2          | -     | -     | -     | -     | -     |
| **4**       | 6000    | 4d6    | 2      | 2          | 1     | -     | -     | -     | -     |
| **5**       | 12000   | 5d6    | 3      | 2          | 2     | -     | -     | -     | -     |
| **6**       | 24000   | 6d6    | 3      | 2          | 2     | 1     | -     | -     | -     |
| **7**       | 48000   | 7d6    | 4      | 3          | 2     | 2     | -     | -     | -     |
| **8**       | 90000   | 8d6    | 4      | 3          | 2     | 2     | 1     | -     | -     |
| **9**       | 180000  | 9d6    | 5      | 3          | 3     | 2     | 2     | -     | -     |
| **10**      | 270000  | 9d6+1  | 5      | 3          | 3     | 2     | 2     | 1     | -     |
| **11**      | 360000  | 9d6+2  | 5      | 4          | 3     | 3     | 2     | 2     | -     |
| **12**      | 450000  | 9d6+3  | 6      | 4          | 4     | 3     | 2     | 2     | 1     |
| **13**      | 540000  | 9d6+4  | 6      | 4          | 4     | 3     | 3     | 2     | 2     |
| **14**      | 630000  | 9d6+5  | 6      | 4          | 4     | 4     | 3     | 2     | 2     |
| **15**      | 720000  | 9d6+6  | 7      | 4          | 4     | 4     | 3     | 3     | 2     |
| **16**      | 810000  | 9d6+7  | 7      | 5          | 4     | 4     | 3     | 3     | 2     |
| **17**      | 900000  | 9d6+8  | 7      | 5          | 5     | 4     | 3     | 3     | 2     |
| **18**      | 990000  | 9d6+9  | 8      | 5          | 5     | 4     | 4     | 3     | 3     |
| **19**      | 1080000 | 9d6+10 | 8      | 6          | 5     | 4     | 4     | 3     | 3     |
| **20**      | 1170000 | 9d6+11 | 8      | 6          | 5     | 5     | 4     | 3     | 3     |


#### **SAVES**

| **Lvl**       | **Death** | **Wands** | **Paralysis** | **Breath** | **Spells** |
|---------------|-----------|-----------|---------------|------------|------------|
| **1**         | 11        | 12        | 14            | 16         | 15         |
| **2**         | 10        | 11        | 13            | 15         | 14         |
| **3**         | 10        | 11        | 13            | 15         | 14         |
| **4**         | 9         | 10        | 13            | 15         | 14         |
| **5**         | 9         | 10        | 13            | 15         | 14         |
| **6**         | 9         | 10        | 12            | 14         | 13         |
| **7**         | 9         | 10        | 12            | 14         | 13         |
| **8**         | 8         | 9         | 12            | 14         | 13         |
| **9**         | 8         | 9         | 12            | 14         | 13         |
| **10**        | 8         | 9         | 11            | 13         | 12         |
| **11**        | 8         | 9         | 11            | 13         | 12         |
| **12**        | 7         | 8         | 11            | 13         | 12         |
| **13**        | 7         | 8         | 11            | 13         | 12         |
| **14**        | 7         | 8         | 10            | 12         | 11         |
| **15**        | 7         | 8         | 10            | 12         | 11         |
| **16**        | 6         | 7         | 10            | 12         | 11         |
| **17**        | 6         | 7         | 10            | 12         | 11         |
| **18**        | 6         | 7         | 9             | 11         | 10         |
| **19**        | 6         | 7         | 9             | 11         | 10         |
| **20**        | 5         | 6         | 9             | 11         | 10         |

</details>

### **Turn Undead**
<details>

| **Caster Level** | **1 Hit Die** | **2 Hit Dice** | **3 Hit Dice** | **4 Hit Dice** | **5 Hit Dice** | **6 Hit Dice** | **7 Hit Dice** | **8 Hit Dice** | **9+ Hit Dice** |
|:----------------:|:-------------:|:--------------:|:--------------:|:--------------:|:--------------:|:--------------:|:--------------:|:--------------:|:---------------:|
| **1**            | 13            | 17             | 19             | No             | No             | No             | No             | No             | No              |
| **2**            | 11            | 15             | 18             | 20             | No             | No             | No             | No             | No              |
| **3**            | 9             | 13             | 17             | 19             | No             | No             | No             | No             | No              |
| **4**            | 7             | 11             | 15             | 18             | 20             | No             | No             | No             | No              |
| **5**            | 5             | 9              | 13             | 17             | 19             | No             | No             | No             | No              |
| **6**            | 3             | 7              | 11             | 15             | 18             | 20             | No             | No             | No              |
| **7**            | 2             | 5              | 9              | 13             | 17             | 19             | No             | No             | No              |
| **8**            | T             | 3              | 7              | 11             | 15             | 18             | 20             | No             | No              |
| **9**            | T             | 2              | 5              | 9              | 13             | 17             | 19             | No             | No              |
| **10**           | T             | T              | 3              | 7              | 11             | 15             | 18             | 20             | No              |
| **11**           | D             | T              | 2              | 5              | 9              | 13             | 17             | 19             | No              |
| **12**           | D             | T              | T              | 3              | 7              | 11             | 15             | 18             | 20              |
| **13**           | D             | D              | T              | 2              | 5              | 9              | 13             | 17             | 19              |
| **14**           | D             | D              | T              | T              | 3              | 7              | 11             | 15             | 18              |
| **15**           | D             | D              | D              | T              | 2              | 5              | 9              | 13             | 17              |
| **16**           | D             | D              | D              | T              | T              | 3              | 7              | 11             | 15              |
| **17**           | D             | D              | D              | D              | T              | 2              | 5              | 9              | 13              |
| **18**           | D             | D              | D              | D              | T              | T              | 3              | 7              | 11              |

</details>

</details>

## **The Magic-User**
<details>

- **Ancestry Limits:** Human, Halfing & Elf
- **Prime Requisite:** INT (Must be 9+)
- **Hit Dice:** D4
- **Weapons:** Cudgel, dagger, walking staff.
- **Armor:** None.
- **Starting Saves:** Death 13; Wands 14; Paralysis 13; Breath 16; Spells 15
- **Special:**
  - **Spell casting:** Start with **Read Magic** and 1+INT modifier number of spells, randomly selected. Magic-users carry spell books containing the formulae for arcane spells.
  - **Using magic items:** Able to use magic scrolls of spells on their spell list. They can also use items that may only be used by arcane spell casters (e.g. magic wands).
  - **Magical research:** Can spend time and money on research to add new spells to their spell book and to research other magical effects. At 7th level, they are able to make temporary magic items. At 9th level, they can make permanent ones.

### **Progression & Saves**
<details>


#### **PROGRESSION**

|             |         |        |        | **Spells** |       |       |       |       |       |
|-------------|---------|--------|--------|------------|-------|-------|-------|-------|-------|
| **Lvl**     | **XP**  | **HD** | **AB** | **1**      | **2** | **3** | **4** | **5** | **6** |
| **1**       | 0       | 1d4    | 1      | 1          | -     | -     | -     | -     | -     |
| **2**       | 2500    | 2d4    | 1      | 2          | -     | -     | -     | -     | -     |
| **3**       | 5000    | 3d4    | 1      | 2          | 1     | -     | -     | -     | -     |
| **4**       | 10000   | 4d4    | 2      | 2          | 2     | -     | -     | -     | -     |
| **5**       | 20000   | 5d4    | 2      | 2          | 2     | 1     | -     | -     | -     |
| **6**       | 40000   | 6d4    | 3      | 3          | 2     | 2     | -     | -     | -     |
| **7**       | 80000   | 7d4    | 3      | 3          | 2     | 2     | 1     | -     | -     |
| **8**       | 150000  | 8d4    | 3      | 3          | 3     | 2     | 2     | -     | -     |
| **9**       | 300000  | 9d4    | 4      | 3          | 3     | 2     | 2     | 1     | -     |
| **10**      | 450000  | 9d4+1  | 4      | 4          | 3     | 3     | 2     | 2     | -     |
| **11**      | 600000  | 9d4+2  | 4      | 4          | 4     | 3     | 2     | 2     | 1     |
| **12**      | 750000  | 9d4+3  | 4      | 4          | 4     | 3     | 3     | 2     | 2     |
| **13**      | 900000  | 9d4+4  | 5      | 4          | 4     | 4     | 3     | 2     | 2     |
| **14**      | 1050000 | 9d4+5  | 5      | 4          | 4     | 4     | 3     | 3     | 2     |
| **15**      | 1200000 | 9d4+6  | 5      | 5          | 4     | 4     | 3     | 3     | 2     |
| **16**      | 1350000 | 9d4+7  | 6      | 5          | 5     | 4     | 3     | 3     | 2     |
| **17**      | 1500000 | 9d4+8  | 6      | 5          | 5     | 4     | 4     | 3     | 3     |
| **18**      | 1650000 | 9d4+9  | 6      | 6          | 5     | 4     | 4     | 3     | 3     |
| **19**      | 1800000 | 9d4+10 | 7      | 6          | 5     | 5     | 4     | 3     | 3     |
| **20**      | 1950000 | 9d4+11 | 7      | 6          | 5     | 5     | 4     | 4     | 3     |


#### **SAVES**

| **Lvl**       | **Death** | **Wands** | **Paralysis** | **Breath** | **Spells** |
|---------------|-----------|-----------|---------------|------------|------------|
| **1**         | 13        | 14        | 13            | 16         | 15         |
| **2**         | 13        | 14        | 13            | 15         | 14         |
| **3**         | 13        | 14        | 13            | 15         | 14         |
| **4**         | 12        | 13        | 12            | 15         | 13         |
| **5**         | 12        | 13        | 12            | 15         | 13         |
| **6**         | 12        | 12        | 11            | 14         | 13         |
| **7**         | 12        | 12        | 11            | 14         | 13         |
| **8**         | 11        | 11        | 10            | 14         | 12         |
| **9**         | 11        | 11        | 10            | 14         | 12         |
| **10**        | 11        | 10        | 9             | 13         | 11         |
| **11**        | 11        | 10        | 9             | 13         | 11         |
| **12**        | 10        | 10        | 9             | 13         | 11         |
| **13**        | 10        | 10        | 9             | 13         | 11         |
| **14**        | 10        | 9         | 8             | 12         | 10         |
| **15**        | 10        | 9         | 8             | 12         | 10         |
| **16**        | 9         | 8         | 7             | 12         | 9          |
| **17**        | 9         | 8         | 7             | 12         | 9          |
| **18**        | 9         | 7         | 6             | 11         | 9          |
| **19**        | 9         | 7         | 6             | 11         | 9          |
| **20**        | 8         | 6         | 5             | 11         | 8          |

</details>
</details>

## **The Thief**
<details>

- **Ancestry Limits:** Any
- **Prime Requisite:** DEX (Must be 9+)
- **Hit Dice:** D4
- **Weapons:** Any
- **Armor:** Leather, no Shields
- **Starting Saves:** Death 13; Wands 14; Paralysis 13; Breath 16; Spells 15
- **Special:**
  - **Thief Abilities:** Thieves have abilities to do things others cannot through intense training.

### **Progression & Saves**
<details>


#### **PROGRESSION**

| **Lvl** | **XP** | **HD** | **AB** |
|:-------:|:------:|:------:|:------:|
| **1**   | 0      | 1d4    | 1      |
| **2**   | 1250   | 2d4    | 1      |
| **3**   | 2500   | 3d4    | 2      |
| **4**   | 5000   | 4d4    | 2      |
| **5**   | 10000  | 5d4    | 3      |
| **6**   | 20000  | 6d4    | 3      |
| **7**   | 40000  | 7d4    | 4      |
| **8**   | 75000  | 8d4    | 4      |
| **9**   | 150000 | 9d4    | 5      |
| **10**  | 225000 | 9d4+2  | 5      |
| **11**  | 300000 | 9d4+4  | 5      |
| **12**  | 375000 | 9d4+6  | 6      |
| **13**  | 450000 | 9d4+8  | 6      |
| **14**  | 525000 | 9d4+10 | 6      |
| **15**  | 600000 | 9d4+12 | 7      |
| **16**  | 675000 | 9d4+14 | 7      |
| **17**  | 750000 | 9d4+16 | 7      |
| **18**  | 825000 | 9d4+18 | 8      |
| **19**  | 900000 | 9d4+20 | 8      |
| **20**  | 975000 | 9d4+22 | 8      |



#### **SAVES**

| **Lvl** | **Death** | **Wands** | **Paralysis** | **Breath** | **Spells** |
|:-------:|:---------:|:---------:|:-------------:|:----------:|:----------:|
| **1**   | 13        | 14        | 13            | 16         | 15         |
| **2-3** | 12        | 14        | 12            | 15         | 14         |
| **4-5** | 11        | 13        | 12            | 14         | 13         |
| **6-7** | 11        | 13        | 11            | 13         | 13         |
| **8-9** | 10        | 12        | 11            | 12         | 12         |
| **10-11**| 9         | 12        | 10            | 11         | 11         |
| **12-13**| 9         | 10        | 10            | 10         | 11         |
| **14-15**| 8         | 10        | 9             | 9          | 10         |
| **16-17**| 7         | 9         | 9             | 8          | 9          |
| **18-19**| 7         | 9         | 8             | 7          | 9          |
| **20**  | 6         | 8         | 8             | 6          | 8          |


</details>

### **Thief Abilities**

<details>

| **Lvl** | **Open Locks** | **Remove Traps** | **Pick Pockets** | **Move Silently** | **Climb Walls** | **Hide** | **Listen** |
|:-------:|:--------------:|:----------------:|:----------------:|:-----------------:|:---------------:|:--------:|:----------:|
| **1**   | 25             | 20               | 30               | 25                | 80              | 10       | 30         |
| **2**   | 30             | 25               | 35               | 30                | 81              | 15       | 34         |
| **3**   | 35             | 30               | 40               | 35                | 82              | 20       | 38         |
| **4**   | 40             | 35               | 45               | 40                | 83              | 25       | 42         |
| **5**   | 45             | 40               | 50               | 45                | 84              | 30       | 46         |
| **6**   | 50             | 45               | 55               | 50                | 85              | 35       | 50         |
| **7**   | 55             | 50               | 60               | 55                | 86              | 40       | 54         |
| **8**   | 60             | 55               | 65               | 60                | 87              | 45       | 58         |
| **9**   | 65             | 60               | 70               | 65                | 88              | 50       | 62         |
| **10**  | 68             | 63               | 74               | 68                | 89              | 53       | 65         |
| **11**  | 71             | 66               | 78               | 71                | 90              | 56       | 68         |
| **12**  | 74             | 69               | 82               | 74                | 91              | 59       | 71         |
| **13**  | 77             | 72               | 86               | 77                | 92              | 62       | 74         |
| **14**  | 80             | 75               | 90               | 80                | 93              | 65       | 77         |
| **15**  | 83             | 78               | 94               | 83                | 94              | 68       | 80         |
| **16**  | 84             | 79               | 95               | 85                | 95              | 69       | 83         |
| **17**  | 85             | 80               | 96               | 87                | 96              | 70       | 86         |
| **18**  | 86             | 81               | 97               | 89                | 97              | 71       | 89         |
| **19**  | 87             | 82               | 98               | 91                | 98              | 72       | 92         |
| **20**  | 88             | 83               | 99               | 93                | 99              | 73       | 95         |



- **Open Locks:** Unlock a lock without a proper key.  It may only be tried once per lock. If the attempt fails, the Thief must wait until they have gained another level of experience before trying again.

- **Remove Traps:** Rolled twice: first to detect the trap, and second to disarm it.  Ref makes these rolls as the player won't know for sure if the character is successful or not until someone actually tests the trapped (or suspected) area.

- **Pick Pockets:** Allows the Thief to lift the wallet, cut the purse, etc. of a victim without being noticed.  If the roll fails, the Thief didn't get what they wanted; but further, the intended victim (or an onlooker, at the GM's option) will notice the attempt if the die roll is more than two times the target number (or if the die roll is 00).

- **Move Silently:**

- **Climb Walls:** Permits the Thief to climb sheer surfaces with few or no visible handholds.  This ability should normally be rolled by the player.  If the roll fails, the Thief falls from about halfway up the wall or other vertical surface.  The GM may require multiple rolls if the distance climbed is more than 100 feet.  See Falling Damage on page 82 for the consequences of failing this roll.

- **Hide:** Permits the Thief to hide in any shadowed area large enough to contain their body. A Thief hiding in shadows must remain still for this ability to work.

- **Listen:** Generally used to listen at a door, or to try to listen for distant sounds in a dungeon. Ref make this roll for the player.  Note that the Thief and their party must try to be quiet in order for the Thief to use this ability.

- **Sneak Attack:** Any time they are behind an opponent in melee and it is likely the opponent doesn't know the Thief is there.  The GM may require a Move Silently or Hide roll to determine this.  The Sneak Attack is made with a +4 attack bonus and does double damage if it is successful. A Thief can't make a Sneak Attack on the same opponent twice in any given combat. May also be performed bare-handed (in which case subduing damage is done).  Also, the Sneak Attack can be performed with the "flat of the blade;" the bonuses and penalties cancel out, so the attack has a +0 attack bonus and does normal damage; the damage done in this case is subduing damage.

</details>
</details>

## **The Paladin (Fighter Sub-Class)**
<details>

- **Ancestry Limits:** Any
- **Prime Requisite:** STR (Must be 9+); WIS (Must be 11+); CHA (Must be 11+)
- **Hit Dice:** D8
- **Weapons:** Any
- **Armor:** Any, including Shields
- **Starting Saves:** Death 12; Wands 13; Paralysis 14; Breath 15; Spells 17
- **Special:**
  - **Protection from Evil:** Emanates an aura equivalent to the spell in 10' radius.
  - **Detect Evil:** May cast, at will, as the spell.
  - **Holy Weapon:** Once per day, per level, can make a non-magical melee weapon equivalent to a magic weapon ofr purposes of hitting creatures only able to be struck with silver or magical weapons. Lasts 1 Turn.
  - **Lay on Hands:** Once per day, may Lay on Hands to any wounded character and heal 2 points of damage; add the Paladin's Charisma bonus to this figure. Increase number per day by one at each odd-numbered level. Starting at 7th level, they may choose **Cure Disease** (as the spell) instead of providing healing. At 11th level, they may substitute with **Neutralize Poison** (as the spell).
  - **Deity disfavour:** Paladins must be faithful to the tenets of their alignment, clergy, and religion. Those who fall from favour with their deity may incur penalties.
  - **Turn Undead:** Paladins can invoke the power of their deity to repel undead monsters encountered. Requires brandishing a holy symbol.
  - **Spell casting:** Once a Paladin has proven their faith (at 10th level), the character may pray to receive spells.

### **Progression & Saves**
<details>


#### **PROGRESSION**

|          |         |        | **Spells** |   |
|:--------:|:-------:|:------:|:------:|:---:|
| **Lvl**  | **XP**  | **HD** | **1**  | **2** |
| **1**    | 0       | 1d8    | -      | - |
| **2**    | 2500    | 2d8    | -      | - |
| **3**    | 5000    | 3d8    | -      | - |
| **4**    | 10000   | 4d8    | -      | - |
| **5**    | 20000   | 5d8    | -      | - |
| **6**    | 40000   | 6d8    | -      | - |
| **7**    | 80000   | 7d8    | -      | - |
| **8**    | 150000  | 8d8    | -      | - |
| **9**    | 300000  | 9d8    | -      | - |
| **10**   | 450000  | 9d8+2  | 1      | - |
| **11**   | 600000  | 9d8+4  | 2      | - |
| **12**   | 750000  | 9d8+6  | 2      | 1 |
| **13**   | 900000  | 9d8+8  | 2      | 2 |
| **14**   | 1050000 | 9d8+10 | 3      | 2 |
| **15**   | 1200000 | 9d8+12 | 3      | 3 |
| **16**   | 1350000 | 9d8+14 | 4      | 3 |
| **17**   | 1500000 | 9d8+16 | 4      | 4 |
| **18**   | 1650000 | 9d8+18 | 5      | 4 |
| **19**   | 1800000 | 9d8+20 | 5      | 5 |
| **20**   | 1950000 | 9d8+22 | 6      | 5 |



#### **SAVES**

| **Lvl**       | **Death** | **Wands** | **Paralysis** | **Breath** | **Spells** |
|---------------|-----------|-----------|---------------|------------|------------|
| **1**         | 12        | 13        | 14            | 15         | 17         |
| **2**         | 11        | 12        | 14            | 15         | 16         |
| **3**         | 11        | 12        | 14            | 15         | 16         |
| **4**         | 11        | 11        | 13            | 14         | 15         |
| **5**         | 11        | 11        | 13            | 14         | 15         |
| **6**         | 10        | 11        | 12            | 14         | 15         |
| **7**         | 10        | 11        | 12            | 14         | 15         |
| **8**         | 9         | 10        | 12            | 13         | 14         |
| **9**         | 9         | 10        | 12            | 13         | 14         |
| **10**        | 9         | 9         | 11            | 12         | 13         |
| **11**        | 9         | 9         | 11            | 12         | 13         |
| **12**        | 8         | 9         | 10            | 12         | 13         |
| **13**        | 8         | 9         | 10            | 12         | 13         |
| **14**        | 7         | 8         | 10            | 11         | 12         |
| **15**        | 7         | 8         | 10            | 11         | 12         |
| **16**        | 7         | 7         | 9             | 10         | 11         |
| **17**        | 7         | 7         | 9             | 10         | 11         |
| **18**        | 6         | 7         | 8             | 10         | 11         |
| **19**        | 6         | 7         | 8             | 10         | 11         |
| **20**        | 5         | 6         | 8             | 9          | 10         |

</details>

### **Turn Undead**

<details>

| **Caster Level** | **1 Hit Die** | **2 Hit Dice** | **3 Hit Dice** | **4 Hit Dice** | **5 Hit Dice** | **6 Hit Dice** | **7 Hit Dice** | **8 Hit Dice** | **9+ Hit Dice** |
|:----------------:|:-------------:|:--------------:|:--------------:|:--------------:|:--------------:|:--------------:|:--------------:|:--------------:|:---------------:|
| **2-3**          | 13            | 17             | 19             | No             | No             | No             | No             | No             | No              |
| **4-5**          | 11            | 15             | 18             | 20             | No             | No             | No             | No             | No              |
| **6-7**          | 9             | 13             | 17             | 19             | No             | No             | No             | No             | No              |
| **8-9**          | 7             | 11             | 15             | 18             | 20             | No             | No             | No             | No              |
| **10-11**        | 5             | 9              | 13             | 17             | 19             | No             | No             | No             | No              |
| **12-13**        | 3             | 7              | 11             | 15             | 18             | 20             | No             | No             | No              |
| **14-15**        | 2             | 5              | 9              | 13             | 17             | 19             | No             | No             | No              |
| **16-17**        | T             | 3              | 7              | 11             | 15             | 18             | 20             | No             | No              |
| **18-19**        | T             | 2              | 5              | 9              | 13             | 17             | 19             | No             | No              |
| **20**           | T             | T              | 3              | 7              | 11             | 15             | 18             | 20             | No              |

</details>

</details>

## **The Spellsword (Fighter+MU)**
<details>

- **Ancestry Limits:** Elf
- **Prime Requisite:** STR (Must be 9+); INT (Must be 9+)
- **Hit Dice:** D6
- **Weapons:** Any.
- **Armor:** Any, including shields.
- **Special:**
  - **Cleave:** If a Spellsword drops a foe of to Zero health the fighter may make another attack if an available target is within range. Total number of 'Cleave' attacks not to exceed the fighter's level.
  - **Spell casting:** Start with **Read Magic** and 1+INT modifier number of spells, randomly selected. Magic-users carry spell books containing the formulae for arcane spells.
  - **Using magic items:** Able to use magic scrolls of spells on their spell list. They can also use items that may only be used by arcane spell casters (e.g. magic wands).
  - **Magical research:** Can spend time and money on research to add new spells to their spell book and to research other magical effects. At 7th level, they are able to make temporary magic items. At 9th level, they can make permanent ones.

### **Progression & Saves**
<details>


#### **PROGRESSION**

|             |         |        |        | **Spells** |       |       |       |       |       |
|-------------|---------|--------|--------|------------|-------|-------|-------|-------|-------|
| **Lvl**     | **XP**  | **HD** | **AB** | **1**      | **2** | **3** | **4** | **5** | **6** |
| **1**       | 0       | 1d6    | 1      | 1          | -     | -     | -     | -     | -     |
| **2**       | 4500    | 2d6    | 2      | 2          | -     | -     | -     | -     | -     |
| **3**       | 9000    | 3d6    | 2      | 2          | 1     | -     | -     | -     | -     |
| **4**       | 18000   | 4d6    | 3      | 2          | 2     | -     | -     | -     | -     |
| **5**       | 36000   | 5d6    | 4      | 2          | 2     | 1     | -     | -     | -     |
| **6**       | 72000   | 6d6    | 4      | 3          | 2     | 2     | -     | -     | -     |
| **7**       | 144000  | 7d6    | 5      | 3          | 2     | 2     | 1     | -     | -     |
| **8**       | 270000  | 8d6    | 6      | 3          | 3     | 2     | 2     | -     | -     |
| **9**       | 540000  | 9d6    | 6      | 3          | 3     | 2     | 2     | 1     | -     |
| **10**      | 810000  | 9d6+2  | 6      | 4          | 3     | 3     | 2     | 2     | -     |
| **11**      | 1080000 | 9d6+4  | 7      | 4          | 4     | 3     | 2     | 2     | 1     |
| **12**      | 1350000 | 9d6+6  | 7      | 4          | 4     | 3     | 3     | 2     | 2     |
| **13**      | 1620000 | 9d6+8  | 8      | 4          | 4     | 4     | 3     | 2     | 2     |
| **14**      | 1890000 | 9d6+10 | 8      | 4          | 4     | 4     | 3     | 3     | 2     |
| **15**      | 2160000 | 9d6+12 | 8      | 5          | 4     | 4     | 3     | 3     | 2     |
| **16**      | 2430000 | 9d6+14 | 9      | 5          | 5     | 4     | 3     | 3     | 2     |
| **17**      | 2700000 | 9d6+16 | 9      | 5          | 5     | 4     | 4     | 3     | 3     |
| **18**      | 2970000 | 9d6+18 | 10     | 6          | 5     | 4     | 4     | 3     | 3     |
| **19**      | 3240000 | 9d6+20 | 10     | 6          | 5     | 5     | 4     | 3     | 3     |
| **20**      | 3510000 | 9d6+22 | 10     | 6          | 5     | 5     | 4     | 4     | 3     |


#### **SAVES**

| **Lvl** | **Death** | **Wands** | **Paralysis** | **Breath** | **Spells** |
|---------|-----------|-----------|---------------|------------|------------|
| **1**   | 12        | 11        | 12            | 15         | 13         |
| **2**   | 11        | 10        | 12            | 15         | 12         |
| **3**   | 11        | 10        | 12            | 15         | 12         |
| **4**   | 11        | 9         | 11            | 14         | 11         |
| **5**   | 11        | 9         | 11            | 14         | 11         |
| **6**   | 10        | 9         | 10            | 14         | 11         |
| **7**   | 10        | 9         | 10            | 14         | 11         |
| **8**   | 9         | 8         | 9             | 13         | 10         |
| **9**   | 9         | 8         | 9             | 13         | 10         |
| **10**  | 9         | 7         | 8             | 12         | 9          |
| **11**  | 9         | 7         | 8             | 12         | 9          |
| **12**  | 8         | 7         | 8             | 12         | 9          |
| **13**  | 8         | 7         | 8             | 12         | 9          |
| **14**  | 7         | 6         | 7             | 11         | 8          |
| **15**  | 7         | 6         | 7             | 11         | 8          |
| **16**  | 7         | 5         | 6             | 10         | 7          |
| **17**  | 7         | 5         | 6             | 10         | 7          |
| **18**  | 6         | 5         | 5             | 10         | 7          |
| **19**  | 6         | 5         | 5             | 10         | 7          |
| **20**  | 5         | 4         | 4             | 9          | 6          |


</details>

</details>

## **The Druid (Cleric Sub-Class)**
<details>

- **Ancestry Limits:** Any
- **Prime Requisite:** WIS (Must be 9+)
- **Hit Dice:** D6
- **Weapons:** 1H melee, staff, sling, short bow.			
- **Armor:** No metal armor, only wooden Shields.
- **Starting Saves:** Death 11; Wands 12; Paralysis 14; Breath 16; Spells 15
- **Special:**
  - **Deity disfavour:** Druids must be faithful to the tenets of their religion. Druids who fall from favor with their deity may incur penalties.
  - **Holy Symbol:** Often a Druid uses mistletoe as a holy symbol or focus, but this can vary with specific nature deities. Many times they will have many on a cord around their neck and grab the one that is associated with the spirit they are calling.
  - **Animal Affinity:** Druids have the Animal Affinity ability, which is the ability to calm or befriend normal animals.  The Druid attempts to communicate a benign intent, and through his or her connection to the natural world the animals affected may be either calmed or befriended.
  - **Spell casting:** At 2nd level, the character can cast spells of divine nature available to Druids. List below.Using magic items: As spell casters, clerics can use magic scrolls of spells on their spell list. They can also use items that may only be used by divine spell casters (e.g. some magic staves).
  - **Magical research:** Can spend time and money on research to create new spells or other magical effects associated with their deity. At 7th level, they are able to make potions. At 9th level, they can make permanent magical items.
  - **Identify Nature:** They can identify any natural animal or plant, and can identify clean water.

### **Progression & Saves**
<details>


#### **PROGRESSION**

|             |         |        |        | **Spells** |       |       |       |       |       |
|-------------|---------|--------|--------|------------|-------|-------|-------|-------|-------|
| **Lvl**     | **XP**  | **HD** | **AB** | **1**      | **2** | **3** | **4** | **5** | **6** |
| **1**       | 0       | 1d6    | 1      | -          | -     | -     | -     | -     | -     |
| **2**       | 1500    | 2d6    | 1      | 1          | -     | -     | -     | -     | -     |
| **3**       | 3000    | 3d6    | 2      | 2          | -     | -     | -     | -     | -     |
| **4**       | 6000    | 4d6    | 2      | 2          | 1     | -     | -     | -     | -     |
| **5**       | 12000   | 5d6    | 3      | 2          | 2     | -     | -     | -     | -     |
| **6**       | 24000   | 6d6    | 3      | 2          | 2     | 1     | -     | -     | -     |
| **7**       | 48000   | 7d6    | 4      | 3          | 2     | 2     | -     | -     | -     |
| **8**       | 90000   | 8d6    | 4      | 3          | 2     | 2     | 1     | -     | -     |
| **9**       | 180000  | 9d6    | 5      | 3          | 3     | 2     | 2     | -     | -     |
| **10**      | 270000  | 9d6+1  | 5      | 3          | 3     | 2     | 2     | 1     | -     |
| **11**      | 360000  | 9d6+2  | 5      | 4          | 3     | 3     | 2     | 2     | -     |
| **12**      | 450000  | 9d6+3  | 6      | 4          | 4     | 3     | 2     | 2     | 1     |
| **13**      | 540000  | 9d6+4  | 6      | 4          | 4     | 3     | 3     | 2     | 2     |
| **14**      | 630000  | 9d6+5  | 6      | 4          | 4     | 4     | 3     | 2     | 2     |
| **15**      | 720000  | 9d6+6  | 7      | 4          | 4     | 4     | 3     | 3     | 2     |
| **16**      | 810000  | 9d6+7  | 7      | 5          | 4     | 4     | 3     | 3     | 2     |
| **17**      | 900000  | 9d6+8  | 7      | 5          | 5     | 4     | 3     | 3     | 2     |
| **18**      | 990000  | 9d6+9  | 8      | 5          | 5     | 4     | 4     | 3     | 3     |
| **19**      | 1080000 | 9d6+10 | 8      | 6          | 5     | 4     | 4     | 3     | 3     |
| **20**      | 1170000 | 9d6+11 | 8      | 6          | 5     | 5     | 4     | 3     | 3     |


#### **SAVES**

| **Lvl**       | **Death** | **Wands** | **Paralysis** | **Breath** | **Spells** |
|---------------|-----------|-----------|---------------|------------|------------|
| **1**         | 11        | 12        | 14            | 16         | 15         |
| **2**         | 10        | 11        | 13            | 15         | 14         |
| **3**         | 10        | 11        | 13            | 15         | 14         |
| **4**         | 9         | 10        | 13            | 15         | 14         |
| **5**         | 9         | 10        | 13            | 15         | 14         |
| **6**         | 9         | 10        | 12            | 14         | 13         |
| **7**         | 9         | 10        | 12            | 14         | 13         |
| **8**         | 8         | 9         | 12            | 14         | 13         |
| **9**         | 8         | 9         | 12            | 14         | 13         |
| **10**        | 8         | 9         | 11            | 13         | 12         |
| **11**        | 8         | 9         | 11            | 13         | 12         |
| **12**        | 7         | 8         | 11            | 13         | 12         |
| **13**        | 7         | 8         | 11            | 13         | 12         |
| **14**        | 7         | 8         | 10            | 12         | 11         |
| **15**        | 7         | 8         | 10            | 12         | 11         |
| **16**        | 6         | 7         | 10            | 12         | 11         |
| **17**        | 6         | 7         | 10            | 12         | 11         |
| **18**        | 6         | 7         | 9             | 11         | 10         |
| **19**        | 6         | 7         | 9             | 11         | 10         |
| **20**        | 5         | 6         | 9             | 11         | 10         |

</details>

### **Animal Affinity Table**
<details>

| **Druid Level** | **< 1 HD** | **1HD** | **2HD** | **3HD** | **4HD** | **5HD** | **6HD** | **7HD** | **8HD** | **9HD** | **10HD** |
|-----------------|------------|---------|---------|---------|---------|---------|---------|---------|---------|---------|----------|
| **1**           | 9          | 13      | 17      | 19      | No      | No      | No      | No      | No      | No      | No       |
| **2**           | 7          | 11      | 15      | 18      | 20      | No      | No      | No      | No      | No      | No       |
| **3**           | 5          | 9       | 13      | 17      | 19      | No      | No      | No      | No      | No      | No       |
| **4**           | 3          | 7       | 11      | 15      | 18      | 20      | No      | No      | No      | No      | No       |
| **5**           | 2          | 5       | 9       | 13      | 17      | 19      | No      | No      | No      | No      | No       |
| **6**           | C          | 3       | 7       | 11      | 15      | 18      | 20      | No      | No      | No      | No       |
| **7**           | C          | 2       | 5       | 9       | 13      | 17      | 19      | No      | No      | No      | No       |
| **8**           | C          | C       | 3       | 7       | 11      | 15      | 18      | 20      | No      | No      | No       |
| **9**           | B          | C       | 2       | 5       | 9       | 13      | 17      | 19      | No      | No      | No       |
| **10**          | B          | C       | C       | 3       | 7       | 11      | 15      | 18      | 20      | No      | No       |
| **11**          | B          | B       | C       | 2       | 5       | 9       | 13      | 17      | 19      | No      | No       |
| **12**          | B          | B       | C       | C       | 3       | 7       | 11      | 15      | 18      | 20      | No       |
| **13**          | B          | B       | B       | C       | 2       | 5       | 9       | 13      | 17      | 19      | No       |
| **14**          | B          | B       | B       | C       | C       | 3       | 7       | 11      | 15      | 18      | 20       |
| **15**          | B          | B       | B       | B       | C       | 2       | 5       | 9       | 13      | 17      | 19       |
| **16**          | B          | B       | B       | B       | C       | C       | 3       | 7       | 11      | 15      | 18       |
| **17**          | B          | B       | B       | B       | B       | C       | 2       | 5       | 9       | 13      | 17       |
| **18**          | B          | B       | B       | B       | B       | C       | C       | 3       | 7       | 11      | 15       |
| **19**          | B          | B       | B       | B       | B       | B       | C       | 2       | 5       | 9       | 13       |
| **20**          | B          | B       | B       | B       | B       | B       | C       | C       | 3       | 7       | 11       |


</details>

</details>

## **The Ranger (Fighter Sub-Class)**
<details>

- **Ancestry Limits:** Any
- **Prime Requisite:** STR (Must be 9+); DEX (Must be 11+); WIS (Must be 11+)
- **Hit Dice:** D8
- **Weapons:** Any.
- **Armor:** Any, shields allowed.
- **Starting Saves:** Death 12; Wands 13; Paralysis 14; Breath 15; Spells 17
- **Special:**
  - **Wilderness Skills:** Can Move Silently, Hide, and Track when in wilderness areas using the appropriate table. Apply -20% penalty when attempting in urban areas. Move Silently and Hide may not be used in armor heavier than leather. While Tracking, they must roll once per hour traveled or lose trail.
  - **Chosen Enemy:** Ranger must declare a chosen enemy. Against this enemy, they get a bonus of +3 to damage.
  - **Expert Archer:** When using any regular bow (not crossbows) add +2 to Attack Bonus. At 5th level, they may fire three arrows every two rounds. This means they fire once on every odd round, and two on every even round with the second attack coming at the end of the round. At 9th level, they may fire two arrows every round, with the second coming at the end of the round.

### **Progression & Saves**
<details>


#### **PROGRESSION**

| **Lvl** | **XP**  | **HD** | **AB** |
|:-------:|:-------:|:------:|:------:|
| **1**   | 0       | 1d8    | 1      |
| **2**   | 2200    | 2d8    | 2      |
| **3**   | 4400    | 3d8    | 2      |
| **4**   | 8800    | 4d8    | 3      |
| **5**   | 17600   | 5d8    | 4      |
| **6**   | 35200   | 6d8    | 4      |
| **7**   | 70400   | 7d8    | 5      |
| **8**   | 132000  | 8d8    | 6      |
| **9**   | 264000  | 9d8    | 6      |
| **10**  | 396000  | 9d8+2  | 6      |
| **11**  | 528000  | 9d8+4  | 7      |
| **12**  | 660000  | 9d8+6  | 7      |
| **13**  | 792000  | 9d8+8  | 8      |
| **14**  | 924000  | 9d8+10 | 8      |
| **15**  | 1056000 | 9d8+12 | 8      |
| **16**  | 1188000 | 9d8+14 | 9      |
| **17**  | 1320000 | 9d8+16 | 9      |
| **18**  | 1452000 | 9d8+18 | 10     |
| **19**  | 1584000 | 9d8+20 | 10     |
| **20**  | 1716000 | 9d8+22 | 10     |


#### **SAVES**

| **Lvl**       | **Death** | **Wands** | **Paralysis** | **Breath** | **Spells** |
|---------------|-----------|-----------|---------------|------------|------------|
| **1**         | 12        | 13        | 14            | 15         | 17         |
| **2**         | 11        | 12        | 14            | 15         | 16         |
| **3**         | 11        | 12        | 14            | 15         | 16         |
| **4**         | 11        | 11        | 13            | 14         | 15         |
| **5**         | 11        | 11        | 13            | 14         | 15         |
| **6**         | 10        | 11        | 12            | 14         | 15         |
| **7**         | 10        | 11        | 12            | 14         | 15         |
| **8**         | 9         | 10        | 12            | 13         | 14         |
| **9**         | 9         | 10        | 12            | 13         | 14         |
| **10**        | 9         | 9         | 11            | 12         | 13         |
| **11**        | 9         | 9         | 11            | 12         | 13         |
| **12**        | 8         | 9         | 10            | 12         | 13         |
| **13**        | 8         | 9         | 10            | 12         | 13         |
| **14**        | 7         | 8         | 10            | 11         | 12         |
| **15**        | 7         | 8         | 10            | 11         | 12         |
| **16**        | 7         | 7         | 9             | 10         | 11         |
| **17**        | 7         | 7         | 9             | 10         | 11         |
| **18**        | 6         | 7         | 8             | 10         | 11         |
| **19**        | 6         | 7         | 8             | 10         | 11         |
| **20**        | 5         | 6         | 8             | 9          | 10         |

</details>

### **Wilderness Skills**

<details>

| **Lvl** | **Move Silently** | **Hide** | **Tracking** |
|:-------:|:-----------------:|:--------:|:------------:|
| **1**   | 25                | 10       | 40           |
| **2**   | 30                | 15       | 44           |
| **3**   | 35                | 20       | 48           |
| **4**   | 40                | 25       | 52           |
| **5**   | 45                | 30       | 56           |
| **6**   | 50                | 35       | 60           |
| **7**   | 55                | 40       | 64           |
| **8**   | 60                | 45       | 68           |
| **9**   | 65                | 50       | 72           |
| **10**  | 68                | 53       | 75           |
| **11**  | 71                | 56       | 78           |
| **12**  | 74                | 59       | 81           |
| **13**  | 77                | 62       | 84           |
| **14**  | 80                | 65       | 87           |
| **15**  | 83                | 68       | 90           |
| **16**  | 85                | 69       | 91           |
| **17**  | 87                | 70       | 92           |
| **18**  | 89                | 71       | 93           |
| **19**  | 91                | 72       | 94           |
| **20**  | 93                | 73       | 95           |

</details>
</details>
