# **EXTRA CLASSES**
[Back to Wiki](https://gitlab.com/voidcollection/shadows_of_the_abbey/-/wikis/home)


## **The Barbarian**
<details>

- **Prime Requisite:** STR (Must be 9+), DEX (Must be 9+), CON (Must be 9+)
- **Hit Dice:** D10
- **Weapons:** Any.
- **Armor:** Any, including shield.
- **Special:**
  - **Rage:** Once per day a Barbarian can fly into a Rage, which will last ten rounds. While raging, they cannot use any abilities that require concentration, or  activate magic items of any kind (including potions).  While raging, they must engage nearest enemy in melee combat with +2 bonus on attack rolls, damage rolls, and Saving Throws against mind altering spells but suffer a -2 to AC. They can end it early with a successful Save vs. Spells. At the end, they suffer -2 to all rolls and AC for 1 hour. Use twice at 6th level and 3 times a day at 12th level.
  -  **Abilities when Wearing Leather Armor or Less:**
     - **Surprise:** Can be surprised only on 1-iin-6. In the wilderness, can surprise on a roll of 1 to 3-in-6.
     - **Runner:** Adds 5’ to their tactical movement in combat.
     - **Wall Scaling:** Able to climb sheer walls without tools.
  -  **Superstitious:** Refuses use of arcane magic and distrusts it’s users. Accepts holy magic that align with beliefs.

### **Progression & Saves**
<details>

|**Lvl**|**XP**|**HD**|**AB**|
| :- | :- | :- | :- |
|1|0|1d10|+1|
|2|2,500|2d10|+2|
|3|5,000|3d10|+2|
|4|10,000|4d10|+3|
|5|20,000|5d10|+4|
|6|40,000|6d10|+4|
|7|80,000|7d10|+5|
|8|150,000|8d10|+6|
|9|300,000|9d10|+6|
|10|450,000|9d10+2|+6|
|11|600,000|9d10+4|+7|
|12|750,000|9d10+6|+7|
|13|900,000|9d10+8|+8|
|14|1,050,000|9d10+10|+8|
|15|1,200,000|9d10+12|+8|
|16|1,350,000|9d10+14|+9|
|17|1,500,000|9d10+16|+9|
|18|1,650,000|9d10+18|+10|
|19|1,800,000|9d10+20|+10|
|20|1,950,000|9d10+22|+10|

|**Saves**||||||
| :- | :- | :- | :- | :- | :- |
|**Lvl**|**Death**|**Wands**|**Paralysis**|**Breath**|**Spells**|
|**1**|12|13|14|15|17|
|**2-3**|11|12|14|15|16|
|**4-5**|11|11|13|14|15|
|**6-7**|10|11|12|14|15|
|**8-9**|9|10|12|13|14|
|**10-11**|9|9|11|12|13|
|**12-13**|8|9|10|12|13|
|**14-15**|7|8|10|11|12|
|**16-17**|7|7|9|10|11|
|**18-19**|6|7|8|10|11|

</details>
</details>

## **The Bard**
<details>

- **Prime Requisite:** CHA (Must be 9+)
- **Hit Dice:** D4
- **Weapons:** Any.
- **Armor:** Any, including shield.
- **Special:**
  - **Bard Songs:** All Bards have the ability to play and sing songs which can influence not just the mood but even the abilities of those who hear them. When performed, these songs typically apply a bonus of some sort to die rolls made by allies of the Bard, or a penalty to his or her enemies.  This is referred to as the "bonus" of the song. Bards begin play knowing Two Bard Song plus one additional song for each point of his or her Charisma bonus. At 3rd level and at each odd level afterwards, the Bard learns an additional song. Songs begin with a bonus of +1 (applied as a negative number when used against an enemy) and goes up every 4 levels.
  - **Keen Hearing:** +1 to Hear Noises checks.
  - **Lore:** 2-in-6 chance of knowing a bit of lore about a creature, place, or item when first encountering it. Automatic and rolled by Ref.

### **Progression & Saves**
<details>

|**Level**|**XP**|**HD**|**AB**|**Song Prof.**|**Bonus**|
| :- | :- | :- | :- | :- | :- |
|1|0|1d4|+1|2|+1|
|2|1,500|2d4|+1|2|+1|
|3|3,000|3d4|+2|3|+1|
|4|6,000|4d4|+2|3|+1|
|5|12,000|5d4|+3|4|+2|
|6|24,000|6d4|+3|4|+2|
|7|48,000|7d4|+4|5|+2|
|8|90,000|8d4|+4|5|+2|
|9|180,000|9d4|+5|6|+3|
|10|270,000|9d4+2|+5|6|+3|
|11|360,000|9d4+4|+5|7|+3|
|12|450,000|9d4+6|+6|7|+3|
|13|540,000|9d4+8|+6|8|+4|
|14|630,000|9d4+10|+6|8|+4|
|15|720,000|9d4+12|+7|9|+4|
|16|810,000|9d4+14|+7|9|+4|
|17|900,000|9d4+16|+7|10|+5|
|18|990,000|9d4+18|+8|10|+5|
|19|1,080,000|9d4+20|+8|11|+5|
|20|1,170,000|9d4+22|+8|11|+5|

|**Saves**||||||
| :- | :- | :- | :- | :- | :- |
|**Lvl**|**Death**|**Wands**|**Paralysis**|**Breath**|**Spells**|
|**1**|12|13|14|15|17|
|**2-3**|11|12|14|15|16|
|**4-5**|11|11|13|14|15|
|**6-7**|10|11|12|14|15|
|**8-9**|9|10|12|13|14|
|**10-11**|9|9|11|12|13|
|**12-13**|8|9|10|12|13|
|**14-15**|7|8|10|11|12|
|**16-17**|7|7|9|10|11|
|**18-19**|6|7|8|10|11|

</details>

### **Bard Songs**
<details>

All Bards have the ability to play and sing songs which can influence not just the mood but even the abilities of those who hear them. When performed, these songs typically apply a bonus of some sort to die rolls made by allies of the Bard, or a penalty to his or her enemies.  This is referred to as the "bonus" of the song. Bards begin play knowing two Bard Songs plus one additional song for each point of his or her Charisma bonus. At 3rd level and at each odd level afterwards, the Bard learns an additional song. Songs begin with a bonus of +1 (applied as a negative number when used against an enemy) and goes up every 4 levels.

Listed below are the basic Bard Songs available to Bard characters with an explanation of each song's benefit. The player should be encouraged to come up with his or her own unique song names and perhaps even come up with some sample lyrics or melody for each. Of course, this is not necessary for actual play but may enhance the role-playing aspect of the campaign.

Songs that are played during the course of combat must be played continuously. Regardless of the Bard's particular playing style, he or she may not utilize a weapon; if Combat Options is being used, the Bard may not engage in Defending.  The Bard may move about, but may not attack during a round in which he or she is performing.  If the Bard is struck in combat, the effect is immediately canceled until the Bard's turn when he or she can resume performing.  If a Bard chooses to end a song (with a flourish), the effect lingers one additional round; this does not apply if the Bard is interrupted.  Note that this allows the Bard to take advantage of the song effect himself on his next turn.

Not all songs require an instrument; if a song says "singing" then no instrument is needed, while if one says "playing" then an instrument is required.  Performing without an instrument applies a penalty of -1 to the song (completely nullifying the effect if the song has a bonus of +1).

### **The Songs**

**Alertness Song (rondo) –** While singing this quiet tune, all allies within 10 feet are less likely to be surprised, reducing the die roll range by 1 (from 1-2 on d6 to a roll of 1 on d6). A second rank of proficiency reduces the chance further to a roll of 1 on d8, and a third rank modifies the roll to 1 on d10. Proficiency in this particular Bard Song may not be increased beyond 3 such ranks. 

**Battle Song, offense (march) –** While playing all allies within 60 feet receive the benefit of +1 to their AB.

**Battle Song, defense (strophic) –** While playing all allies within 60 feet receive the benefit of +1 to their AC. 

**Charms Song (lullaby) –** By playing lullaby, a Bard lulls those listening into a drowsy day-dreamy state. Allies are unaffected, but others have a -1 penalty on saves versus sleep, charms, illusions, suggestions and similar effects.

**Funeral Song (requiem) –** While playing this song, undead creatures (or vile beings from netherworld regions) are more easily turned by Clerics or those with similar powers, granting a +1 to the Turning attempt.

**Healing Rest Song (nocturne) –** By playing periodically during the periods of rest, each allied character including the Bard receives the benefit of an additional point of healing (see p. 51 of the Core Rules).  A major disruption of the rest period will spoil the effect. 

**Laying of Hands Song (hymn) –** While this song of divine inspiration is being played, any allied Clerics within 60 feet receive an additional +1 point per die when casting any healing spell.

**Lock Picking Song (measures) –** By this carefully timed piece, a Bard can assist in the concentration of roguish characters during attempts to Open Locks and in the Removal of Traps (but not location of such traps).  The Thief gets a +1 bonus on such attempts for each rank of proficiency of the Bard.  Any character with such abilities can benefit from the song's effect.

**Magic Weakening Song (elegy) –** When playing this tune, magical effects are easier to shrug off, granting a +1 to any saving throw versus a magical effect (at the very least, all saves vs. Wands and Spells, and any other effect the GM rules is magical).  However, this song affects all within 60 feet of the Bard, whether ally or enemy.  If opposing Bards play the song at the same time, only the highest bonus is applied.

**Morale Improving Song (polonaise) –** While the Bard plays, allies within 60 feet have the benefit of +1 on morale or saves against fear effects. 

**Morale Defeating Song (caprice) –** While the Bard plays, any enemies within 60 feet have a penalty of -1 on morale or saves against fear effects.

**Pied Piper Song (allegro) –** While playing this song, natural animals (including giant varieties) are more easily befriended or calmed by druids or those with similar powers. The druid receives a +1 to his or her Animal Affinity (Turning) attempt. While the song is playing, any animal specific charm or control type spell (as determined by Game Master) has a -1 penalty to the animal's saves.

**Recall Song (lament) –** This calming tune aids concentration and contemplation, allowing a spell caster (including the playing Bard if applicable) to recall a previously cast 1st level spell after a period of 1 hour of meditation while the Bard plays. Neither the Bard nor the spell caster may be interrupted during this time. No one spell caster may benefit from this effect more than once per day, but multiple spell casters may benefit from the effect. Increased proficiency allows spells of higher level to be recalled (up to level 5 spells), but the spell caster may choose lower level spells if desired. Regardless, only one spell can be recalled. 

**Song of Destruction (crescendo) –** When playing this tune, magical effects are much more destructive, granting a +1 to the effective caster level of damaging spells originating within 60 feet of the Bard, modifying spell effects such as range, damage, and/or duration. This song affects both allies and enemies equally. Only the highest proficiency Bard effect applies when multiple bards might play this song at same time. The empowered caster does not receive additional memorized spells or other level derived benefits, only increased effectiveness with their current allotment of damaging spells.

**Travel Song (barcarolle) –** By playing a rhythmic tune during the majority of a day's traveling time, the allied group receives the benefit of +10% additional distance after all other factors are figured. Each additional level of proficiency increases this bonus by an additional +10% (up to +50% maximum).

</details>
</details>

## **The Druid**
<details>

- **Prime Requisite:** WIS (Must be 9+)
- **Hit Dice:** D6
- **Weapons:** One-handed melee weapons, as well as staff, sling, and short bow.
- **Armor:** No metal armor of any type, only wooden Shields.
- **Special:**
  - **Deity disfavour:** Druids must be faithful to the tenets of their religion. Druids who fall from favor with their deity may incur penalties.
  - **Holy Symbol:** Often a Druid uses mistletoe as a holy symbol, but this can vary with specific nature deities.
  - **Animal Affinity:** Druids have the Animal Affinity ability, which is the ability to calm or befriend normal animals.  The Druid attempts to communicate a benign intent, and through his or her connection to the natural world the animals affected may be either calmed or befriended.
  - **Spell casting:** At 2nd level, the character can cast spells of divine nature available to Druids. List below.
  - **Using magic items:** As spell casters, clerics can use magic scrolls of spells on their spell list. They can also use items that may only be used by divine spell casters (e.g. some magic staves).
  - **Magical research:** Can spend time and money on research to create new spells or other magical effects associated with their deity. At 7th level, they are able to make potions. At 9th level, they can make permanent magical items.
  - **Identify Nature:** They can identify any natural animal or plant, and can identify clean water.

### **Progression & Saves**
<details>

|||||**Spells**||||||
| :- | :- | :- | :- | :- | :- | :- | :- | :- | :- |
|**Lvl**|**XP**|**HD**|**AB**|**1**|**2**|**3**|**4**|**5**|**6**|
|1|0|1d6|+1|-|-|-|-|-|-|
|2|1,500|2d6|+1|1|-|-|-|-|-|
|3|3,000|3d6|+2|2|-|-|-|-|-|
|4|6,000|4d6|+2|2|1|-|-|-|-|
|5|12,000|5d6|+3|2|2|-|-|-|-|
|6|24,000|6d6|+3|2|2|1|-|-|-|
|7|48,000|7d6|+4|3|2|2|-|-|-|
|8|90,000|8d6|+4|3|2|2|1|-|-|
|9|180,000|9d6|+5|3|3|2|2|-|-|
|10|270,000|9d6+1|+5|3|3|2|2|1|-|
|11|360,000|9d6+2|+5|4|3|3|2|2|-|
|12|450,000|9d6+3|+6|4|4|3|2|2|1|
|13|540,000|9d6+4|+6|4|4|3|3|2|2|
|14|630,000|9d6+5|+6|4|4|4|3|2|2|
|15|720,000|9d6+6|+7|4|4|4|3|3|2|
|16|810,000|9d6+7|+7|5|4|4|3|3|2|
|17|900,000|9d6+8|+7|5|5|4|3|3|2|
|18|990,000|9d6+9|+8|5|5|4|4|3|3|
|19|1,080,000|9d6+10|+8|6|5|4|4|3|3|
|20|1,170,000|9d6+11|+8|6|5|5|4|3|3|

|**Saves**||||||
| :- | :- | :- | :- | :- | :- |
|**Lvl**|**Death** |**Wands**|**Paralysis**|**Breath**|**Spells**|
|**1**|11|12|14|16|15|
|**2-3**|10|11|13|15|14|
|**4-5**|9|10|13|15|14|
|**6-7**|9|10|12|14|13|
|**8-9**|8|9|12|14|13|
|**10-11**|8|9|11|13|12|
|**12-13**|7|8|11|13|12|
|**14-15**|7|8|10|12|11|
|**16-17**|6|7|10|12|11|
|**18-19**|6|7|9|11|10|
|**20**|5|6|9|11|10|

</details>

### **Animal Affinity Information**
<details>

#### **Druid Animal Affinity Table**

||**Hit Dice of Animal**|||||||||||
| :- | :- | :- | :- | :- | :- | :- | :- | :- | :- | :- | :- |
|**Lvl**|**< 1**|**1**|**2**|**3**|**4**|**5**|**6**|**7**|**8**|**9**|**10**|
|1|9|13|17|19|No|No|No|No|No|No|No|
|2|7|11|15|18|20|No|No|No|No|No|No|
|3|5|9|13|17|19|No|No|No|No|No|No|
|4|3|7|11|15|18|20|No|No|No|No|No|
|5|2|5|9|13|17|19|No|No|No|No|No|
|6|C|3|7|11|15|18|20|No|No|No|No|
|7|C|2|5|9|13|17|19|No|No|No|No|
|8|C|C|3|7|11|15|18|20|No|No|No|
|9|B|C|2|5|9|13|17|19|No|No|No|
|10|B|C|C|3|7|11|15|18|20|No|No|
|11|B|B|C|2|5|9|13|17|19|No|No|
|12|B|B|C|C|3|7|11|15|18|20|No|
|13|B|B|B|C|2|5|9|13|17|19|No|
|14|B|B|B|C|C|3|7|11|15|18|20|
|15|B|B|B|B|C|2|5|9|13|17|19|
|16|B|B|B|B|C|C|3|7|11|15|18|
|17|B|B|B|B|B|C|2|5|9|13|17|
|18|B|B|B|B|B|C|C|3|7|11|15|
|19|B|B|B|B|B|B|C|2|5|9|13|
|20|B|B|B|B|B|B|C|C|3|7|11|

*-Tame, Domesticated, or Normal Beasts of Burden are treated as half their actual Hit Dice.
-Monstrous Animals or other "Near-Natural" Animals are treated as 1 Hit Die higher.*

Druids have the Animal Affinity ability, which is the ability to calm or befriend normal animals.  The Druid attempts to communicate a benign intent, and through his or her connection to the natural world the animals affected may be either calmed or befriended.  The player rolls 1d20 and tells the GM the result.  Note that the player should always roll.

Tame or normally domesticated animals are treated as half their actual HD. Monstrous animals are treated as they are 1 HD more than listed.  If the table indicates "No" for that combination, it is not possible for the Druid to affect that type of animal.  If the table gives a number, that is the minimum number needed on the 1d20 to Calm that sort of animal.  If the table says "C" for that combination, that type of animal is automatically affected.  If the result shown is a "B" for that combination, that type of animal is automatically befriended.

If the roll is a success, 2d6 HD of animals are affected.  Surplus hit dice are lost, but at least one animal is always affected if the first roll is a success.  

If a mixed group of animals (say, a boar and a black bear) is to be affected, the player still rolls just once.  The result is checked against the weakest sort first (the boar), and if they are successfully Calmed or Befriended, the same result is checked against the next higher type of animal.  Likewise, the 2d6 HD are rolled only once. 

If a Druid succeeds at Calming or Befriending the animals, but not all animals present are affected, he or she may try again in the next round to affect those which remain.  If any roll to Calm or Befriend the Animals fails, that Druid may not attempt to use his or her Animal Affinity ability again for one full turn.  A partial failure (possible against a mixed group) counts as a failure for this purpose.

Calm animals will not interact with the Druid (or others accompanying the Druid) in any manner, unless approached by the Druid.  The Druid can calmly get them to leave an area. In this case, the GM should roll a reaction roll with any result below favorable meaning the animals flee. A Befriended animal will follow the Druid, guarding and assisting within its capabilities so long as the Druid remains in the general vicinity of its normal lair or range.  However, it will not "fight to the death" or sacrifice itself indiscriminately.  When substantially wounded, an animal will flee the area immediately.  Check morale as necessary when the situation seems appropriate.

</details>

### **Druid Spell Lists**
<details>

Below are listed spells available to the Druid subclass described above.  Spells in **bold** are new to this supplement.  Spells with a \* are reversible.

**First Level Druid Spells**

|1|**Animal Friendship**|
| :- | :- |
|2|Create Water|
|3|Cure Light Wounds\*|
|4|Detect Magic|
|5|**Detect Snares and Pits**|
|6|**Entangle**|
|7|**Faerie Fire**|
|8|**Pass Without Trace**|

**Second Level Druid Spells**

|1|Charm Animal|
| :- | :- |
|2|Find Traps|
|3|**Heat Metal**\*|
|4|**Obscuring Mists**|
|5|**Produce Flame/Cold**|
|6|**Slow Poison**|
|7|Speak With Animals|
|8|**Warp Wood\***|

**Third Level Druid Spells**

|1|**Assume Animal Form**|
| :- | :- |
|2|**Call Lightning**|
|3|Cure Disease\*|
|4|**Hold Animal**|
|5|Neutralize Poison\*|
|6|**Plant Growth**|
|7|**Protection From Fire**|
|8|Water Breathing|

**Fourth Level Druid Spells**

|1|**Call Woodland Beings**|
| :- | :- |
|2|**Control Temperature, 10' Radius**|
|3|Cure Serious Wounds\*|
|4|Lower Water|
|5|**Protection From Lightning**|
|6|**Speak With Plants**|
|7|**Summon Animals I**|
|8|**Tree Sanctuary**|

**Fifth Level Druid Spells**

|1|**Commune With Nature**|
| :- | :- |
|2|**Control Winds**|
|3|Dispel Evil|
|4|**Flame Strike**|
|5|Growth of Animals|
|6|**Rock to Mud\***|
|7|**Summon Animals II**|
|8|Wall of Fire|

**Sixth Level Druid Spells**

|1|**Animate Natural Objects**|
| :- | :- |
|2|Find the Path|
|3|**Part Water** |
|4|**Pass Tree**|
|5|Reincarnate|
|6|**Summon Animals III**|
|7|**Weather Summoning**|
|8|Word of Recall|

</details>
</details>

  ## **The Illusionist**
<details>

- **Prime Requisite:** INT (Must be 13+)
- **Hit Dice:** D4
- **Weapons:** Cudgel, dagger, walking staff.
- **Armor:** None.
- **Special:**
  - **Spell casting:** Start with **Read Magic** and 1+INT modifier number of spells, randomly selected. Illusionists carry spell books containing the formulae for arcane spells of their own particular order. 
  - **Resistant Against Illusion:** +2 on Saves vs any sort of illusion or phantasm.
  - **Using magic items:** Able to use magic scrolls of spells on their spell list. They can also use items that may only be used by arcane spell casters (e.g. magic wands).
  - **Magical research:** Can spend time and money on research to add new spells to their spell book and to research other magical effects. At 7th level, they are able to make temporary magic items. At 9th level, they can make permanent ones.

### **Progression & Saves**
<details>

|||||**Spells**||||||
| :- | :- | :- | :- | :- | :- | :- | :- | :- | :- |
|**Lvl**|**XP**|**HD**|**AB**|**1**|**2**|**3**|**4**|**5**|**6**|
|1|0|1d4|+1|1|-|-|-|-|-|
|2|2,500|2d4|+1|2|-|-|-|-|-|
|3|5,000|3d4|+1|2|1|-|-|-|-|
|4|10,000|4d4|+2|2|2|-|-|-|-|
|5|20,000|5d4|+2|2|2|1|-|-|-|
|6|40,000|6d4|+3|3|2|2|-|-|-|
|7|80,000|7d4|+3|3|2|2|1|-|-|
|8|150,000|8d4|+3|3|3|2|2|-|-|
|9|300,000|9d4|+4|3|3|2|2|1|-|
|10|450,000|9d4+1|+4|4|3|3|2|2|-|
|11|600,000|9d4+2|+4|4|4|3|2|2|1|
|12|750,000|9d4+3|+4|4|4|3|3|2|2|
|13|900,000|9d4+4|+5|4|4|4|3|2|2|
|14|1,050,000|9d4+5|+5|4|4|4|3|3|2|
|15|1,200,000|9d4+6|+5|5|4|4|3|3|2|
|16|1,350,000|9d4+7|+6|5|5|4|3|3|2|
|17|1,500,000|9d4+8|+6|5|5|4|4|3|3|
|18|1,650,000|9d4+9|+6|6|5|4|4|3|3|
|19|1,800,000|9d4+10|+7|6|5|5|4|3|3|
|20|1,950,000|9d4+11|+7|6|5|5|4|4|3|

|**Saves**||||||
| :- | :- | :- | :- | :- | :- |
|**Lvl**|**Death** |**Wands**|**Paralysis**|**Breath**|**Spells**|
|**1**|13|14|13|16|15|
|**2-3**|13|14|13|15|14|
|**4-5**|12|13|12|15|13|
|**6-7**|12|12|11|14|13|
|**8-9**|11|11|10|14|12|
|**10-11**|11|10|9|13|11|
|**12-13**|10|10|9|13|11|
|**14-15**|10|9|8|12|10|
|**16-17**|9|8|7|12|9|
|**18-19**|9|7|6|11|9|
|**20**|8|6|5|11|8|

</details>

### **Illusionist Spell Lists**
<details>

Below are listed spells available to the Illusionist subclass described above. Spells in **bold** are new to this supplement.  Spells with a \* are reversible.

**First Level Illusionist Spells**

|1|**Audible Glamour**|
| :- | :- |
|2|Detect Invisibility|
|3|**Change Self**|
|4|**Color Spray**|
|5|**Dancing Lights**|
|6|**Detect Illusion**|
|7|**Gaze Reflection**|
|8|Light\*|
|9|Magic Mouth|
|10|Mirror Image|
|11|**Phantasmal Image**|
|12|Ventriloquism|

**Second Level Illusionist Spells**

|1|**Alter Self**|
| :- | :- |
|2|**Blur**|
|3|Continual Light\*|
|4|Detect Magic|
|5|**Dispel Illusion**|
|6|**Hypnotic Pattern**|
|7|Invisibility|
|8|**Obscurement**|
|9|Phantasmal Force|
|10|**Phantom Trap**|
|11|Read Languages|
|12|**Rope Trick**|

**Third Level Illusionist Spells**

|1|Clairvoyance|
| :- | :- |
|2|**Color Cloud**|
|3|Darkvision|
|4|**Displacement**|
|5|**Illusory Script**|
|6|**Illusory Wall**|
|7|Invisibility, 10' Radius|
|8|**Phantom Messenger**|
|9|**Phantom Steed**|
|10|**Shadow Door**|
|11|**Spectral Force**|
|12|**Stinking Cloud**|

**Fourth Level Illusionist Spells**

|1|**Advanced Illusion**|
| :- | :- |
|2|Confusion|
|3|Dispel Magic|
|4|Hallucinatory Terrain|
|5|Hold Person|
|6|**Improved Invisibility**|
|7|Massmorph|
|8|**Phantasmal Killer**|
|9|**Rainbow Pattern**|
|10|Silence, 15' Radius|
|11|**Solid Fog**|
|12|**Suggestion**|

**Fifth Level Illusionist Spells**

|1|Cloudkill|
| :- | :- |
|2|**Dream**|
|3|Feeblemind|
|4|**Mirage Arcana**|
|5|**Mislead**|
|6|**Nightmare**|
|7|**Programmed Illusion**|
|8|Projected Image|
|9|**Seeming**|
|10|Wizard Eye|

**Sixth Level Illusionist Spells**

|1|**False Vision**|
| :- | :- |
|2|Invisible Stalker|
|3|**Mass Invisibility**|
|4|**Mass Suggestion**|
|5|**Permanent Illusion**|
|6|**Shadow Walk**|
|7|True Seeing|
|8|**Maze**|
|9|**Phase Door**|
|10|**Veil**|

</details>
</details>
