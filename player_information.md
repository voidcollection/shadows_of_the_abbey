# **PLAYER RESOURCES**
[Back to Wiki](https://gitlab.com/voidcollection/shadows_of_the_abbey/-/wikis/home)

# **Expectations**

<details>
<summary>Less / More</summary>

## **Safety Tools**
As this game has a definite focus on danger and horror, we will be using safety tools to make sure we can explore these topics in a responsible way. The tools we will be using are Lines & Veils and the X card.

**Lines & Veils** are a list of things that the group agrees are either subject matter we don't want to include in our game (lines) or handle sensitively or off-screen (veils). The list can be updated and have things added or removed at any point. No one will be asked why they have added or removed anything.
Link: https://tinyurl.com/4x26rw9w

The X Card is an in-the-moment tool to request that we move on from a scene. This can be used by saying simply requesting to skip or move on during a scene or privately messaging me. This also does not need to explained and everyone respects the request.

## **Playstyle & Focus**

- This will be a game about delving into strange ruins, overcoming great evil, and obtaining riches beyond measure. Encounters will be deadly.
- **Backstories** are not necessary. A simple sentence or two will suffice.
- **Stable play** is strongly encouraged. This both allows for easier exploration of other classes, provides a way to continue play while a PC is recovering or training, and allows for a quick replacement in the event a PC dies.
- Adventures typically start and end in town.
- Adventure takes place outside of town, not in.
- Retainers are classed employees. To build a pool of retainers, players will need to put out hiring notices. If they are mistreated, it will become harder to employ them. Traditional pay is 1/2 share and 2gp x level of retainer.

## **Maps & Mapping**

- Games will be played using **Theater of the Mind** with occasional supplementary material if needed. There will not be battle maps.
- Dungeon maps will not be provided to players. It is recommended that player's elect a **Mapper** to track their progress through the dungeon to reduce the chances of getting lost. This will also be helpful for return trips.

## **Time Management**

Regular sessions will be on a biweekly basis on Sunday. We can have sessions outside of this time if enough players have something they want to accomplish and can agree on a time that I am available for. All adventures must begin and end in a place of safety. Ending in the middle of a dungeon is a very very bad idea.

If agreed at end of session, the next session can pick up the next day in-game while they are in the dungeon but the party a night passes. Preparation and fortifications can reduce the chance of something occurring during the night. Otherwise, outside of sessions we will be using 1:1 time during downtime. This means that time between sessions is reflected in the game world. Plots proceed, problems get worst, situations change, life goes on whether you are there to see it or not. During game, time will move as quickly or as slowly as needed. 

## **General OSR Principles**

### **Agency**

- Attributes and related saves do not define your character. They are tools.
- Don’t ask only what your character would do, ask what you would do, too.
- Be creative with your intuition, items, and connections.

### **Teamwork**

- Seek consensus from the other players before barreling forward.
- Stay on the same page about goals and limits, respecting each other and accomplishing more as a group than alone.

### **Exploration**

- Asking questions and listening to detail is more useful than any stats, items, or skills you have. Interrogate the fiction.
- Take the Ref’s description without suspicion, but don’t shy away from seeking more information. -There is no single correct way forward.

### **Talking**

- Treat NPCs as if they were real people, and rely on your curiosity to safely gain information and solve problems.
- You’ll find that most people are interesting and will want to talk things through before getting violent. No one wants to die for nothing.

### **Caution**

- Fighting is a choice and rarely a wise one; consider whether violence is the best way to achieve your goals.
- Try to stack the odds in your favor and retreat when things seem unfavorable.

### **Planning**

- Think of ways to avoid your obstacles through reconnaissance, subtlety, and fact-finding.
- Do some research and ask around about your objectives.

### **Ambition**

- Set goals and use your meager means to take steps forward.
- Expect nothing. Earn your reputation.
- Keep things moving forward and play to see what happens.

</details>

---
---

# **Setting Aspects**

<details>
<summary>Less / More</summary>

## **Magic**

- Magic is rare, strange, and dangerous. Magic and magic users are often either feared or worshipped.
- Magical items are unique one-of-a-kind artifacts and are exceptionally difficult to identify. Some might offer guidance or educated guesses but experimentation is the only sure-fire way to know what an item does.
- Magic items cannot be bought and sold. They are often traded for favors among the wealthy or knowledgeable.
- Unless innate or coming from a device, magic consists of intricate rituals and are not subtle.
- Magic-Users are distrusted due to their lack of limits vs holy casters and their tenets.

## **Society & Technology**

- Travel is very dangerous. Not only for the typical reasons of the referenced era but also due to monsters not being complete myth. Much of the world is unknown or difficult to visit.
- Slavery is universally prohibited by all governments and equal rights for women, POC, and LGBTQ+ members is considered indisputable.
- "Common" is the common trade language that is spoken to some degree by most communities.
- Dwarves, when it comes to magic, reject the Arcane and embrace the Holy.

</details>

---
---

# **Character Generation & Rules**

<details>
<summary>Less / More</summary>

## **Characters Creation Steps:**

1. Roll 3d6 down the line. May swap two stats.
2. Select a **RACE** from the list below. Make sure you meet the requirements.
3. Select a **CLASS** from the list below. Make sure you meet the requirements. 
4. Full HD for starting HP.
5. All classes start with 1d6x10gp, a small sack, a dagger or club, and no armor. Everything after can be bought normally. All holy classes start with basic holy symbol, all arcane classes start with spell book.
6. Name your adventurer!
7. Play!

## **Rerolls:**

Can be chosen as a reward for submitting a session recap. All starting characters have one. Can only bank one at a time.

## **Encumbrance Rules:**
Normal player characters are able to carry up to 60 pounds and still be considered lightly loaded, or up to 150 pounds and be considered heavily loaded. Strength raises this limit. Consult the tables below:

### **Load**
| **Strength** | **Light Load** | **Heavy Load** |
|--------------|:---------------|----------------|
| **3**        | 25             | 60             |
| **4-5**      | 35             | 90             |
| **6-8**      | 50             | 120            |
| **9-12**     | 60             | 150            |
| **13-15**    | 65             | 165            |
| **16-17**    | 70             | 180            |
| **18**       | 80             | 195            |

### Encounter Movement and Encumbrance
| **Armor Type**           | **Light Load** | **Heavy Load** |
|--------------------------|:---------------|----------------|
| No Armor/Magic Leather   | 40'            | 30'            |
| Leather Armor/Magic Metal| 30'            | 20'            |
| Metal Armor              | 20'            | 10'            |

# **Ancestry**

<details>
<summary> Less / More </summary>

## **Dwarf**
- **Ability Requirements:** CON 9 or higher, CHA 17 or lower
- **Hit Die:** Any
- **Weapons:** Large Weapons require two hands. No 2-handed swords, pole-arms, or longbows.
- **Special:** Darkvision (60'); Detect new construction, shifting walls, slanting passages, traps w/ 1-2 on d6. Can hear noises w/ 1-2 on d6.
- **Save Bonuses:** +4 vs Death, Wands, Paralysis, Spells; +3 vs Spells.
- **Languages:** Common; Dwarvish; +1/point of INT Bonus

---

## **Elves**
- **Ability Requirements:** INT 9 or higher, CON 17 or lower
- **Hit Die:** d6 maximum
- **Weapons:** Large weapons require two hands.
- **Special:** Darkvision (60'); Detect secret doors w/ 1-2 on d6; Immune to ghoul paralysis; Range reduction by one for surprise check; Can hear noises w/ 1-2 on d6.
- **Save Bonuses:** +2 vs Wands & Spells; +1 vs. Paralysis.
- **Languages:** Common; Elvish; +1/point of INT Bonus

---

## **Halfling**
- **Ability Requirements:** DEX 9 or higher, STR 17 or lower
- **Hit Die:** d6 maximum
- **Weapons:** Must use medium weapons in two hands. Cannot use large weapons.
- **Special:** +1 attack bonus on ranged weapons; +2 bonus to AC when attacked in melee by creatures larger than man-sized; +1 to initiative die rolls; Hide (10% chance to be detected outdoors, 30% chance to be detected indoors); Can hear noises w/ 1-2 on d6.
- **Save Bonuses:** +4 vs Death, Wands, Paralysis, Spells; +3 vs Breath.
- **Languages:** Common; Halfling; +1/point of INT Bonus

---

## **Human**
- **Ability Requirements:** No ability score requirements.
- **Hit Die:** Any
- **Weapons:** Large Weapons require two hands.
- **Special:** +20% to all earned experience.
- **Save Bonuses:** None
- **Languages:** Common; +1/point of INT Bonus

</details>

---
---

# **Classes**

<details>
<summary> Less / More </summary>

## **The Cleric**
- **Ancestry Limits:** Any
- **Prime Requisite:** WIS (Must be 9+)
- **Hit Dice:** D6
- **Weapons:** Blunt weapons only.
- **Armor:** Any, shields allowed.
- **Starting Saves:** Death 11; Wands 12; Paralysis 14; Breath 16; Spells 15
- **Special:**
  - **Deity disfavour:** Clerics must be faithful to the tenets of their alignment, clergy, and religion. Clerics who fall from favour with their deity may incur penalties.
  - **Turn Undead:** Clerics can invoke the power of their deity to repel undead monsters encountered. Requires brandishing a holy symbol.
  - **Spell casting:** Once a cleric has proven their faith (at 2nd level), the character may pray to receive spells.
  - **Using magic items:** As spell casters, clerics can use magic scrolls of spells on their spell list. They can also use items that may only be used by divine spell casters (e.g. some magic staves).
  - **Magical research:** Can spend time and money on research to create new spells or other magical effects associated with their deity. At 7th level, they are able to make temporary magic items. At 9th level, they can make permanent ones.

## **The Fighter**
- **Ancestry Limits:** Any
- **Prime Requisite:** STR (Must be 9+)
- **Hit Dice:** D8
- **Weapons:** Any.
- **Armor:** Any, shields allowed.
- **Starting Saves:** Death 12; Wands 13; Paralysis 14; Breath 15; Spells 17
- **Special:**
  - **Cleave:** If a fighter drops a foe of to Zero health the fighter may make another attack if an available target is within range. Total number of 'Cleave' attacks not to exceed the fighter's level.

## **The Magic-User**
- **Ancestry Limits:** Human, Elf
- **Prime Requisite:** INT (Must be 9+)
- **Hit Dice:** D4
- **Weapons:** Cudgel, dagger, walking staff.
- **Armor:** None.
- **Starting Saves:** Death 13; Wands 14; Paralysis 13; Breath 16; Spells 15
- **Special:**
  - **Spell casting:** Start with **Read Magic** and 1+INT modifier number of spells, randomly selected. Magic-users carry spell books containing the formulae for arcane spells.
  - **Using magic items:** Able to use magic scrolls of spells on their spell list. They can also use items that may only be used by arcane spell casters (e.g. magic wands).
  - **Magical research:** Can spend time and money on research to add new spells to their spell book and to research other magical effects. At 7th level, they are able to make temporary magic items. At 9th level, they can make permanent ones.'

## **The Thief**
- **Ancestry Limits:** Any
- **Prime Requisite:** DEX (Must be 9+)
- **Hit Dice:** D4
- **Weapons:** Any
- **Armor:** Leather, no Shields
- **Starting Saves:** Death 13; Wands 14; Paralysis 13; Breath 16; Spells 15
- **Special:**
  - **Thief Abilities:** Thieves have abilities to do things others cannot through intense training such as *Open Locks*, *Remove Traps*, *Pick Pockets*, *Move Silently*, *Climb  Walls*, *Hide*,  and *Listen*

## **The Paladin**
- **Ancestry Limits:** Any
- **Prime Requisite:** STR (Must be 9+); WIS (Must be 11+); CHA (Must be 11+)
- **Hit Dice:** D8
- **Weapons:** Any
- **Armor:** Any, including Shields
- **Starting Saves:** Death 12; Wands 13; Paralysis 14; Breath 15; Spells 17
- **Special:**
  - **Protection from Evil:** Emanates an aura equivalent to the spell in 10' radius.
  - **Detect Evil:** May cast, at will, as the spell.
  - **Holy Weapon:** Once per day, per level, can make a non-magical melee weapon equivalent to a magic weapon ofr purposes of hitting creatures only able to be struck with silver or magical weapons. Lasts 1 Turn.
  - **Lay on Hands:** Once per day, may Lay on Hands to any wounded character and heal 2 points of damage; add the Paladin's Charisma bonus to this figure. Increase number per day by one at each odd-numbered level. Starting at 7th level, they may choose **Cure Disease** (as the spell) instead of providing healing. At 11th level, they may substitute with **Neutralize Poison** (as the spell).
  - **Deity disfavour:** Paladins must be faithful to the tenets of their alignment, clergy, and religion. Those who fall from favour with their deity may incur penalties.
  - **Turn Undead:** Paladins can invoke the power of their deity to repel undead monsters encountered. Requires brandishing a holy symbol.
  - **Spell casting:** Once a Paladin has proven their faith (at 10th level), the character may pray to receive spells.

## **The Druid**
- **Ancestry Limits:** Any
- **Prime Requisite:** WIS (Must be 9+)
- **Hit Dice:** D6
- **Weapons:** One-handed melee weapons, as well as staff, sling, and short bow.
- **Armor:** No metal armor of any type, only wooden Shields.
- **Starting Saves:** Death 11; Wands 12; Paralysis 14; Breath 16; Spells 15
- **Special:**
  - **Deity disfavour:** Druids must be faithful to the tenets of their religion. Druids who fall from favor with their deity may incur penalties.
  - **Holy Symbol:** Often a Druid uses mistletoe as a holy symbol, but this can vary with specific nature deities.
  - **Animal Affinity:** Druids have the Animal Affinity ability, which is the ability to calm or befriend normal animals.  The Druid attempts to communicate a benign intent, and through his or her connection to the natural world the animals affected may be either calmed or befriended.
  - **Spell casting:** At 2nd level, the character can cast spells of divine nature available to Druids.
  - **Using magic items:** As spell casters, clerics can use magic scrolls of spells on their spell list. They can also use items that may only be used by divine spell casters (e.g. some magic staves).
  - **Magical research:** Can spend time and money on research to create new spells or other magical effects associated with their deity. At 7th level, they are able to make potions. At 9th level, they can make permanent magical items.
  - **Identify Nature:** They can identify any natural animal or plant, and can identify clean water.

## **The Spellsword**
- **Ancestry Limits:** Elf
- **Prime Requisite:** STR (Must be 9+); INT (Must be 9+)
- **Hit Dice:** D6
- **Weapons:** Any.
- **Armor:** Any, including shields.
- **Starting Saves:** - Death 12; Wands 11; Paralysis 15; Breath 16; Spells 13
- **Special:**
  - **Cleave:** If a fighter drops a foe of to Zero health the fighter may make another attack if an available target is within range. Total number of 'Cleave' attacks not to exceed the Spellsword's level.
  - **Spell casting:** Start with **Read Magic** and 1+INT modifier number of spells, randomly selected. Magic-users carry spell books containing the formulae for arcane spells.
  - **Using magic items:** Able to use magic scrolls of spells on their spell list. They can also use items that may only be used by arcane spell casters (e.g. magic wands).
  - **Magical research:** Can spend time and money on research to add new spells to their spell book and to research other magical effects. At 7th level, they are able to make temporary magic items. At 9th level, they can make permanent ones.

## **The Ranger**
- **Ancestry Limits:** Human, Elf, Halfling
- **Prime Requisite:** STR (Must be 9+); DEX (Must be 11+); WIS (Must be 11+)
- **Hit Dice:** D8
- **Weapons:** Any.
- **Armor:** Any, shields allowed.
- **Starting Saves:** Death 12; Wands 13; Paralysis 14; Breath 15; Spells 17
- **Special:**
  - **Wilderness Skills:** Can Move Silently, Hide, and Track when in wilderness areas using the appropriate table. Apply -20% penalty when attempting in urban areas. Move Silently and Hide may not be used in armor heavier than leather. While Tracking, they must roll once per hour traveled or lose trail.
  - **Chosen Enemy:** Ranger must declare a chosen enemy. Against this enemy, they get a bonus of +3 to damage.
  - **Expert Archer:** When using any regular bow (not crossbows) add +2 to Attack Bonus. At 5th level, they may fire three arrows every two rounds. This means they fire once on every odd round, and two on every even round with the second attack coming at the end of the round. At 9th level, they may fire two arrows every round, with the second coming at the end of the round.

</details>

---
---

## **DOWNTIME**

- **Rumor-Mongering:** A PC can spend the week buying drinks and greasing palms to get rumors about local happenings. Requires spending 1d4x10gp and making a reaction roll, modified by Charisma. This takes 1 full week per attempt. This is rolled by the Ref. **2:** Failure. **3-11:** Hear 1 Rumor. **12+:** Hear 2 Rumors.
- **Recruitment:** Retainers available for employment need to be developed. May only be used once a week. As those adventurers gain XP and level up, as long as they are not mistreated, remain in the pool for employment. 
  - **Taverns:** Spending the day going through taverns spreading the word and buying rounds costs 50gp. There is a 3-in-6 chance of successfully locating applicants.
  - **Help Wanted Postings:** Spend the week posting notices in public places costs 25gp. There is a 2-in-6 chance of successfully locating applicants who will apply at the specified location by **next session**.
- **Carousing:** Gold spent or "wasted" in endeavors that do not provide direct mechanical benefit such as donating to a temple, undertaking a public works project, partying, etc. Takes **1 Week** and Costs **1d8x100gp**. At the end of the week, make the **Save vs. Poison**. Failure still results in XP gained but the PC also suffers a complication related to the endeavor. If the player does not have the funds, they gain only 1/2 xp, spend all they have and suffer a -4 to their Save. To avoid this, a PC can take out a loan for this purpose but you will now be in debt to someone. Carousing also provides opportunities to gain contacts, hear rumors, etc.
- **Ability Improvements:** Ability scores can be raised through training. The first time a score is raised, it costs 2000gp and takes 1 month. Each additional time the same stat is increased, the cost doubles. A score cannot be raised beyond 16.

---
---

## **ADVANCEMENT**

- **XP for Exploration (Individual):** XP gained for each room properly explored during an expedition. Room must be engaged with. Each room is worth 50 XP and resets when the party returns to town. A room can only be “explored” once. Multiplied by level.
- **XP for Challenges Overcome (Individual):** XP gained from overcoming challenges presented by enemies.
- **XP for Carousing/Projects (Individual):** See above entry on Carousing
- **XP for Session Attendance (Individual):** 100xp gained from coming to the session.
- **Recaps (Individual):** Players have until the following weekend to post a recap of the session they attended, in-character or out. Once posted, the player may choose between recieving an extra 100xp towards their character or One Free Reroll to use in future sessions. Only one free reroll from this source can be banked at a time.

</details>
